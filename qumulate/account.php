<?php

error_reporting(E_ERROR | E_PARSE);

	$email=$_POST['email']; 
	$_SESSION['email']=$email;
	$domainurl=explode("/",$_POST['returnurl']);



?>
<!DOCTYPE html>
<html>
<head>
    <title>Account</title>
    
    <!-- Style Sheets -->
    <link href="css/dialog_box.css"  rel="stylesheet" type="text/css" />
    <link href="css/tabcontent.css" rel="stylesheet" type="text/css" />
    <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" >
    <link href="css/jerome.css" rel="stylesheet" type="text/css" />


    <!-- Javascript Files -->
    <script type="text/javascript" src="js/tabcontent.js" /></script>
    <script type="text/javascript" src="js/function.js" /></script>
    <script type="text/javascript" src="js/jquery1.js" /></script>
    <script type="text/javascript" src="js/postmessage.js" /></script> 
    <script type="text/javascript" src="js/manager.js" /></script>
    <script type="text/javascript" src="js/dialog_box.js"/></script>
    <script type="text/javascript" src="js/blockUI.js"/></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/bootbox.js"></script>



</head>

<body>
    <!-- start Header -->
    <div id="header"></div>

    <!-- end Header -->

    <script type="application/javascript" >

	$( "#header" ).load( "loggedHeader1.html");

    var currentInvPage; //Pagination for transaction history
    var currentUsePage; //Pagination for Usage history
    var currentPayPage
    $.ajaxSetup({
        cache: false
    });

    var accountId;
    var defaultCCNumberId;     
    var defaultCCType; 
    var paymentMethods;

    $(document).ready(function(){



        $('.account-summary-table').hide();
        $('.contact-info').hide();
        $('.subscription-summary-table').hide();
        $('.paymentmethod-summary-table').hide();
        $('.transaction-history-data').hide();

        $('.makePaymentTablediv').hide();

        $('.loading').show();
        $('.change_plan').hide();
        $('.add_payment_method_panel').hide();
        $('.bill-to-contact-summary-table-input').hide();
        $('.sold-to-contact-summary-table-input').hide();
        $('.update_sold_to_contact').show(); 
        $('.save_sold_to_changes').hide();
        $('.cancel_sold_to_changes').hide();
        $('.update_bill_to_contact').show();
        $('.save_bill_to_changes').hide();
        $('.cancel_bill_to_changes').hide();

// returnurl starts


var returnurl  = sessionStorage.getItem('returnurl');
var domainurl  = sessionStorage.getItem('domainurl');

			if(returnurl == null || returnurl == "" || returnurl == undefined)
			{
				sessionStorage.setItem('returnurl', document.getElementById('returnurl').value);
                        		if(sessionStorage.getItem('returnurl')){
                        	}
			}

			if(domainurl == null || domainurl == "" || domainurl == undefined)
			{
				sessionStorage.setItem('domainurl', document.getElementById('domainurl').value);
                        		if(sessionStorage.getItem('domainurl')){
                        	}		
			}

var qumulateuuid = "<?php echo $_POST['qumulateGroupUUID']; ?>";

var qumulateGroupUUID  = sessionStorage.getItem('qumulateGroupUUID');
var qumulateGroupUUID1 = sessionStorage.getItem('qumulateuuid');

if( qumulateGroupUUID == "" || qumulateGroupUUID == null ){

	if(qumulateuuid == "" || qumulateuuid == undefined )
	{
		qumulateuuid = "<?php echo $_POST['qumulateGroupUUID']; ?>";

		qumulateGroupUUID  = sessionStorage.setItem('qumulateGroupUUID', qumulateuuid);
		qumulateGroupUUID1  = sessionStorage.setItem('qumulateuuid', qumulateuuid);

	}
	else
	{
		qumulateuuid = "<?php echo $_POST['qumulateGroupUUID']; ?>";

		qumulateGroupUUID  = sessionStorage.setItem('qumulateGroupUUID', qumulateuuid);
		qumulateGroupUUID1  = sessionStorage.setItem('qumulateuuid', qumulateuuid);

	}
}


	if(qumulateGroupUUID  != "" && qumulateGroupUUID != qumulateuuid )
	{
		if(qumulateuuid == ""){

		}else{
			qumulateuuid = "<?php echo $_POST['qumulateGroupUUID']; ?>";

			qumulateGroupUUID  = sessionStorage.setItem('qumulateGroupUUID', qumulateuuid);
			qumulateGroupUUID1  = sessionStorage.setItem('qumulateuuid', qumulateuuid);

		}
	}

// returnurl ends


        $.getJSON("backend/index.php?type=IsUserLoggedIn",{email:document.getElementById('email').value},
            function(data){

                if(!data.success) { 

                    if(data.msg[0].msg=='SESSION_NOT_SET'){
                        //window.location.replace('https://test.qumulate.varian.com/index.xhtml');

				var logout = "<?php include 'backend/config.php'; echo $logout;  ?>";
				window.location.replace(logout);

                    }
                } else {

			 $.getJSON("backend/index.php?type=ReadCountry",
				function(data){
					if(data.success){
					var html ='';					
					newdata = data.msg;

					jQuery.each(newdata, function(i, val) {

						var result = $.parseJSON(val);
						$.each(result , function(k, v) {
						    html += "<option value='"+k+"'>"+v+"</option>";
						});
					});

					$('#bill_to_country').html(html);
					$('#sold_to_country').html(html);
				}

			});


                    getAccountSummary();

                    //show Usage history by default
                    loadInvoiceDetails(false);
                    loadPaymentDetails();
                    //getUsageHistory();
                    $("#transaction_select").val('invoice_history');
                    loadMakePaymentDetails();
                }
            }
        ); 
    });
    
    
    /**
     * updateHeader Updates html page header with welcome message and the account name
     * @param {var} accountName Account name of the user that is logged in
     */
    function updateHeader(accountName){

        var welcome = " Welcome ";

        var welcomeMessage = welcome.concat(accountName);
        var arrowDown = "  <b class=\"caret\"></b>";
        document.getElementById('accountHeaderName').innerHTML = accountName.concat(arrowDown);//welcomeMessage.concat(arrowDown);
    }

    /**
     * getTransactionHistory fetches the relevant transaction history data based on 
     * transaction type
     * @param {var} transactionType Type of transaction to fetch
     */
    function getTransactionHistory(transactionType){

        $('.loading').show();
        $('.transaction-history-data').hide();

        if(transactionType == "invoice_history"){

            loadInvoiceDetails(false);
        } else if(transactionType == "payment_history"){

            loadPaymentDetails();
        } else if(transactionType == "usage_history"){

            getUsageHistory();
        } else {
            //Do nothing
        }
    }

    /**
     * getUsageHistory fetches Usage History of the account that is logged in to
     *
     */
    function getUsageHistory(){
        currentUsePage = 1;
        $.getJSON("backend/index.php?type=GetUsageHistory",
            function(data){
                if(data.success){

                    var use = JSON.parse(data.msg[0]);

                    var html = "";
                    var accountName;

                    if(use.success)
                    {
                        if(use.usage.length > 0)
                        {
                            html+= "<table class=\"table table-hover\">";

                            html+= "    <thead>";
                            html+= "    <tr id=\"table_heading\">";
                            html+= "        <td ><b>#</b></td>";
                            html+= "        <td ><b>Account name</b></td>";
                            html+= "        <td ><b>Subscription Number</b></td>";
                            html+= "        <td><b>Charge Number</b></td>";
                            html+= "        <td ><b>Period Start Date</b></td>";
                            html+= "        <td ><b>UOM</b></td>";
                            html+= "        <td ><b>Quantity</b></td>";
                            html+= "        <td ><b>Status</b></td>";
                            html+= "    </tr>";
                            html+= "    </thead>";  
                            for(var i in use.usage){
                                var index = i;
                                ++index;
                                accountName = use.usage[i].accountName;

                                html+= "    <tr id=\"table_data\">";
                                html+= "       <td >"+index + "</td>";
                                html+="        <td width=\"20%\">"+accountName+"</td>";
                                html+="        <td>"+use.usage[i].subscriptionNumber+"</td>";
                                html+="        <td>"+use.usage[i].chargeNumber+"</td>";
                                //Extract the date and ignore the time
                                var date = formatZDate(use.usage[i].startDateTime.replace(/\s+/g, ''));
                                date = date.substr(0,date.indexOf(' '));
                                html+="        <td>"+date+"</td>";
                                html+="        <td >"+use.usage[i].unitOfMeasure+"</td>";
                                html+="        <td >"+use.usage[i].quantity+"</td>";
                                html+="        <td >"+use.usage[i].status+"</td>";
                                html+="    </tr>";

                            }
                            html+= "</table>";
                            $('.usetablediv').html(html);

                            var htmlPage = " ";
                            htmlPage+= "<ul class=\"pagination\">";
                            htmlPage+= "    <li class=\"disabled\" id=\"prevUse\" ><a id=\"prevUseButton\" onClick=\"\">&laquo;</a></li>";
                            htmlPage+= "    <li><a name=\"currentUsePage\" id=\"use.1\"><span class=\"sr-only\">page </span>1 </a></li>";
                            htmlPage+= "    <li class=\"disabled\" id=\"nextUse\"><a id=\"nextUseButton\" onClick=\"\">&raquo;</a></li>";
                            htmlPage+= "</ul>";
                            $('.pageUseButton').html(htmlPage);

                            var htmlGreet = "";
                            htmlGreet = "<a style=\"float:right; color:#FFA500;\"> Welcome " + accountName + "!";
                            $('.accountName').html(htmlGreet);
                        } else {
                            html+= "<table class=\"table table-hover\">";
                            html+= "    <tr>";
                            html+= " <th> No Usage records available at the moment </th>"
                            html+="    </tr>";
                            html+= "</table>";
                            $('.usetablediv').html(html);
                        }

                        if(typeof use.nextPage != 'undefined') {

                            //New pagination
                            document.getElementById("nextUse").className = "";
                            document.getElementById('nextUseButton').onclick = function(){ getNextPage();} ;
                            
                        }
                    }
                    showHistoryData();     
                }
            }
        );
    }
    
    
    /**
     * Get the next page of the usage history data
     */
    function getNextPage(){

        var currentUPage = document.getElementsByName("currentUsePage");
        var pNo = currentUPage[0].id.split(".");

        nextPageNum = parseInt(pNo[1]) + 1;
        getPage(nextPageNum);
    }

    /**
     * Get the previous page of the usage history data
     */
    function getPreviousPage(){

        var currentUPage = document.getElementsByName("currentUsePage");
        var pNo = currentUPage[0].id.split(".");

        var currPage = parseInt(pNo[1]);
        if(currPage > 1){
            prevPageNum = currPage - 1;
            getPage(prevPageNum);
        } else {
            prevPageNum = 1;
            getPage(prevPageNum);
        }
    }

    /**
     * Get the usage history records for the given page number 
     * @param {var} pageNumber page number of the usage history records to fetch
     */
    function getPage(pageNumber){
        
        $.getJSON("backend/index.php?type=GetUsageHistoryPage", 
            {pageNum : pageNumber},
            function(data){
                if(data.success){

                    var use = JSON.parse(data.msg[0]);
                
                    var html = "";
                    if(use.success)
                    {
                        if(use.usage.length > 0)
                        {   
                            html+= "<table class=\"table table-hover\">";
                            html+= "    <tr id=\"table_heading\">";
                            html+= "        <td ><b>#</b></td>";
                           // html+= "        <td ><b>Account name</b></td>";
                            html+= "        <td><b>Subscription Number</b></td>";
                            html+= "        <td><b>Charge Number</b></td>";
                            html+= "        <td><b>Period Start Date</b></td>";
                            html+= "        <td ><b>UOM</b></td>";
                            html+= "        <td ><b>Quantity</b></td>";
                            html+= "        <td ><b>Status</b></td>";
                            html+= "    </tr>"; 
                            for(var i in use.usage){
                                var index = i;
                                
                                var page = (pageNumber - 1) * 20;
                                ++index;
                                var ind = index + page;
                                html+= "    <tr id=\"table_data\">";
                                html+="        <td >"+ ind +"</td>";
                               // html+="        <td>"+use.usage[i].accountName+"</td>";
                                html+="        <td >"+use.usage[i].subscriptionNumber+"</td>";
                                html+="        <td >"+use.usage[i].chargeNumber+"</td>";
                                //Extract the date and ignore the time
                                var date = formatZDate(use.usage[i].startDateTime.replace(/\s+/g, ''));
                                date = date.substr(0,date.indexOf(' '));
                                html+="        <td >"+date+"</td>";
                                html+="        <td >"+use.usage[i].unitOfMeasure+"</td>";
                                html+="        <td>"+use.usage[i].quantity+"</td>";
                                html+="        <td>"+use.usage[i].status+"</td>";
                                html+="    </tr>";

                            }
                            html+=" </table>";
                            $('.usetablediv').html(html);

                            html = " ";
                            html+= "<ul class=\"pagination\">";
                            html+= "    <li class=\"disabled\" id=\"prevUse\" ><a id=\"prevUseButton\" onClick=\"\">&laquo;</a></li>";
                            html+= "    <li><a name=\"currentUsePage\" id=\"use."+ pageNumber + "\"><span class=\"sr-only\">page </span>"+ pageNumber + " </a></li>";
                            html+= "    <li class=\"disabled\" id=\"nextUse\"><a id=\"nextUseButton\" onClick=\"\">&raquo;</a></li>";
                            html+= "</ul>";
                            $('.pageUseButton').html(html);
                        } else {
                            html+= "<table class=\"table table-hover\">";
                            html+= "    <tr>";
                            html+= " <th> No Usage records available at the moment </th>"
                            html+="    </tr>";
                            html+= "</table";
                            $('.usetablediv').html(html);
                        }

                        if(typeof use.nextPage != 'undefined') {
                        
                            if(pageNumber == 1){
                                document.getElementById('prevUseButton').onclick = function(){ null;} ;
                                document.getElementById("prevUse").className = "disabled";
                            } else {
                                document.getElementById('prevUseButton').onclick = function(){ getPreviousPage();} ;
                                document.getElementById("prevUse").className = "";
                            }
                            
                            document.getElementById('nextUseButton').onclick = function(){ getNextPage();} ;
                            document.getElementById("nextUse").className = "";
                            
                            
                        } else {
                            document.getElementById("nextUse").className = "disabled";
                            document.getElementById("prevUse").className = "";
                            document.getElementById('prevUseButton').onclick = function(){ getPreviousPage();} ;
                            document.getElementById('nextUseButton').onclick = function(){ null;} ;
                        }
                    }
                }  
            }
        );
    }

    


    /**
     * getAccountSummary fetched the Account details of the user that is successfully logged in
     */
    function getAccountSummary(){

        $.getJSON("backend/index.php?type=GetAccountSummary",
            function(data){
                if(data.success){
                    var accountName;

                    var acc = JSON.parse(data.msg[0]);
                    accountId = acc.basicInfo.id;
                    accountName = acc.basicInfo.name;
                    window.account = accountName;


                    //Show Account Info
                    $('.account_name').html(accountName);
                    $('.last_payment_amount').html((acc.basicInfo.lastPaymentAmount!=null ? '$'+acc.basicInfo.lastPaymentAmount : 'N/A'));
                    $('.account_balance').html('$'+acc.basicInfo.balance);
                    $('.account_status').html(acc.basicInfo.status);
                    $('.last_payment_date').html((acc.basicInfo.lastPaymentDate!=null ? formatZDate(acc.basicInfo.lastPaymentDate).replace(/\s+/g, '') : 'N/A'));
                    $('.last_invoice_date').html((acc.basicInfo.lastInvoiceDate!=null ? formatZDate(acc.basicInfo.lastInvoiceDate).replace(/\s+/g, '') : 'N/A'));


                    // //Bill to contact details
                    $('.bill-to-contact-summary-table-output .output_bill_to_first_name').text(acc.billToContact.firstName);

                    $('.bill-to-contact-summary-table-output .output_bill_to_last_name').text(acc.billToContact.lastName);

                    $('.bill-to-contact-summary-table-output .output_bill_to_mailing_code').text(acc.billToContact.zipCode);

                    $('.bill-to-contact-summary-table-output .output_bill_to_mailing_address').text(acc.billToContact.address1);

                    $('.bill-to-contact-summary-table-output .output_bill_to_mailing_country').text(acc.billToContact.country);

                    $('.bill-to-contact-summary-table-output .output_bill_to_mailing_city').text(acc.billToContact.city);

                    $('.bill-to-contact-summary-table-output .output_bill_to_mailing_state').text(acc.billToContact.state);

                    $('.bill-to-contact-summary-table-output .output_bill_to_email').text(acc.billToContact.workEmail);

                    $('.bill-to-contact-summary-table-output .output_bill_to_phone').text(acc.billToContact.workPhone);


                    loadInputBillToContact();

                    //Sold to contact details
                    $('.sold-to-contact-summary-table-output .output_sold_to_first_name').text(acc.soldToContact.firstName);
                    
                    $('.sold-to-contact-summary-table-output .output_sold_to_last_name').text(acc.soldToContact.lastName);

                    $('.sold-to-contact-summary-table-output .output_sold_to_mailing_code').text(acc.soldToContact.zipCode);

                    $('.sold-to-contact-summary-table-output .output_sold_to_mailing_address').text(acc.soldToContact.address1);

                    $('.sold-to-contact-summary-table-output .output_sold_to_mailing_country').text(acc.soldToContact.country);

                    $('.sold-to-contact-summary-table-output .output_sold_to_mailing_city').text(acc.soldToContact.city);

                    $('.sold-to-contact-summary-table-output .output_sold_to_mailing_state').text(acc.soldToContact.state);

                    $('.sold-to-contact-summary-table-output .output_sold_to_email').text(acc.soldToContact.workEmail);

                    $('.sold-to-contact-summary-table-output .output_sold_to_phone').text(acc.soldToContact.workPhone);

                    loadInputSoldToContact(acc);
                    
                    //Display contact info:
                    if(acc.soldToContact.id == acc.billToContact.id){
                        var x=document.getElementById("sold_to_contact_info");
                        x.style.display='none';
                    }

                    getSubscriptionDetails(acc.subscriptions[0]);
                    getPaymentMethodSummary();

                    var htmlGreet = "";
                    htmlGreet = "<a style=\"float:right; color:#FE8402;text-transform: none;\"> Welcome " + accountName + "!";
                    $('.accountName').html(htmlGreet);
                    $('.accountName').show();

			var name = acc.billToContact.firstName+" "+acc.billToContact.lastName;
                    updateHeader(name);
                }
            }
        );
    }

    /**
     * loadInputBillToContact loads the input summary table with Bill To Contact details
     */
    function loadInputBillToContact(){

        document.getElementById('bill_to_first_name').value = document.getElementById('output_bill_to_fname').innerHTML;
        document.getElementById('bill_to_last_name').value = document.getElementById('output_bill_to_lname').innerHTML;
        document.getElementById('bill_to_code').value = document.getElementById('output_bill_to_zcode').innerHTML;
        document.getElementById('bill_to_address').value = document.getElementById('output_bill_to_addr').innerHTML;
        document.getElementById('bill_to_email').value = document.getElementById('output_bill_to_email').innerHTML;
        document.getElementById('bill_to_phone').value = document.getElementById('output_bill_to_phone').innerHTML;
        var country = document.getElementById('output_bill_to_country').innerHTML;
        //Load the value to Country drop down list
        $.ajax({
            type:"POST",
            async:false,
            url: "backend/index.php?type=GetISOCountryCode",
            data:{countryName: country} ,
            success: function(data){

                data = JSON.parse(data);
                document.getElementById('bill_to_country').value = data.msg[0];
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) { 

                console.log("Status: " + textStatus); console.log("Error: " + errorThrown); 
            } 
        });

        document.getElementById('bill_to_city').value = document.getElementById('output_bill_to_city').innerHTML;

        var state = document.getElementById('output_bill_to_state').innerHTML;
        if(country == "United States" || country == "Canada" || country == "US" || country == "CA")
        {

            //prepare drop down box for states
            $.ajax({
                type:"POST",
                url: "backend/index.php?type=GetISOStateCode",
                data:{countryName: country,
                      stateName: state} ,
                success: function(data){

                    data = JSON.parse(data);
                    if(data.success){
                        prepareState('bill_to_country', function() {    
                            setSateDropDown("bill_to_state", data.msg[0]); 

                        });
                    }
                },
                error: function(XMLHttpRequest, textStatus, errorThrown) { 
                    console.log("Status: " + textStatus);
                } 
            });
        } else {
            divA = document.getElementById('bill_to_stateD');
            divB = document.getElementById('bill_to_stateT');
            divB.value = state;
            divB.style.display = 'block';
            divA.style.display = 'none';
        }
    }

    

    /**
     * loadInputSoldToContact loads the input summary table for Sold To Contact
     */
    function loadInputSoldToContact(){

        document.getElementById('sold_to_first_name').value = document.getElementById('output_sold_to_fname').innerHTML;
        document.getElementById('sold_to_last_name').value = document.getElementById('output_sold_to_lname').innerHTML;
        document.getElementById('sold_to_code').value = document.getElementById('output_sold_to_zcode').innerHTML;
        document.getElementById('sold_to_address').value = document.getElementById('output_sold_to_addr').innerHTML;
	document.getElementById('sold_to_email').value = document.getElementById('output_sold_to_email').innerHTML;
        document.getElementById('sold_to_phone').value = document.getElementById('output_sold_to_phone').innerHTML;

        var country = document.getElementById('output_sold_to_country').innerHTML;
        //Load the value to Country drop down list
        $.ajax({
            type:"POST",
            async:false,
            url: "backend/index.php?type=GetISOCountryCode",
            data:{countryName: country} ,
            success: function(data){
                data = JSON.parse(data);
                document.getElementById('sold_to_country').value = data.msg[0];
                
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) { 
                console.log("Status: " + textStatus); 
            } 
        });
        document.getElementById('sold_to_city').value = document.getElementById('output_sold_to_city').innerHTML;

        var state = document.getElementById('output_sold_to_state').innerHTML;
        if(country == "United States" || country == "Canada" )
                {
                    $.ajax({
                        type:"POST",
                        url: "backend/index.php?type=GetISOStateCode",
                        data:{countryName: country,
                            stateName: state} ,
                        success: function(data){
                            data = JSON.parse(data);
                            if(data.success){
                                //prepare drop down box for states
                                prepareState('sold_to_country', function() {
                                    setSateDropDown("sold_to_state", data.msg[0]); 
                                });
                            }
                        },
                        error: function(XMLHttpRequest, textStatus, errorThrown) { 
                            console.log("Status: " + textStatus); console.log("Error: " + errorThrown); 
                        } 
                    });
                } else {

                    divA = document.getElementById('sold_to_stateD');
                    divB = document.getElementById('sold_to_stateT');
                    divB.value = state;
                    divB.style.display = 'block';
                    divA.style.display = 'none';
                    
                }

    }
    
    /**
     * getSubscriptionDetails displays the subscription information on to the screen
     * @param {JSON} data subscription data
     */
    function getSubscriptionDetails(data){
        //Display Start Date of Subscription
	//alert(data.ratePlans[0].ratePlanCharges[1].id);
	 $('.subscription_number').html(data.subscriptionNumber);
        $('.subscription_type').html(data.termType);
        $('.subscription_start_date').html(formatZDate(data.subscriptionStartDate).replace(/\s+/g, ''));
        $('.subscription_status').html(data.status);

        if(data.termType == "TERMED"){

            $('.subscription_initial_term').html(data.initialTerm + " Months");
            $('.subscription_renewal_term').html(data.renewalTerm + " Months");
        } else {

            $('.init_term').hide();
            $('.renew_term').hide();
            $('.subscription_initial_term').hide();
            $('.subscription_renewal_term').hide();
        }
        
        if(data.status == "Active"){
            $('.change_plan').show();
        }

        $.getJSON("backend/index.php?type=GetLatestSubscription",
            function(tempData){
                // alert('Callback called');
                if(!tempData.success) { 
                    alert("Error is: " + tempData.msg[0].msg);
                } else { 
                    var newData = JSON.parse(tempData.msg);

                    //Display active plans
                    var chosenPlansHtml = "";
                    var removedPlansHtml = "";

                    var subscription = newData.subscriptions[0];

                    for(var i in subscription.ratePlans){
                        var rp = subscription.ratePlans[i];
                        if (rp.lastChangeType != "Remove") {
                            chosenPlansHtml += "<li class='border_bottom_dashed'>";
                            chosenPlansHtml += "   <div class='rateplan_info'>";
                            chosenPlansHtml += "       <span class='rateplan_name'>"+rp.productName+" : "+rp.ratePlanName+"</span><br>";
                            chosenPlansHtml += "   </div><br/>";
                            chosenPlansHtml += "   <ul>";
                            for (var i in rp.ratePlanCharges) {
                                var charge = rp.ratePlanCharges[i];
                                chosenPlansHtml += "    <li>"+charge.name+"</li>";
	
				chosenPlansHtml += "    <li>&nbsp;&nbsp;&nbsp;Price: "+charge.mrr+"</li>";
				if(charge.quantity!=null)
				chosenPlansHtml += "    <li>&nbsp;&nbsp;&nbsp;Quantity: "+charge.quantity+"</li><br />";

                            }
                            chosenPlansHtml += "   </ul>";
                            chosenPlansHtml += "</li>";
                        } else {
             
                        }
                    }
                    $('.chosen_plans').html(chosenPlansHtml);
                }
            }
        );
    }

    
    /**
     * getUpdatedBillToContact fetches the Bill To Cotact details from the Input tags
     */
    function getUpdatedBillToContact(){

        var billToContact=new Array();
        //The order of the elements are based on the PHP index arrangement for 
        //BillTo Contact
        billToContact[0]=document.getElementById('bill_to_address').value;      
        billToContact[1]= "";  
        billToContact[2]=document.getElementById('bill_to_city').value;
        billToContact[3]=document.getElementById('bill_to_country').value; 
        billToContact[4]=document.getElementById('bill_to_phone').value; ;   
        billToContact[5]=document.getElementById('bill_to_code').value; 

        var country = document.getElementById('bill_to_country').value;

        if(country == 'CAN' || country == 'USA' || country == 'US' || country == 'CA'){

            billToContact[6]=document.getElementById('bill_to_stateD').value; 

        } else {

            billToContact[6]=document.getElementById('bill_to_stateT').value; 
        } 

        billToContact[7]=document.getElementById('bill_to_first_name').value; 
        billToContact[8]=document.getElementById('bill_to_last_name').value;
	billToContact[9]=document.getElementById('bill_to_email').value;  
        billToContact = JSON.stringify(billToContact);

        return billToContact;
    }

    /**
     * getUpdatedSoldToContact fetches the Sold To Cotact details from the Input tags
     */
    function getUpdatedSoldToContact(){

        var soldToContact=new Array();
        //The order of the elements are based on the PHP index arrangement for 
        //BillTo Contact
        soldToContact[0]=document.getElementById('sold_to_address').value;      
        soldToContact[1]=null;  
        soldToContact[2]=document.getElementById('sold_to_city').value;
        soldToContact[3]=document.getElementById('sold_to_country').value; 
        soldToContact[4]=document.getElementById('sold_to_phone').value;   
        soldToContact[5]=document.getElementById('sold_to_code').value; 

        var country = document.getElementById('sold_to_country').value;

        if(country == 'CAN' || country == 'USA' || country == 'US' || country == 'CA'){

            soldToContact[6]=document.getElementById('sold_to_stateD').value; 

        } else {

            soldToContact[6]=document.getElementById('sold_to_stateT').value; 

        } 

        soldToContact[7]=document.getElementById('sold_to_first_name').value; 
        soldToContact[8]=document.getElementById('sold_to_last_name').value;  
 soldToContact[9]=document.getElementById('sold_to_email').value;
        soldToContact = JSON.stringify(soldToContact);

        return soldToContact;
    }

    /*
     * Show bill to contact update success message
     */
    function billToContactSuccessMessage(){

        $.blockUI({
                message: $('div.bill_to_success_message'),
                fadeIn: 700,
                fadeOut: 700,
                timeout: 2000,
                showOverlay: false,
                centerY: false,
                css: {
                    width: '300px',
                    height: "35px",
                    bottom: '20px',
                    top: '-100',
                    left: '10px',
                    right: '',
                    border: 'non',
                    radius: '5px',
                    padding: '5px',
                    backgroundColor: '#000',
                    '-webkit-border-radius': '10px',
                    '-moz-border-radius': '10px',
                    opacity: .6,
                    color: '#fff',
                }
            });
    }

    /*
     * Show bill to contact update error message
     */
    function billToContactErrorMessage(){

        $.blockUI({
                message: $('div.bill_to_error_message'),
                fadeIn: 700,
                fadeOut: 700,
                timeout: 2000,
                showOverlay: false,
                centerY: false,
                css: {
                    width: '300px',
                    bottom: '20px',
                    top: '-100',
                    left: '10px',
                    right: '',
                    border: 'non',
                    radius: '5px',
                    padding: '5px',
                    backgroundColor: '#000',
                    '-webkit-border-radius': '10px',
                    '-moz-border-radius': '10px',
                    opacity: .6,
                    color: '#fff',
                }
            });
    }


    /**
     * When the user clicks the Save Changes button on the contact panel, update the Contact 
     * record with the user's preferences and return a message.
     */
    function updateBillToContact(){
        
        //Update Contact
        $.getJSON("backend/index.php?type=UpdateBillToContact", {billToContact : getUpdatedBillToContact()},
            function(data){
                 var responseMessage = data.msg[0];
                 responseMessage = JSON.parse(responseMessage);

                 if(responseMessage.success){
                    //If true, show success message, re-render panel
                //    $('.bill_to_success_message').text('Bill To Contact successfully saved!');
                //    $('.bill_to_success_message').show();
                    billToContactSuccessMessage();
                    //Set output fields to the same value as the input fields
                    $('.bill-to-contact-summary-table-output .output_bill_to_first_name').text(document.getElementById('bill_to_first_name').value);

                    if(document.getElementById('bill_to_country').value == "CAN" ||
                        document.getElementById('bill_to_country').value == "USA" || document.getElementById('bill_to_country').value == "US" ||
			document.getElementById('bill_to_country').value == "CA" )
                    {

                        $('.bill-to-contact-summary-table-output .output_bill_to_mailing_state').text($("#bill_to_stateD option:selected").text());

                    } else {

                        $('.bill-to-contact-summary-table-output .output_bill_to_mailing_state').text(document.getElementById('bill_to_stateT').value);

                    }
                    $('.bill-to-contact-summary-table-output .output_bill_to_last_name').text(document.getElementById('bill_to_last_name').value);
                    $('.bill-to-contact-summary-table-output .output_bill_to_mailing_code').text(document.getElementById('bill_to_code').value);
                    $('.bill-to-contact-summary-table-output .output_bill_to_mailing_address').text(document.getElementById('bill_to_address').value);
                    $('.bill-to-contact-summary-table-output .output_bill_to_mailing_country').text($("#bill_to_country option:selected").text());
                    $('.bill-to-contact-summary-table-output .output_bill_to_mailing_city').text(document.getElementById('bill_to_city').value);
                    $('.bill-to-contact-summary-table-output .output_bill_to_email').text(document.getElementById('bill_to_email').value);
                    $('.bill-to-contact-summary-table-output .output_bill_to_phone').text(document.getElementById('bill_to_phone').value);



                    $('.bill-to-contact-summary-table-input').hide();
                    $('.bill-to-contact-summary-table-output').fadeIn();    
                    $('.update_bill_to_contact').show();
                    $('.save_bill_to_changes').hide();
                    $('.cancel_bill_to_changes').hide();

                } else {

                 //   $('.bill_to_success_message').text(responseMessage.reasons[0].message);
                 //   $('.bill_to_success_message').show();
                    $('.bill_to_error_message').text(responseMessage.reasons[0].message);
                    billToContactErrorMessage();
                    $('.bill-to-contact-summary-table-input').show();
                    $('.bill-to-contact-summary-table-output').hide();  

                }
            }
        );
    }
    
    /*
     * Show bill to contact update success message
     */
    function soldToContactSuccessMessage(){

        $.blockUI({
                message: $('div.sold_to_success_message'),
                fadeIn: 700,
                fadeOut: 700,
                timeout: 2000,
                showOverlay: false,
                centerY: false,
                css: {
                    width: '300px',
                    height: "35px",
                    bottom: '20px',
                    top: '-100',
                    left: '10px',
                    right: '',
                    border: 'non',
                    radius: '5px',
                    padding: '5px',
                    backgroundColor: '#000',
                    '-webkit-border-radius': '10px',
                    '-moz-border-radius': '10px',
                    opacity: .6,
                    color: '#fff',
                }
            });
    }

    /*
     * Show bill to contact update error message
     */
    function soldToContactErrorMessage(){

        $.blockUI({
                message: $('div.sold_to_error_message'),
                fadeIn: 700,
                fadeOut: 700,
                timeout: 2000,
                showOverlay: false,
                centerY: false,
                css: {
                    width: '300px',
                    bottom: '20px',
                    top: '-100',
                    left: '10px',
                    right: '',
                    border: 'non',
                    radius: '5px',
                    padding: '5px',
                    backgroundColor: '#000',
                    '-webkit-border-radius': '10px',
                    '-moz-border-radius': '10px',
                    opacity: .6,
                    color: '#fff',
                }
            });
    }

    /** 
     * When the user clicks the Save Changes button on the contact panel, update the Contact 
     * record with the user's preferences and return a message.
     */
    function updateSoldToContact(){
        //Update Contact
        $.getJSON("backend/index.php?type=UpdateSoldToContact", {soldToContact : getUpdatedSoldToContact()},
            function(data){
                 var responseMessage = data.msg[0];
                 responseMessage = JSON.parse(responseMessage);

                 if(responseMessage.success){
                    //If true, show success message, re-render panel
                   // $('.sold_to_success_message').text('Sold to contact successfully saved!');
                   // $('.sold_to_success_message').show();
                    soldToContactSuccessMessage();
                    //Set output fields to the same value as the input fields
                    $('.sold-to-contact-summary-table-output .output_sold_to_first_name').text(document.getElementById('sold_to_first_name').value);
                    if(document.getElementById('sold_to_country').value == "CAN" ||
                        document.getElementById('sold_to_country').value == "USA" || document.getElementById('sold_to_country').value == "CA" ||
                        document.getElementById('sold_to_country').value == "US" )
                    {

                        $('.sold-to-contact-summary-table-output .output_sold_to_mailing_state').text($("#sold_to_stateD option:selected").text());

                    } else {

                        $('.sold-to-contact-summary-table-output .output_sold_to_mailing_state').text(document.getElementById('sold_to_stateT').value);

                    }
                    
                    $('.sold-to-contact-summary-table-output .output_sold_to_last_name').text(document.getElementById('sold_to_last_name').value);
                    $('.sold-to-contact-summary-table-output .output_sold_to_mailing_code').text(document.getElementById('sold_to_code').value);
                    $('.sold-to-contact-summary-table-output .output_sold_to_mailing_address').text(document.getElementById('sold_to_address').value);
                    $('.sold-to-contact-summary-table-output .output_sold_to_mailing_country').text($("#sold_to_country option:selected").text());
                    $('.sold-to-contact-summary-table-output .output_sold_to_mailing_city').text(document.getElementById('sold_to_city').value);    
                    $('.sold-to-contact-summary-table-output .output_sold_to_email').text(document.getElementById('sold_to_email').value);    
                    $('.sold-to-contact-summary-table-output .output_sold_to_phone').text(document.getElementById('sold_to_phone').value);    


                    $('.update_sold_to_contact').show();
                    $('.save_sold_to_changes').hide();
                    $('.cancel_sold_to_changes').hide();
                    $('.sold-to-contact-summary-table-input').hide();
                    $('.sold-to-contact-summary-table-output').fadeIn();
                    
                } else {

                    $('.sold_to_error_message').text(responseMessage.reasons[0].message);
                    //$('.sold_to_success_message').show();
                    soldToContactErrorMessage();
                    $('.sold-to-contact-summary-table-output').hide();
                    $('.sold-to-contact-summary-table-input').show();
                }
            }
        );
    }

    /**
     * cancelSoldToContact Cancels the change Sold To Contact information
     */
    function cancelSoldToContact(){

        $('.sold-to-contact-summary-table-output').fadeIn();
        $('.sold-to-contact-summary-table-input').hide();
        $('.update_sold_to_contact').show();
        $('.save_sold_to_changes').hide();
        $('.cancel_sold_to_changes').hide();
        loadInputSoldToContact();

    }

    /**
     * cancelBillToContact Cancels the change Sold To Contact information
     */
    function cancelBillToContact(){

        $('.bill-to-contact-summary-table-output').fadeIn();
        $('.bill-to-contact-summary-table-input').hide();
        $('.update_bill_to_contact').show();
        $('.save_bill_to_changes').hide();
        $('.cancel_bill_to_changes').hide();
        loadInputBillToContact();

    }

    /**
     * getPaymentMethodInfo(paymentMethodId)
    */
     function getPaymentMethodInfo(paymentMethodId){
        var pm;

        if(paymentMethods != null){
            
            for (var i in paymentMethods.creditCards){

                pm = paymentMethods.creditCards[i];
                if(paymentMethodId == pm.id){
                    break;
                }
            }
        }
        return pm;
     }
     
    /**
     * getPaymentMethodSummary fetches the payment methods available for the user logged in
     */
    function getPaymentMethodSummary(){

        $('.paymentmethod-summary-table').hide();
        $('.paymentmethod-summary .loading').show();

        $.getJSON("backend/index.php?type=GetPaymentMethodSummary",
            function(data){
                if(data.success){
                    var html = "";
                    var ps = JSON.parse(data.msg[0]);
                    paymentMethods = ps;
                    if(paymentMethods.creditCards.length > 0){
                        //For each payment method, print out
                        for(var i in ps.creditCards){
                            var pm = ps.creditCards[i];
                            html+="    <tr class='border_bottom_dashed' height= '70px;''>";
                            html+="        <td width='5%'>&#8227;</td>";
                            html+="        <td width='15%' class='vaTop'><b>"+pm.cardType+"</b></td>";
                            html+="        <td width='35%'>";
                            html+="            <span class='card_masked_number'>"+pm.cardNumber+"</span><br>";
                            html+="            Exp: <span class='card_expiration_month'>"+pm.expirationMonth+"</span>/<span class='card_expiration_year'>"+pm.expirationYear+"</span><br>";
                            html+="        </td>";
                            if(pm.defaultPaymentMethod==false){
                                html+="        <td width='50%' class='vaTop'><a href='javascript:' id='pm_update_"+pm.id+"' class='btn_submit item_button btn_make_default'>Make Default</a>";
                                html+="        <a href='javascript:' id='pm_remove_"+pm.id+"' class='btn_submit item_button btn_remove_default'>Remove Card</a></td>";
                            } else {
                                html+="        <td width='50%' class='vaTop'><b>[Default]</b></td>"; 
                                defaultCCNumberId = pm.cardNumber;     
                                defaultCCType = pm.cardType;       
                            }
                            html+="    </tr>";
                        }

                    } else {

                        html+="    <p style='margin-left:150px;'> <i>No payment methods attached to this account.</i></p>";
                    }
                    
                    $('.paymentmethod-summary-table').html(html);
                                    
                    $(".btn_make_default").click(function(event){
                        changeDefaultPm(event);
                    });
        
                    $(".btn_remove_default").click(function(event){
                        removePm(event);
                    });
                    $('.paymentmethod-summary-table').fadeIn('fast');
                }
            }
        );
    }
    
    /** 
     * updateContact prepares Update Bill To Cotact details
     */
    function updateContact(){

        $('.success_message').hide();
        $('.error_message').hide();
        $('.bill-to-contact-summary-table-output').hide();
        $('.bill-to-contact-summary-table-input').fadeIn('fast');
        $('.update_bill_to_contact').hide();
        $('.save_bill_to_changes').show();
        $('.cancel_bill_to_changes').show();
    }


    /**
     * prepareUpdateSoldToContact prepares Update Sold To Cotact details
     */
    function prepareUpdateSoldToContact(){

        $('.success_message').hide();
        $('.error_message').hide();
        $('.sold-to-contact-summary-table-output').hide();
        $('.sold-to-contact-summary-table-input').fadeIn('fast');
        $('.update_sold_to_contact').hide();
        $('.save_sold_to_changes').show();
        $('.cancel_sold_to_changes').show();

    }

    /**
     * confirm_payment shows the dialogue box with relevant information before the payment is made
     * @param {var} invNumber   Invoice number
     * @param {var} invBalance  Invoice balance
     * @param {var} invId       Invoice Id
     * @param {var} accId       Account Id
     */
    function confirm_paymentMethod_operations(invNumber, invBalance, invId, accId ) {
        bootbox.dialog({
          message: defaultCCType + " credit card number : " + defaultCCNumberId + " will be charged $"+ 
                    invBalance + " for payment of Invoice " + invNumber,
          title: "Confirm to proceed",
          buttons: {
            confirm: {
              label: "Confirm!",
              className: "btn-primary",
              callback: function() {
                pay_invoice( invId, accId);
              }
            },
            cancel: {
              label: "Cancel!",
              className: "btn-danger",
              callback: function() {
              }
            }
          }
        });
    }

    /**
     * changeDefaultPm Updates Default Payment Method for the logged in user
     */
    function changeDefaultPm(event){
        var buttonId = event.target.id;
        var pmId = buttonId.split('pm_update_')[1];
        var pmInfo = getPaymentMethodInfo(pmId);

        bootbox.dialog({
          message: "Are you sure you want to make " + pmInfo.cardType + "credit card number " +
                    pmInfo.cardNumber + " default?",
          title: "Confirm to proceed",
          buttons: {
            confirm: {
              label: "Confirm",
              className: "btn-primary",
              callback: function() {
                 $.getJSON("backend/index.php?type=UpdatePaymentMethod", {pmId:pmId},
                    function(data){
                        if(data.success){
                            paymentMethodUpdateSuccess();
                            getPaymentMethodSummary();

                        }
                        
                    } 
                );
              }
            },
            cancel: {
              label: "Cancel",
              className: "btn-danger",
              callback: function() {
              }
            }
          }
        });
       
    }
    
    /**
     * removePm removes Payment Method
     */
    function removePm(event){
        var buttonId = event.target.id;
        var pmId = buttonId.split('pm_remove_')[1];
        var pmInfo = getPaymentMethodInfo(pmId);

        bootbox.dialog({
          message: "Are you sure you want to remove " + pmInfo.cardType + " credit card number " +
                    pmInfo.cardNumber + " from your account?",
          title: "Confirm to proceed",
          buttons: {
            confirm: {
              label: "Confirm",
              className: "btn-primary",
              callback: function() {
                $.getJSON("backend/index.php?type=RemovePaymentMethod", {pmId:pmId},
                    function(data){
                        if(data.success){
                            paymentMethodDeleteSuccess();
                            getPaymentMethodSummary();

                        }
                    } 
                );
              }
            },
            cancel: {
              label: "Cancel",
              className: "btn-danger",
              callback: function() {
              }
            }
          }
        });
        
    }
    
    /**
     * showNewPaymentMethodPanel shows New payment method iframe
     */
    function showNewPaymentMethodPanel(){ 




        $.getJSON("backend/index.php?type=GetExistingIframeSrc", {AccountId : accountId},
            function(data){
                if(data.success){ 

                    var currenturl = data.msg[0];
                    //append the additional field
                    //Coppy the value of Address 1 text box

		    var name='';
		    var currenturl = currenturl.concat("&field_creditCardHolderName=");
                    currenturl = currenturl.concat(name);


                    var address1 = '';
                    
                    var currenturl = currenturl.concat("&field_creditCardAddress1=");
                    currenturl = currenturl.concat(address1);
                    
                    
                    //Coppy the value of city text box
                    var city = '';
                    
                    var currenturl = currenturl.concat("&field_creditCardCity=");
                    currenturl = currenturl.concat(city);
                    
                    var state = '';

                    state = state.trim();

                    if(state == ""){
                        state = '';
                    }
                     
    
                    var currenturl = currenturl.concat("&field_creditCardState=");
                    currenturl = currenturl.concat(state);
                    
                    //Coppy the value of postal code text box
                    var pcode = '';
                    
                    var currenturl = currenturl.concat("&field_creditCardPostalCode=");
                    currenturl = currenturl.concat(pcode);
                    
                    //Copy the value of Country drop down
                    var country = '';
                    
                    var currenturl = currenturl.concat("&field_creditCardCountry=");
                    currenturl = currenturl.concat(country);

		    var phone = '';
                    var currenturl = currenturl.concat("&field_phone=");
                    currenturl = currenturl.concat(phone);

		    var email = '';
                    var currenturl = currenturl.concat("&field_email=");
                    currenturl = currenturl.concat(email);

                    $('#z_hppm_iframe').attr("src",currenturl);


                    $('.save_pm').click(submitNewPaymentMethod);


var src = $('#z_hppm_iframe').contents().find("form").html();
                }
            }
        );
    }
    
    /**
     * submitNewPaymentMethod submits new payment method creation request
     */
    function submitNewPaymentMethod(){
        submitHostedPage('z_hppm_iframe');

    }

    /**
     * cancelNewPaymentMethod cancels new payment method window
     */
    function cancelNewPaymentMethod(){
        hideNewPaymentMethodPanel();
    }

    /**
     * hideNewPaymentMethodPanel hides new payment method window
     */
    function hideNewPaymentMethodPanel(){
        $("#infor.error_message").hide();
        $('.add_payment_method_panel').slideUp('fast');
        $('.new_pm').unbind('click');
        $('.cancel_pm').unbind('click');
        $("#z_hppm_iframe").attr('src', '');
        
    }

    /**
     * hostedpagecallback_success is called when the create new payment method is successful
     * @param {var} ref_id reference id of the hosted payment method created
     */
    function hostedpagecallback_success(ref_id) {

        submitNewPaymentMethod();
        getPaymentMethodSummary();

        hideNewPaymentMethodPanel();
        paymentMethodCreateSuccess();
    }

    /*
     * Show payment method create success message
     */
    function paymentMethodCreateSuccess(){

        $.blockUI({
                message: $('div.payment_method_created_message'),
                fadeIn: 700,
                fadeOut: 700,
                timeout: 5000,
                showOverlay: false,
                centerY: false,
                css: {
                    width: '300px',
                    height: "35px",
                    bottom: '20px',
                    top: '-100',
                    left: '10px',
                    right: '',
                    border: 'non',
                    radius: '5px',
                    padding: '5px',
                    backgroundColor: '#000',
                    '-webkit-border-radius': '10px',
                    '-moz-border-radius': '10px',
                    opacity: .6,
                    color: '#fff',
                }
            });
    
    }

     /*
     * Show payment method update success message
     */
    function paymentMethodUpdateSuccess(){
           $.blockUI({
            message: $('div.payment_method_update_message'),
            fadeIn: 700,
            fadeOut: 700,
            timeout: 5000,
            showOverlay: false,
            centerY: false,
            css: {
                width: '300px',
                height: "35px",
                bottom: '20px',
                top: '-100',
                left: '10px',
                right: '',
                border: 'non',
                radius: '5px',
                padding: '5px',
                backgroundColor: '#000',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                opacity: .6,
                color: '#fff',
            }
        });
    }

    /*
     * Show payment method create success message
     */
    function paymentMethodDeleteSuccess(){

        $.blockUI({
                message: $('div.payment_method_delete_message'),
                fadeIn: 700,
                fadeOut: 700,
                timeout: 5000,
                showOverlay: false,
                centerY: false,
                css: {
                    width: '300px',
                    height: "35px",
                    bottom: '20px',
                    top: '-100',
                    left: '10px',
                    right: '',
                    border: 'non',
                    radius: '5px',
                    padding: '5px',
                    backgroundColor: '#000',
                    '-webkit-border-radius': '10px',
                    '-moz-border-radius': '10px',
                    opacity: .6,
                    color: '#fff',
                }
            });
    
    }

    /**
     * hostedpagecallback_failure is called when there is an error while creating new payment method with the failure
     * information
     *
     * @param  {var} errorCode                            Error code
     * @param  {var} errorMessage                         Error message with description
     * @param  {var} errorField_creditCardType            
     * @param  {var} errorField_creditCardNumber          
     * @param  {var} errorField_creditCardExpirationMonth 
     * @param  {var} errorField_creditCardExpirationYear  
     * @param  {var} errorField_cardSecurityCode          
     * @param  {var} errorField_creditCardHolderName  
     */
    function hostedpagecallback_failure(errorCode, errorMessage, errorField_creditCardType, errorField_creditCardNumber,
            errorField_creditCardExpirationMonth, errorField_creditCardExpirationYear, errorField_cardSecurityCode,
            errorField_creditCardHolderName) {

        var html = '';

        html += formatErrorDisplay(errorCode, '');
        html += formatErrorDisplay(errorMessage, '');
        html += formatErrorDisplay(errorField_creditCardType, 'Card Type: ');
        html += formatErrorDisplay(errorField_creditCardNumber, 'Card Number: ');
        html += formatErrorDisplay(errorField_creditCardExpirationMonth, 'Card Expiration Month: ');
        html += formatErrorDisplay(errorField_creditCardExpirationYear, 'Card Expiration Year: ');
        html += formatErrorDisplay(errorField_cardSecurityCode, 'Card Security Code: ');
        html += formatErrorDisplay(errorField_creditCardHolderName, 'Card Holder Name: ');
        if(html!=''){
            html = 'Your credit card info was not saved for the following reasons.<br><ul>' + html;
            html += '</ul>';
        }

        bootbox.dialog({
          message: html,
          title: "Credit card info error",
          buttons: {
            confirm: {
              label: "Ok",
              className: "btn-primary",
              callback: function() {

                $('#myModal').modal('toggle');
              }
            },
            cancel: {
              label: "Cancel!",
              className: "btn-danger"
            }
          }
        });

       showNewPaymentMethodPanel();
    }

    /**
     * formatErrorDisplay formats the error message by prefixing information and
     * maintaining the correct intendation. Also if the error field is null then
     * it appends it with a message
     *
     * @param  {var} errorField [Field which has the error]
     * @param  {var} prefix     [Prefix message]
     * @return {var}            [Formatted message]
     */
    function formatErrorDisplay(errorField, prefix){
        var result = '';
        var displayError = errorField;
        if(displayError=='NullValue') displayError = "Missing required field.";
        if(errorField!=null && errorField!=''){
            result += '<li>' + prefix + displayError + '</li>';
        }
        return result;
    }

    /**
     * loadPaymentDetails fetches the payment history for the logged in user
     */
    function loadPaymentDetails(){

        $.getJSON("backend/index.php?type=GetPaymentHistory",
            function(data){
                if(data.success){
                    var pay = JSON.parse(data.msg[0]);
                    var html = "";
                    
                    if(pay.success){
                        if(pay.payments[0] != null){
                            html+= "<table class=\"table table-hover\">";

                            html+= "    <thead>";
                            html+= "    <tr id=\"table_heading\">";
                            html+= "        <td width='10%'><b>Reference Number</b></td>";
                            //html+= "        <td width='20%'><b>Account name</b></td>";
                            html+= "        <td width='10%'><b>Payment Date</b></td>";
                            html+= "        <td width='10%'><b>Type</b></td>";
                            html+= "        <td width='10%'><b>Amount</b></td>";
                            html+= "        <td width='10%'><b>Paid invoices</b></td>";
                            html+= "        <td width='15%'><b>Status</b></td>";
                            html+= "    </tr>"; 
                            html+= "    </thead>";  
                            for(var i in pay.payments){
                                html+= "    <tr id=\"table_data\">";
                                html+="        <td width='10%'>"+pay.payments[i].paymentNumber+"</td>";
                                //html+="        <td width='20%'>"+pay.payments[i].accountName+"</td>";
                                html+="        <td width='10%'>"+formatZDate(pay.payments[i].effectiveDate).replace(/\s+/g, '')+"</td>";
                                html+="        <td width='10%'>"+pay.payments[i].type+"</td>";
                                html+="        <td width='10%'>"+pay.payments[i].amount+"</td>";
                                var invoices = "";
                                for(var j in pay.payments[i].paidInvoices){

                                        invoices+= pay.payments[i].paidInvoices[j].invoiceNumber +" - $"+ pay.payments[i].paidInvoices[j].appliedPaymentAmount + "<br/>";
                                    
                                }
                                html+="        <td width='10%'>"+invoices+"</td>";
                                html+="        <td width='15%'>"+pay.payments[i].status+"</td>";
                                html+="    </tr>";
                            }
                            html+= "</table>";
                            $('.paytablediv').html(html);

                            var htmlPage = " ";
                            htmlPage+= "<ul class=\"pagination\">";
                            htmlPage+= "    <li class=\"disabled\" id=\"prev\" ><a id=\"prevButton\" onClick=\"\">&laquo;</a></li>";
                            htmlPage+= "    <li><a name=\"currentPayPage\" id=\"1\"><span class=\"sr-only\">page </span>1 </a></li>";
                            htmlPage+= "    <li class=\"disabled\" id=\"next\"><a id=\"nextButton\" onClick=\"\">&raquo;</a></li>";
                            htmlPage+= "</ul>";
                            $('.pagePayButton').html(htmlPage);

                            if(typeof pay.nextPage != 'undefined') {

                                //New pagination
                                document.getElementById("next").className = "";
                                document.getElementById('nextButton').onclick = function(){ 
                                    getNextPage();
                                };
                            }

                        } else {

                            html+= "<table class=\"table table-hover\">";
                            html+= "    <tr>";
                            html+= " <th> No Payment records available for this account </th>"
                            html+="    </tr>";
                            html+= "</table>";
                            $('.paytablediv').html(html);
                        }
                    }
                    else {
                        html+= "<table class=\"table table-hover\">";
                        html+= "    <tr>";
                        html+= " <th> No Payment records available at the moment</th>"
                        html+="    </tr>";
                        html+= "</table>";
                        $('.paytablediv').html(html);
                    }
            } else {
                html+= "<table class=\"table table-hover\">";
                html+= "    <tr>";
                html+= " <th> There was an error fetching your payment records. Please contact support! </th>"
                html+="    </tr>";
                html+= "</table>";
                $('.paytablediv').html(html);
            }
            showHistoryData();
        });
    }

    /**
     * Get the next page of payment history details of the logged in user
     */
    function getNextPaymentPage(){

        var currentPPage = document.getElementsByName("currentPayPage");

        nextPageNum = parseInt(currentPPage[0].id) + 1;
        getPaymentPage(nextPageNum);
    }

    /**
     * Get the previous page of payment history details of the logged in user
     */
    function getPreviousPaymentPage(){

        var currentPPage = document.getElementsByName("currentPayPage");
        var currPage = parseInt(currentPPage[0].id);
        if(currPage > 1){
            prevPageNum = currPage - 1;
            getPaymentPage(prevPageNum);
        } else {
            prevPageNum = 1;
            getPaymentPage(prevPageNum);
        }
    }

    /**
     * Get the payment history records of the given page number of the logged in user
     */
    function getPaymentPage(pageNumber){

        $.getJSON("backend/index.php?type=GetPaymentHistoryPage",
            {pageNum : pageNumber},
            function(data){
                if(data.success){
                    var pay = JSON.parse(data.msg[0]);
                    var html = "";

                    if(pay.success)
                    {
                        html+= "<table class=\"table table-hover\">";
                        html+= "    <tr id=\"table_heading\">";
                        html+= "        <td width='2%'><b>#</b></td>";
                        html+= "        <td width='10%'><b>Payment Number</b></td>";
                        //html+= "        <td width='20%'><b>Account name</b></td>";
                        html+= "        <td width='10%'><b>Payment Date</b></td>";
                        html+= "        <td width='10%'><b>Type</b></td>";
                        html+= "        <td width='10%'><b>Amount</b></td>";
                        html+= "        <td width='20%'><b>Paid invoices</b></td>";
                        html+= "        <td width='5%'><b>Status</b></td>";
                        html+= "    </tr>"; 
                        for(var i in pay.payments){
                            var index = i;      
                            var page = (pageNumber - 1) * 20;
                            ++index;
                            var ind = index + page;
                            html+= "    <tr id=\"table_data\">";
                            html+="        <td width='5%'>"+ ind +"</td>";
                            html+="        <td width='10%'>"+pay.payments[i].paymentNumber+"</td>";
                            //html+="        <td width='20%'>"+pay.payments[i].accountName+"</td>";
                            html+="        <td width='10%'>"+formatZDate(pay.payments[i].effectiveDate).replace(/\s+/g, '')+"</td>";
                            html+="        <td width='10%'>"+pay.payments[i].type+"</td>";
                            html+="        <td width='10%'>"+pay.payments[i].amount+"</td>";
                            var invoices = "";
                            for(var j in pay.payments[i].paidInvoices){

                                invoices+= pay.payments[i].paidInvoices[j].invoiceNumber +" - $"+ pay.payments[i].paidInvoices[j].appliedPaymentAmount + "<br/>";
                            }
                            html+="        <td width='25%'>"+invoices+"</td>";
                            html+="        <td width='5%'>"+pay.payments[i].status+"</td>";
                            html+="    </tr>";
                        }
                        html+=" </table>";
                        $('.paytablediv').html(html);
                        html+=" </table>";
                        $('.paytablediv').html(html);

                        html = " ";
                        html+= "<ul class=\"pagination\">";
                        html+= "    <li class=\"disabled\" id=\"prev\" ><a id=\"prevButton\" onClick=\"\">&laquo;</a></li>";
                        html+= "    <li><a name=\"currentPayPage\" id=\""+ pageNumber + "\"><span class=\"sr-only\">page </span>"+ pageNumber + " </a></li>";
                        html+= "    <li class=\"disabled\" id=\"next\"><a id=\"nextButton\" onClick=\"\">&raquo;</a></li>";
                        html+= "</ul>";
                        $('.pagePayButton').html(html);
                        
                        if(typeof pay.nextPage != 'undefined') {
                            
                            if(pageNumber == 1){

                                document.getElementById('prevButton').onclick = function(){ null;} ;
                                document.getElementById("prev").className = "disabled";
                            } else {
                                document.getElementById('prevButton').onclick = function(){ getPreviousPaymentPage();} ;
                                document.getElementById("prev").className = "";
                            }
                            document.getElementById('nextButton').onclick = function(){ getNextPaymentPage();} ;
                            document.getElementById("next").className = "";
                        } else {
                            document.getElementById("next").className = "disabled";
                            document.getElementById("prev").className = "";
                            document.getElementById('prevButton').onclick = function(){ getPreviousPaymentPage();} ;
                            document.getElementById('nextButton').onclick = function(){ null;} ;
                        }
                    } else {
                            html+= "<table class=\"table table-hover\">";
                            html+= "    <tr>";
                            html+= " <th> No Usage records available at the moment </th>"
                            html+="    </tr>";
                            html+= "</table";
                            $('.paytablediv').html(html);
                    }
                } else {
                    html+= "<table class=\"table table-hover\">";
                    html+= "    <tr>";
                    html+= " <th> There was an error fetching your payment records. Please contact support! </th>"
                    html+="    </tr>";
                    html+= "</table";
                    $('.paytablediv').html(html);
                }
            }
        );
    }

    /**
     * loadInvoiceDetails fetches the invoice history details of the user logged in
     * @param {boolean} pageLoad True if its the page is refreshed/loaded, False if other wise
     */
    function loadInvoiceDetails(pageLoad){
        $.getJSON("backend/index.php?type=GetInvoiceHistory",
            function(data){
                if(data.success){
                    var inv = JSON.parse(data.msg[0]);

                    var html = "";
                    if(inv.success){
                        if(inv.invoices[0] != null){

                            html+= "<table class=\"table table-hover\">";
                            html+= "    <thead>";
                            html+= "    <tr id=\"table_heading\">";
                            html+= "        <td width='10%'><b>Invoice Number</b></td>";
                           // html+= "        <td width='20%'><b>Account name</b></td>";
                            html+= "        <td width='10%'><b>Invoice Date</b></td>";
                            html+= "        <td width='10%'><b>Due Date</b></td>";
                            html+= "        <td width='10%'><b>Amount</b></td>";
                            html+= "        <td width='10%'><b>Balance</b></td>";
                            html+= "        <td width='15%'><b>Status</b></td>";
                            html+= "    </tr>"; 
                            html+= "    </thead>";  
                            for(var i in inv.invoices){
                                html+= "    <tr id=\"table_data\">";
                                html+="        <td width='10%'>"+inv.invoices[i].invoiceNumber+"</td>";
                              //  html+="        <td width='20%'>"+inv.invoices[i].accountName+"</td>";
                                html+="        <td width='10%'>"+formatZDate(inv.invoices[i].invoiceDate).replace(/\s+/g, '')+"</td>";
                                html+="        <td width='10%'>"+formatZDate(inv.invoices[i].dueDate).replace(/\s+/g, '')+"</td>";
                                html+="        <td width='10%'>$"+inv.invoices[i].amount+"</td>";
                                html+="        <td width='10%'>$"+inv.invoices[i].balance+"</td>";
                                html+="        <td width='15%'>"+inv.invoices[i].status+"</td>";
                                html+="    </tr>";
                            }

                            html+= "</table>";
                            $('.invtablediv').html(html);

                            var htmlPage = " ";
                            htmlPage+= "<ul class=\"pagination\">";
                            htmlPage+= "    <li class=\"disabled\" id=\"prevInv\" ><a id=\"prevInvButton\" onClick=\"\">&laquo;</a></li>";
                            htmlPage+= "    <li><a name=\"currentInvPage\" id=\"1\"><span class=\"sr-only\">page </span>1 </a></li>";
                            htmlPage+= "    <li class=\"disabled\" id=\"nextInv\"><a id=\"nextInvButton\" onClick=\"\">&raquo;</a></li>";
                            htmlPage+= "</ul>";
                            $('.pageInvButton').html(htmlPage);

                            if(typeof inv.nextPage != 'undefined') {

                                //New pagination
                                document.getElementById("nextInv").className = "";
                                document.getElementById('nextInvButton').onclick = function(){ 
                                    getNextInvoicePage();
                                };
                            }  
                        } else {
                            html+= "<table class=\"table table-hover\">";
                            html+= "    <tr>";
                            html+= " <th> No Invoice records available for this account </th>"
                            html+="    </tr>";
                            html+= "</table>";
                            $('.invtablediv').html(html);
                        }
                         

                    } else {

                        html+= "<table class=\"table table-hover\">";
                        html+= "    <tr>";
                        html+= " <th> No Invoice records available at the moment </th>"
                        html+="    </tr>";
                        html+= "</table>";
                        $('.invtablediv').html(html);
                            
                    }
                } else {
                    html+= "<table class=\"table table-hover\">";
                    html+= "    <tr>";
                    html+= " <th> There was an error fetching your invoice records. Please contact support! </th>"
                    html+="    </tr>";
                    html+= "</table>";
                    $('.invtablediv').html(html);
                }
                if(!pageLoad){
                    showHistoryData();
                }
            }
         );
    }

    /**
     * getNextInvoicePage gets the next page of invoice history data
     */
    function getNextInvoicePage(){

        var currentPPage = document.getElementsByName("currentInvPage");

        nextPageNum = parseInt(currentPPage[0].id) + 1;

        getInvoicePage(nextPageNum);
    }

    /**
     * getPreviousInvoicePage gets the previous page of invoice history data
     */
    function getPreviousInvoicePage(){

        var currentPPage = document.getElementsByName("currentInvPage");
        var currPage = parseInt(currentPPage[0].id);
        if(currPage > 1){
            prevPageNum = currPage - 1;

            getInvoicePage(prevPageNum);
        } else {
            prevPageNum = 1;

            getInvoicePage(prevPageNum);
        }
    }

    /**
     * getInvoicePage gets the invoice history records for the given page number 
     */
    function getInvoicePage(pageNumber){
        $.getJSON("backend/index.php?type=GetInvoiceHistoryPage",
            {pageNum : pageNumber},
            function(data){
                if(data.success){
                    var inv = JSON.parse(data.msg[0]);

                    var html = "";
                    if(inv.success){
                        html+= "<table class=\"table table-hover\">";
                        html+= "    <tr id=\"table_heading\">";
                        html+= "        <td width='2%'><b> </b></td>";
                        html+= "        <td width='10%'><b>Invoice Number</b></td>";
                        html+= "        <td width='20%'><b>Account name</b></td>";
                        html+= "        <td width='10%'><b>Invoice Date</b></td>";
                        html+= "        <td width='10%'><b>Due Date</b></td>";
                        html+= "        <td width='10%'><b>Amount</b></td>";
                        html+= "        <td width='10%'><b>Balance</b></td>";
                        html+= "        <td width='15%'><b>Status</b></td>";
                        html+= "    </tr>"; 
                        for(var i in inv.invoices){
                            var index = i;
                                    
                            var page = (pageNumber - 1) * 20;
                            ++index;
                            var ind = index + page;
                            html+= "    <tr id=\"table_data\">";
                            html+="        <td width='5%'>"+ ind +"</td>";
                            html+="        <td width='10%'>"+inv.invoices[i].invoiceNumber+"</td>";
                            html+="        <td width='20%'>"+inv.invoices[i].accountName+"</td>";
                            html+="        <td width='10%'>"+formatZDate(inv.invoices[i].invoiceDate).replace(/\s+/g, '')+"</td>";
                            html+="        <td width='10%'>"+formatZDate(inv.invoices[i].dueDate).replace(/\s+/g, '')+"</td>";
                            html+="        <td width='10%'>$"+inv.invoices[i].amount+"</td>";
                            html+="        <td width='10%'>$"+inv.invoices[i].balance+"</td>";
                            html+="        <td width='15%'>"+inv.invoices[i].status+"</td>";
                            html+="    </tr>";
                        }
                        html+=" </table>";
                        $('.invtablediv').html(html);
                        html = " ";
                        html+= "<ul class=\"pagination\">";
                        html+= "    <li class=\"disabled\" id=\"prevInv\" ><a id=\"prevInvButton\" onClick=\"\">&laquo;</a></li>";
                        html+= "    <li><a name=\"currentInvPage\" id=\""+ pageNumber + "\"><span class=\"sr-only\">page </span>"+ pageNumber + " </a></li>";
                        html+= "    <li class=\"disabled\" id=\"nextInv\"><a id=\"nextInvButton\" onClick=\"\">&raquo;</a></li>";
                        html+= "</ul>";
                        $('.pageInvButton').html(html);

                        if(typeof inv.nextPage != 'undefined') {
                        
                            if(pageNumber == 1){

                                document.getElementById('prevInvButton').onclick = function(){ null;} ;
                                document.getElementById("prevInv").className = "disabled";
                            } else {
                                document.getElementById('prevInvButton').onclick = function(){ getPreviousInvoicePage();} ;
                                document.getElementById("prevInv").className = "";
                            }
                            
                            document.getElementById('nextInvButton').onclick = function(){ getNextInvoicePage();} ;
                            document.getElementById("nextInv").className = "";
                            
                            
                        } else {
                            document.getElementById("nextInv").className = "disabled";
                            document.getElementById("prevInv").className = "";
                            document.getElementById('prevInvButton').onclick = function(){ getPreviousInvoicePage();} ;
                            document.getElementById('nextInvButton').onclick = function(){ null;} ;
                        }

                    } else {
                        html+= "<table class=\"table table-hover\">";
                        html+= "    <tr>";
                        html+= " <th> No Invoice records available at the moment </th>"
                        html+="    </tr>";
                        html+= "</table";
                        $('.invtablediv').html(html);
                    }
                } else {
                    html+= "<table class=\"table table-hover\">";
                    html+= "    <tr>";
                    html+= " <th> There was an error fetching your invoice records. Please contact support! </th>"
                    html+="    </tr>";
                    html+= "</table";
                    $('.invtablediv').html(html);
                }
            }
        );
    }

    /** 
     * loadMakePaymentDetails method fetches all the outstanding invoices of the 
     * logged in customer and provides a button where the user can pay the invoice 
     * balance 
     */
    function loadMakePaymentDetails(){

        $.getJSON("backend/index.php?type=getInvoiceForPayment",
            function(data){

                if(data.success){
                    var inv = data.msg;
                    var html = "";

                    html+= "<table class=\"table table-hover\">";
                    html+= "    <thead>";
                    html+= "    <tr id=\"table_heading\">";
                    html+= "        <td width='10%'><b>Invoice Number</b></td>";
                    html+= "        <td width='20%'><b>Account name</b></td>";
                    html+= "        <td width='10%'><b>Invoice Date</b></td>";
                    html+= "        <td width='10%'><b>Due Date</b></td>";
                    html+= "        <td width='10%'><b>Amount</b></td>";
                    html+= "        <td width='10%'><b>Balance</b></td>";
                    html+= "        <td width='15%'><b>Status</b></td>";
                    html+= "        <td width='15%'></td>";
                    html+= "    </tr>"; 
                    html+= "    </thead>";  
                    var invCount = 0;
                    if(inv != null && inv[0] != null){
                        for(var i in inv){
                            
                                html+= "    <tr id=\"table_data\" id=\""+ inv[i].id + "\">";
                                html+="        <td width='10%'>"+inv[i].invoiceNumber+"</td>";
                                html+="        <td width='20%'>"+inv[i].accountName+"</td>";
                                html+="        <td width='10%'>"+formatZDate(inv[i].invoiceDate).replace(/\s+/g, '')+"</td>";
                                html+="        <td width='10%'>"+formatZDate(inv[i].dueDate).replace(/\s+/g, '')+"</td>";
                                html+="        <td width='10%'>$"+inv[i].amount+"</td>";
                                html+="        <td width='10%'>$"+inv[i].balance+"</td>";
                                html+="        <td width='15%'>"+inv[i].status+"</td>";
                                html+="        <td width='15%'><button type=\"submit\" class=\"btn btn-update\" onClick=\"confirm_payment(\'" + inv[i].invoiceNumber + "\', \'" + inv[i].balance + "\', \'" + inv[i].id + "\', \'" + inv[i].accountId + "\');\" >Make Payment</button></td>";
                                html+="    </tr>";
                                invCount++;
                        }
                    } else {
                        if(invCount == 0){
                            html+= "    <tr>";
                            html+= "        <th colspan=\"8\"> No outstanding invoices. </th>"
                            html+= "    </tr>";
                        }
                    }
                    html+= "</table>";
                    //$('.makePaymentTablediv').html(html);
                    showAccountInfo();
                }
            }
        );
    }

    /**
     * showAccountInfo hides the loading symbol and shows all the account information on the 
     * html page
     */
    function showAccountInfo(){

        $('.loading').hide();
        $('.makePaymentTablediv').fadeIn('slow');
        $('.account-summary-table').fadeIn('slow');
        $('.contact-info').fadeIn('slow');
        $('.subscription-summary-table').fadeIn('slow');
        $('.paymentmethod-summary-table').fadeIn('slow');
        $('.transaction-history-data').fadeIn('slow');

    }

    /**
     * showHistoryData hides the loading symbol and shows the transaction history information on the 
     * html page
     */
    function showHistoryData(){

        $('.loading').hide();
        $('.transaction-history-data').fadeIn('slow');
    }

    /**
     * confirm_payment shows the dialogue box with relevant information before the payment is made
     * @param {var} invNumber   Invoice number
     * @param {var} invBalance  Invoice balance
     * @param {var} invId       Invoice Id
     * @param {var} accId       Account Id
     */
    function confirm_payment(invNumber, invBalance, invId, accId ) {
        bootbox.dialog({
          message: defaultCCType + " credit card number : " + defaultCCNumberId + " will be charged $"+ 
                    invBalance + " for payment of Invoice " + invNumber,
          title: "Confirm to proceed",
          buttons: {
            confirm: {
              label: "Confirm!",
              className: "btn-primary",
              callback: function() {
                pay_invoice( invId, accId);
              }
            },
            cancel: {
              label: "Cancel!",
              className: "btn-danger",
              callback: function() {
              }
            }
          }
        });
    }

    /**
     * pay_invoice pays the invoice amount using the default payment method attached to the account
     * @param {var} invId       Invoice Id
     * @param {var} accId       Account Id
     */
    function pay_invoice( invId, accId){

        $.getJSON("backend/index.php?type=payInvoice",
            {accountId : accId,
             invoiceId : invId},
            function(data){

                if(data.success){
                    data = JSON.parse(data.msg[0]);
                    var html = '';
                    if(data.success){
                        bootbox.dialog({
                            message: 'Your payment was successfully received. <br><br> Invoice Number    : ' + data.invoices[0].invoiceNumber +
                                        ' <br> Invoice Amount    : $' + data.invoices[0].invoiceAmount + ' <br> Reference Number : ' + data.paymentId,
                            title: "Payment Successful!",
                            buttons: {
                                confirm: {
                                    label: "OK",
                                    className: "btn-primary",
                                    callback: function() {

                                        window.location='account.php';
                                    }
                                }
                            }
                        });
                    } else {

                        var error = data.reasons[0];

                        bootbox.dialog({
                            message: 'Your payment was not processed due to the following error: <br><br>  ' + error.message,
                            title: "Payment Error!",
                            buttons: {
                                confirm: {
                                    label: "OK",
                                    className: "btn-primary",
                                    callback: function() {
                                        window.location='account.php';
                                    }
                                }
                            }
                        });
                    }
                }
            }
        );
    }

	

</script>

    <div id="bigContainer" style="height:280px;"> 
		<!-- <img style="position:absolute;left:70%;top:50px;height:250px;"src="images/zuora_watermark_home.png"/> -->
		
    </div>
    <!-- end header -->
    <div id="bigAccountContainer" style="background: images/noise.png; width = 100%">
        <div style="position:relative;top:-280px; width: 900px; height: 80%; margin: 0 auto; padding: 120px 0 40px;">
		<!-- new header -->
		<div class="text-center" style="overflow:hidden;">
			<h2 class="nwSbHdng" style="margin-top: 9px;text-align:center !important;font-size:20px;">Order Summary</h2>
		</div>
		<!-- /new header -->	    
            <ul class="tabs" data-persist="true">
                <li><a href="#account_detail">Account Details</a></li>
                <li><a href="#transaction_history">Transaction History</a></li>
                <!-- <li><a href="#make_payment">Make Payment</a></li> -->
            </ul>
            <div class="tabcontents">
                
                <div id="account_detail" style="height:100%; padding:30px;">
                    <div class="titleBox" style = "background-color: #FFFFFF;">
                    <heading><img src="images/InfoGreen.png" style="width:20px;height:22px;padding:0px 2px 2px 0px;margin-right:5px;">Account info:</heading>
                    <div class="loading"></div>
                    <div class="account-summary" style="margin-left:40px;">
                        <br>
                        <table class='account-summary-table' width="100%">
                            <tr >
                                <td id="table_cell_title">Account Name:</td>
                                <td id="table_cell_value"><span class='account_name'></span></td>
                                <td id="table_cell_title">Last Payment:</td>
                                <td id="table_cell_value"><span class='last_payment_amount'></span></td>
                            </tr>
                            <tr>
                                <td id="table_cell_title">Account Balance:</td>
                                <td id="table_cell_value"><span class='account_balance'></span></td>
                                <td id="table_cell_title">Last Payment Date:</td>
                                <td id="table_cell_value"><span class='last_payment_date'></span></td>
                            </tr>
                            <tr>
                                <td id="table_cell_title">Account Status:</td>
                                <td id="table_cell_value"><span class='account_status'></span></td>
                                <td id="table_cell_title">Last Invoice Date:</td>
                                <td id="table_cell_value"><span class='last_invoice_date'></span></td>
                            </tr>
                        </table>
                    </div>
                </div>
                <hr></hr>
                <div class="titleBox" style = "background-color: #FFFFFF;">
                <heading><img src="images/contact.png" style="width:20px;height:20px;padding:0px 2px 2px 0px;margin-right:5px;">Contact Info:</heading>
                <div class="success_message"></div>
                <div class="error_message"></div>
                <div class="loading">Hello...</div>
                <div class="contact-info">
                    <div id="bill_to_contact_info">
                        <table style="width:100%;margin-top:15px;">
                            <tr>
                                <td>
                                    <subheading>Bill To Contact: </subheading>
                                </td>
                                <td style="text-align:right;">
                                    <button type="submit" class="btn btn-update update_bill_to_contact" OnClick="updateContact();" >Edit</button>
                                    <button type="submit" class="btn btn-danger cancel_bill_to_changes" OnClick="cancelBillToContact();" >Cancel</button>
                                    <button type="submit" class="btn btn-update save_bill_to_changes" OnClick="updateBillToContact();" >Save</button>
                                </td>
                            </tr>
                        </table>
                        <div class="bill_to_success_message" style="display:none">
                            <p><img src="images/check48.png" alt="Smiley face" height="15" width="15">  Bill to contact Saved successfully!</p>
                        </div>
                        <div class="bill_to_error_message" style="display:none">
                        </div>
                        
                        <div class="summary-box bill-to-contact-summary" style="margin-left:40px;">
                        
                           <!-- <div class="loading"> loading... </div> -->
                            
                            <table class='bill-to-contact-summary-table-output' style="margin-top:20px; align:center;width:100%;">
                                <tr>
                                    <td id="table_cell_title">First Name:</td>
                                    <td id="table_cell_value"><span class='output_bill_to_first_name' id="output_bill_to_fname"></span></td>
                                    <td id="table_cell_title">Email:</td>
                                    <td id="table_cell_value"><span class='output_bill_to_email' id="output_bill_to_email"></span></td>

                                </tr>
                                <tr>
                                    <td id="table_cell_title">Last Name:</td>
                                    <td id="table_cell_value"><span class='output_bill_to_last_name' id="output_bill_to_lname"></span></td>
                                    <td id="table_cell_title">Phone:</td>
                                    <td id="table_cell_value"><span class='output_bill_to_phone' id="output_bill_to_phone"></span></td>

                                </tr>
                                <tr>
                                    <td id="table_cell_title">Address:</td>
                                    <td id="table_cell_value"><span class='output_bill_to_mailing_address' id="output_bill_to_addr"></span></td>
                                    <td id="table_cell_title"></td>
                                    <td id="table_cell_value"></td>
                                </tr>
                                <tr>
                                    <td id="table_cell_title">City:</td>
                                    <td id="table_cell_value"><span class='output_bill_to_mailing_city' id="output_bill_to_city"></span></td>
                                    <td id="table_cell_title"></td>
                                    <td id="table_cell_value"></td>
                                </tr>
				<tr>
				    <td id="table_cell_title">State:</td>
                                    <td id="table_cell_value"><span class='output_bill_to_mailing_state' id='output_bill_to_state'></span></td>
                                    <td id="table_cell_title"></td>
                                    <td id="table_cell_value"></td>
                                </tr>
				<tr>
                                    <td id="table_cell_title">Country:</td>
                                    <td id="table_cell_value"><span class='output_bill_to_mailing_country' id="output_bill_to_country"></span></td>
                                    <td id="table_cell_title"></td>
                                    <td id="table_cell_value"></td>
                                </tr>
				<tr>
				    <td id="table_cell_title">Postal Code:</td>
                                    <td id="table_cell_value"><span class='output_bill_to_mailing_code' id="output_bill_to_zcode"></span></td>
                                    <td id="table_cell_title"></td>
                                    <td id="table_cell_value"></td>
				</tr>
				<tr>
                                    <td id="table_cell_title"></td>
                                    <td id="table_cell_value"></td>
                                </tr>
                            </table>

                            <table class='bill-to-contact-summary-table-input'>
                                <tr>
                                    <td id="table_cell_title">First Name:</td>
                                    <td id="table_cell_value"><input type="text" class="bill_to_first_name" id="bill_to_first_name" /></td>
                                    <td id="table_cell_title">Email:</td>
                                    <td id="table_cell_value"><input type="text" class="bill_to_email" id="bill_to_email" /></td>
                                </tr>
                                <tr>
                                    <td id="table_cell_title">Last Name:</td>
                                    <td id="table_cell_value"><input type="text" class="last_name" id="bill_to_last_name" /></td>
                                    <td id="table_cell_title">Phone:</td>
                                    <td id="table_cell_value"><input type="text" class="bill_to_phone" id="bill_to_phone" /></td>
                                </tr>
                                <tr>
                                    <td id="table_cell_title">Address:</td>
                                    <td id="table_cell_value"><input type="text" class="mailing_address" id="bill_to_address" /></td>
                                    <td id="table_cell_title"></td>
                                    <td id="table_cell_value"></td>
                                </tr>
                                <tr>
                                    <td id="table_cell_title">City:</td>
                                    <td id="table_cell_value"><input type="text" class="mailing_city" id="bill_to_city" /></td>
                                    <td id="table_cell_title"></td>
                                    <td id="table_cell_value"></td>
                                </tr>
				<tr>
                                    <td id="table_cell_title">State:</td>
                                    <td id="table_cell_value">
                                        <input type="text" class="mailing_state" id="bill_to_stateT" />
                                        <select id="bill_to_stateD" style="display: none;">

                                        </select>
                                    </td>
                                    <td id="table_cell_title"></td>
                                    <td id="table_cell_value"></td>
                                </tr>
				<tr>
                                    <td id="table_cell_title">Country:</td>
                                    <td id="table_cell_value">
                                        <select id="bill_to_country" onchange="prepareState('bill_to_country')">

                                        </select>
                                    </td>
                                    <td id="table_cell_title"></td>
                                    <td id="table_cell_value"></td>
                                </tr>
				<tr>
                                    <td id="table_cell_title">Postal Code:</td>
                                    <td id="table_cell_value"><input type="text" class="mailing_code" id="bill_to_code" /></td>
                                    <td id="table_cell_title"></td>
                                    <td id="table_cell_value"></td>
                                </tr>


                            </table>
                        </div>
                    </div>
                    <div id="sold_to_contact_info">
                        <table style="width:100%; margin-top:30px;">
                            <tr>
                                <td>
                                    <subheading>Sold To Contact: </subheading>
                                </td>
                                <td style="text-align:right;">
                                    <button type="submit" class="btn btn-update update_sold_to_contact" OnClick="prepareUpdateSoldToContact();" >Edit</button>
                                    <button type="submit" class="btn btn-danger cancel_sold_to_changes" OnClick="cancelSoldToContact();" >Cancel</button>
                                    <button type="submit" class="btn btn-update save_sold_to_changes" OnClick="updateSoldToContact();" >Save</button>
                                </td>
                            </tr>
                        </table>
                        <div class="sold_to_success_message" style="display:none">
                            <p><img src="images/check48.png" alt="Smiley face" height="15" width="15">  Sold to contact Saved successfully!</p>
                        </div>
                        <div class="sold_to_success_message" style="display:none">
                        </div>
                        <div class="summary-box sold-to-contact-summary" style="margin-top:20px;margin-left:40px;">
                        
                           <!-- <div class="loading"> loading... </div> -->
                            
                            <table class='sold-to-contact-summary-table-output' id="output_sold_to_contact" style="align:center;width:100%;">
                                <tr>
                                    <td id="table_cell_title">First Name:</td>
                                    <td id="table_cell_value"><span class='output_sold_to_first_name' id="output_sold_to_fname"></span></td>
                                    <td id="table_cell_title">Email:</td>
                                    <td id="table_cell_value"><span class='output_sold_to_email' id='output_sold_to_email'></span></td>
                                </tr>
                                <tr>
                                    <td id="table_cell_title">Last Name:</td>
                                    <td id="table_cell_value"><span class='output_sold_to_last_name' id="output_sold_to_lname"></span></td>
                                    <td id="table_cell_title">Phone:</td>
                                    <td id="table_cell_value"><span class='output_sold_to_phone' id="output_sold_to_phone"></span></td>
                                </tr>
				<tr>
                                    <td id="table_cell_title">Address:</td>
                                    <td id="table_cell_value"><span class='output_sold_to_mailing_address' id="output_sold_to_addr"></span></td>
	  			    <td width="20%"></td>
                                    <td id="table_cell_value"></td>
				 </tr>
				<tr>
                                    <td id="table_cell_title">City:</td>
                                    <td id="table_cell_value"><span class='output_sold_to_mailing_city' id="output_sold_to_city"></span></td>
                                    <td width="20%"></td>
                                    <td id="table_cell_value"><b></b></td>
                                </tr>
				 <tr>
                                    <td id="table_cell_title">State:</td>
                                    <td id="table_cell_value"><span class='output_sold_to_mailing_state' id='output_sold_to_state'></span></td>
	  			    <td width="20%"></td>
                                    <td id="table_cell_value"></td>
				</tr>
				 <tr>
                                    <td id="table_cell_title">Country:</td>
                                    <td id="table_cell_value"><span class='output_sold_to_mailing_country' id="output_sold_to_country"></span></td>
	  			    <td width="20%"></td>
                                    <td id="table_cell_value"></td>
                                </tr>
                                <tr>
				    <td id="table_cell_title">Postal Code:</td>
                                    <td id="table_cell_value"><span class='output_sold_to_mailing_code' id="output_sold_to_zcode"></span></td>
	  			    <td width="20%"></td>
                                    <td id="table_cell_value"></td>
                                </tr>
                                
                            </table>

                            <table class='sold-to-contact-summary-table-input'>
                                <tr>
                                    <td id="table_cell_title">First Name:</td>
                                    <td id="table_cell_value"><input type="text" class="sold_to_first_name" id="sold_to_first_name" /></td>
                                    <td id="table_cell_title">Email:</td>
                                    <td id="table_cell_value"><input type="text" class="sold_to_email" id="sold_to_email" /></td>
                                </tr>
                                <tr>
                                    <td id="table_cell_title">Last Name:</td>
                                    <td id="table_cell_value"><input type="text" class="last_name" id="sold_to_last_name" /></td>
                                    <td id="table_cell_title">Phone:</td>
                                    <td id="table_cell_value"><input type="text" class="sold_to_phone" id="sold_to_phone" /></td>
                                </tr>
                                <tr>
                                    <td id="table_cell_title">Address:</td>
                                    <td id="table_cell_value"><input type="text" class="mailing_address" id="sold_to_address" /></td>
                                    <td ></td>
                                    <td id="table_cell_value"></td>
                                </tr>
                                <tr>
                                    <td id="table_cell_title">City:</td>
                                    <td id="table_cell_value"><input type="text" class="mailing_city" id="sold_to_city" /></td>
                                    <td ></td>
                                    <td id="table_cell_value"></td>
                                </tr>
				<tr>
                                    
                                    <td id="table_cell_title">State:</td>
                                    <td id="table_cell_value">
                                        <input type="text" class="mailing_state" id="sold_to_stateT" />
                                        <select id="sold_to_stateD" style="display: none;">

                                        </select>
                                    </td>
                                    <td ></td>
                                    <td id="table_cell_value"></td>
                                </tr>
                                <tr>
                                    <td id="table_cell_title">Country:</td>
                                    <td id="table_cell_value">
                                        <select id="sold_to_country" onchange="prepareState('sold_to_country')">

                                        </select>
                                    </td>
                                    <td ></td>
                                    <td id="table_cell_value"></td>
                                </tr>
                                <tr>
				   <td id="table_cell_title">Postal Code:</td>
                                    <td id="table_cell_value"><input type="text" class="mailing_code" id="sold_to_code" /></td>
                                    <td ></td>
                                    <td id="table_cell_value"></td>
                                </tr>

                            </table>
                        </div>
                    </div>
                </div>
                <hr></hr>

                

                <div class="titleBox">
                    <div class="payment_method_created_message" style="display:none">
                            <p><img src="images/check48.png" alt="Smiley face" height="15" width="15">  Payment method created successfully!</p>
                    </div>
                    <div class="payment_method_update_message" style="display:none">
                                <p><img src="images/check48.png" alt="Smiley face" height="15" width="15">  Payment method updated successfully!</p>
                    </div>
                    <div class="payment_method_delete_message" style="display:none">
                                <p><img src="images/check48.png" alt="Smiley face" height="15" width="15">  Payment method Deleted successfully!</p>
                    </div>
                    <table style="width:100%;">
                        <tr>
                            <td>
                                <heading><img src="images/DollarSign.png" style="width:20px;height:22px;padding:0px 2px 2px 0px;margin-right:5px;">Payment Methods: </heading>
                            </td>
                            <td style="text-align:right;">
                                <button type="submit" class="btn btn-update new_payment_method" data-toggle="modal" data-target="#myModal" onClick="showNewPaymentMethodPanel();" style="background: grey !important;" disabled>Create New</button>
                                <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                  <div class="modal-dialog">
                                    <div class="modal-content">
                                      <div class="modal-header">
                                        <button type="button" class="btn close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                        <h4 class="modal-title" id="myModalLabel" Style="text-align:left;">Credit card</h4>
                                      </div>
                                      <div class="modal-body">
                                       <iframe id='z_hppm_iframe' src="" style="zoom:0.60" frameborder="0" height="470" width="90%"></iframe>
                                      <div class="modal-footer">
                                        <button type="button" class="btn btn-default cancel_pm" data-dismiss="modal">Close</button>
                                        <button type="button" class="btn btn-primary save_pm" data-dismiss="modal">Save</button>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                            </td>
                        </tr>
                    </table>
                    <br>
                    <div class="loading"> loading... </div>
                    <div class="summary-box paymentmethod-summary" style="margin-left:70px;">
                    
                        <table class='paymentmethod-summary-table'>
                        </table>
                    </div>
                    <br>
                    <div class="innerBlock add_payment_method_panel">
                        <subheading>New Credit Card</subheading>
                        <div id="infor" class="error_message clear-block"></div>
                        <iframe id="z_hppm_iframe" frameborder="0" name="z_hppm_iframe" width="600" height="520" src="" ></iframe>
                        <br>
                        
                        <button type="submit" class="btn btn-update new_pm"  >Add</button>
                        <button type="submit" class="btn btn-update cancel_pm" >Cancel</button>
                    </div>
                   
                </div>
                <hr></hr>

                <div class="titleBox">
                    <table style="width:100%;">
                        <tr>
                            <td>
                                <heading><img src="images/zuora-small.png" style="width:20px;height:22px;padding:0px 2px 2px 0px;margin-right:5px;">Subscriptions: </heading>
                            </td>
                            <td style="text-align:right;">
                                <button class="btn btn-update change_plan" OnClick="location.href='amend.php'" >Change Plan</button>
                            </td>
                        </tr>
                    </table>

                    <div class="summary-box subscription-summary">
                    <br>
                    <div class="loading"> loading... </div>

                      <div class='subscription-summary-table' style="margin-left:40px;">
                          <table width="100%">
                                <tr>
				   <td id="table_cell_title">Subscription Number:</td>
                                    <td id="table_cell_value"><span class='subscription_number'></span></td>
                                    <td id="table_cell_title">Subscription Type:</td>
                                    <td id="table_cell_value"><span class='subscription_type'></span></td>
                                </tr>
                                <tr>
                                    <td id="table_cell_title">Start Date:</td>
                                    <td id="table_cell_value"><span class='subscription_start_date'></span></td>
                                    <td id="table_cell_title" class="init_term">Initial Term:</td>
                                    <td id="table_cell_value"><span class='subscription_initial_term'></span></td>
                                </tr>
                                <tr>
                                    <td id="table_cell_title">Status:</td>
                                    <td id="table_cell_value"><span class='subscription_status'></span></td>
                                    
                                    <td id="table_cell_title" class="renew_term">Renewal Term:</td>
                                    <td id="table_cell_value"><span class='subscription_renewal_term'></span></td>
                                </tr>
                            </table>
                            <br>
                            <div class="innerBlock">
                                <ul class="chosen_plans">
                                </ul>
                                <ul class="removed_plans">
                                </ul>
                           </div>
                        </div>
                    </div>
                </div>
            </div>
            </div>
            <div id="transaction_history">
                <br>
                <!--
                <div id="transaction_container" align="center">
                    <table>
                        <tr>
                            <td>
                                <div class="select-style" >
                                    <select id="transaction_select" onchange="getTransactionHistory(this.value)">
                                        <option value="invoice_history" selected> Invoice History </option>
                                        <option value="payment_history">Payment History</option>
                                        
                                    </select>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>   
                -->
                
                <div class="loading"></div>
                <div class="transaction-history-data" style="width:100%; float: center;">
                    <br>
                    <heading><h3>Invoice History</h3></heading>
                   <!--- <hr></hr>--->
                    <div class="invtablediv" style = "background-color: #FFFFFF;"></div>
                    <div class="pageInvButton" align="center"> </div>
                    <br>
                    <heading><h3>Payment History</h3></heading>
                   <!---- <hr></hr>--->
                    <div class="paytablediv" style = "background-color: #FFFFFF;"></div>
                    <div class="pagePayButton" align="center"> </div>
                    <br>
                    
                </div>  
            </div>
                <div id="make_payment">
                    <div id="content">
                        <br><br>
                        <div class="loading"></div>
                        <div class="makePaymentTablediv" style = "background-color: #FFFFFF;"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<input type="hidden" id="email" name="email" value="<?php echo $_POST['email']; ?>" ><br>
<input type="hidden" id="returnurl" name="returnurl" value="<?php echo $_POST['returnurl']; ?>" ><br>
<input type="hidden" id="domainurl" name="domainurl" value="https://<?php echo $domainurl[2]; ?>" ><br>
<input type="hidden" id="qumulateGroupUUID" name="qumulateGroupUUID" value="<?php echo $_POST['qumulateGroupUUID']; ?>" ><br>


    <!-- start footer -->
    <div id="footer"></div>
<script>
//Load header and footer
$(".loading").load("loading.html");
$("#footer").load("footer.html");

</script>

</body>
</html>
