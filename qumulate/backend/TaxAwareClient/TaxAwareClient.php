<?php

// include('./httpful.phar');

class TaxAwareClient {
	
	// SAP URL and basic authentication details
	/**** Based on the Working environment ****/
	public static $SAPProxuURI = "";
	private static $AuthToken =  "";	
	
	public function getItemData($itemRequestJson) {
		
	//X-SAPWebService-URL => $X-SAPWebService-URL
	/**** Based on the Working environment ****/
		$response = \Httpful\Request::post(self::$SAPProxuURI)->sendsJson()->body($itemRequestJson)->addHeader('Authorization', self::$AuthToken)->addHeader('X-SAPWebService-URL', '$X-SAPWebService-URL')->sendIt();

		if($response->code == 200)  
			return $response;
		else throw new Exception("Debug Response Object!!", $response);
		
	
	}

}

?>
