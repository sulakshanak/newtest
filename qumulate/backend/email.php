<?php

function sendEmail($email, $name, $message){
	include("config.php");
	//SMTP needs accurate times, and the PHP time zone MUST be set
	//This should be done in your php.ini, but this is how to do it if you don't have access to that
	date_default_timezone_set('Etc/UTC');

	require 'PHPMailer/PHPMailerAutoload.php';
	$message = $default_email_body.$message;
	//Create a new PHPMailer instance
	$mail = new PHPMailer();
	//Tell PHPMailer to use SMTP
	$mail->isSMTP();
	//Enable SMTP debugging
	// 0 = off (for production use)
	// 1 = client messages
	// 2 = client and server messages
	//$mail->SMTPDebug = 2;
	//Ask for HTML-friendly debug output
	//$mail->Debugoutput = 'html';
	//Set the hostname of the mail server
	$mail->Host = $default_smtp_host; //gmail SMTP server
	//Set the SMTP port number - likely to be 25, 465 or 587
	$mail->Port = $default_smtp_port;
	//Whether to use SMTP authentication
	$mail->SMTPAuth = $default_smtp_auth;
	$mail->SMTPSecure = $default_smtp_secure;                 // sets the prefix to the servier
	//Username to use for SMTP authentication
	$mail->Username = $default_smtp_username;
	//Password to use for SMTP authentication
	$mail->Password = $default_smtp_password;
	//Set who the message is to be sent from
	$mail->setFrom($defaul_email_notification_from, $defaul_email_notification_from_name);
	//Set an alternative reply-to address
	$mail->addReplyTo($defaul_email_notification_cc, $defaul_email_notification_cc_name);
	//Set who the message is to be sent to
	$mail->addAddress($email, $name);
	//Set the subject line
	$mail->Subject = $default_email_subject;
	//Read an HTML message body from an external file, convert referenced images to embedded,
	//convert HTML into a basic plain-text alternative body
	//$mail->msgHTML(file_get_contents('contents.html'), dirname(__FILE__));
	$mail->Body     = $message;
	$mail->WordWrap = $default_email_body_word_wrap;
	//Replace the plain text body with one created manually
	//$mail->AltBody = 'This is a plain-text message body';
	//Attach an image file
	//$mail->addAttachment('images/phpmailer_mini.png');

	//send the message, check for errors
	if (!$mail->send()) {
	    return "ERROR sending email";
	} else {
	    return "SUCCESS";	
	}

}
?>