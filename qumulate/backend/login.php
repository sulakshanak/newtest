<?php
	function __autoload($class){
	  @include('controller/' . $class . '.php');
	  @include('model/' . $class . '.php');
	}
	include('controller/Authentication.php');
	sec_session_start();

	try{
		
		$username = filter_input(INPUT_POST, 'email', FILTER_SANITIZE_STRING);
		$password = filter_input(INPUT_POST, 'hashP', FILTER_SANITIZE_STRING);

	} catch (Exception $e){

		loginFailure('Must supply email and password');
		return;

	}
	
	if($username==''){

		loginFailure('Must enter an email address');
		return;

	}

	$loginSuccess = validate($username, $password);

	if($loginSuccess){

		loginSuccess($username);

	} else {

		loginFailure('Invalid email');
		return;

	}

	//Set session header and redirect to account detail page
	function loginSuccess($username){

   		session_regenerate_id();
		header('Location: ../account.html');

	}

	function loginFailure($error){

		echo $error;
	}

	function validate($username, $password){

		$MySqlInst = new MysqlRequest();

		$mysqli = $MySqlInst->getDBInstance();

		if (login($username, $password, $mysqli) == true) {

			return true;

		} else {

			return false;
		}
	}

?>