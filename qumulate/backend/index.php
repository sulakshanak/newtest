<?php

/**
 * brief index.php is used as a REST layer to interface between the front end HTML files and backend controller methods.
 * Events can be triggered from this page, using "<Base URL>/backend/?type=<ActionName>"
 * 
 */


$debug = 1; //debug mode



if ($debug == 1) {
	ini_set('display_errors', 1);
	error_reporting(E_ALL);  
}

ini_set('display_errors', '0');     # don't show any errors...
error_reporting(E_ALL | E_STRICT);  # ...but do log them

@include('./config.php');
function __autoload($class){
	global $debug;
 	if ($debug == 1) {
 		error_log("__autoload is being called for " . $class);
 	}
 	@include('./model/' . $class . '.php');
    @include('./controller/' . $class . '.php');
	@include('./TaxAwareClient/' . $class . '.php');
	@include('./sfdc/' . $class . '.php');

}

//session_start();
include('controller/Authentication.php');
sec_session_start();
$errors = array();
$messages = null;
$out = true;

$dupRatePlan = null;

isset($_REQUEST['type']) ? dispatcher($_REQUEST['type']) : '';

function addErrors($field,$msg){
	global $errors;
	$error['field']=$field;
	$error['msg']=$msg;
	$errors[] = $error;
}

/**
 * dispatcher($type): Redirects calls to specific functions based on the $type variable
 * @param $type
 */
function dispatcher($type){
	switch($type) {
		case 'getConfiguration' : getConfigData();
			break;
		case 'prepareGoogleSignUp' : prepareGoogleSignUp();
			break;
		case 'RefreshCatalog' : refreshCatalog();
			break;
		case 'ReadCatalog' : readCatalog();
			break;
		case 'EmptyCart' : emptyCart();
			break;
		case 'GetInitialCart' : getInitialCart();
			break;
		case 'AddItemToCart' : addItemToCart();
			break;
		case 'passwordReset' : passwordReset();
			break;
		case 'RemoveItemFromCart' : removeItemFromCart();
			break;
		case 'GetNewIframeSrc' : getNewIframeSrc();
			break;
		case 'GetExistingIframeSrc' : getExistingIframeSrc();
			break;
		case 'SubscribeWithCurrentCart' : subscribe();
			break;
		case 'GetLatestSubscription' : GetLatestSubscription();
			break;
		case 'PreviewRemoveRatePlan' : previewRemoveRatePlan();
			break;
		case 'RemoveRatePlan' : removeRatePlan();
			break;
		case 'CancelSubscription' : cancelSubscription();
			break;
		case 'PreviewAddRatePlan' : previewAddRatePlan();
			break;
		case 'AddRatePlan' : addRatePlan();
			break;
		case 'AmendPreview' : amendPreview();
			break;	
		case 'AmendLoad' : amendLoad();
			break;
		case 'AmendExecute' : amendExecute();
			break;
		case 'Undo' : undo();
			break;
		case 'GetCancelOption' : GetCancelOption();
			break;
		case 'PreviewUpdateRatePlan' : previewUpdateRatePlan();
			break;
		case 'UpdateRatePlan' : UpdateRatePlan();
			break;
		case 'ReadCountry' : getCountries();
			break;
		case 'ReadUSState' : getUsState();
			break;
		case 'ReadCanadianState' : getCanadianState();
			break;
		case 'checkEmailAvailability' : checkEmailAvailability();
			break;
		case 'checkUserAvailability' : checkUserAvailability();
			break;
		case 'GetAccountSummary' : GetAccountSummary();
			break;
		case 'GetPaymentMethodSummary' : GetPaymentMethodSummary();
			break;
		case 'GetISOCountryCode' : getCountryCode();
			break;
		case 'GetISOStateCode' : getStateCode();
			break;
		case 'UpdateBillToContact' : UpdateContact('BillTo');
			break;	
		case 'UpdateSoldToContact' : UpdateContact('SoldTo');
			break;
		case 'GetInvoiceHistory' : getInvoiceHistory('firstPage');
			break;
		case 'GetInvoiceHistoryPage' : getInvoiceHistory('pageNum');
			break;
		case 'GetPaymentHistory' : getPaymentHistory('firstPage');
			break;
		case 'GetPaymentHistoryPage' : getPaymentHistory('pageNum');
			break;
		case 'GetUsageHistory' : getUsageHistory('firstPage');
			break;
		case 'GetUsageHistoryPage' : getUsageHistory('pageNum');
			break;
		case 'UpdatePaymentMethod' : UpdatePaymentMethod();
			break;
		case 'RemovePaymentMethod' : RemovePaymentMethod();
			break;
		case 'IsUserLoggedIn' : isUserLoggedIn();
			break;
		case 'payInvoice' : payInvoice();
			break;
		case 'getInvoiceForPayment' : getInvoiceForPayment();
			break;
		case 'calculateTax' : calculateTax();
			break;	
		case 'getSFDCAccountDetails': getSFDCAccountDetails();
			break;
		case 'previewSubscription' : previewSubscription();
			break;
		case 'previewSubscriptionTax' : previewSubscriptionTax();
			break;	
		case 'ClearAmendments' : clearAmendments();
			break;	
		case 'getSFDCContactDetails' : getSFDCContactDetails();
			break;
		case 'getOktaContactDetails' : getOktaContactDetails();
			break;
		case 'getBilltoDetails' : getBilltoDetails();
			break;
		case 'getBillToFromOkta' : getBillToFromOkta();
			break;
		case 'AddRatePlan1' : addRatePlan1();
			break;
		case 'getRedirectUrl' : getAccountNumber();
			break;
		default : addErrors(null,'no action specified');
	}
}

/**
 * Function to preview a new subscription
 */
function previewSubscriptionTax(){
	global $debug;
	global $messages;
	if ($debug == 1) {
		error_log("[Preview subscription]");
	}
	$cart = $_SESSION['cart'];
	$billTo = json_decode($_REQUEST['billTo']);
	error_log(print_r($billTo, true));
	$subPreview = new SubscriptionManager(null, $cart, null, $billTo, null, false, false);
	$result = $subPreview->PreviewSubscription();

	if ($debug == 1) {
		error_log(print_r($result, true));
	}

	$messages = $result;
}

/**
 * prepareGoogleSignUp Function makes appropriate calls to 
 * authenticate the user or if the user is not logged in then
 * assigns the google auth URL to the return variable.
 */
function prepareGoogleSignUp(){
	include('controller/GoogleAuth.php');
	global $messages;

	$code = null;
	if(isset($_REQUEST['code'])){

		$code = $_REQUEST['code'];
	} 	

	$messages = googleAuth($code);

	if(isset($messages["account"])){
		$_SESSION['email'] = $messages["account"];
	}
}

/**
 * Function to preview a new subscription
 */
 function calculateTax(){
	include('TaxAwareClient/httpful.phar');
	include('TaxAwareClient/TaxAwareClient.php');
	include ('controller/Date.php');

	global $debug;
	global $messages;
	if ($debug == 1) {
		error_log("[Calculate Tax]");
	}

		date_default_timezone_set('America/Los_Angeles');
		$date = date('Y-m-d');
		$today = getdate();
		$mday = $today['mday'];

    $accountId = $_REQUEST['accountId'];
	$quantity = $_REQUEST['quantity'];
	$unitPrice = $_REQUEST['unitPrice'];
	$grossAmount = $_REQUEST['grossAmount'];
	$streetName = $_REQUEST['street'];
	$city = $_REQUEST['city'];
	$state = $_REQUEST['state'];
	$country = $_REQUEST['country'];
	$postalCode = $_REQUEST['postalCode'];
	$taxDate = date(mdY);
	error_log("City");
	error_log(print_r($city, true));
	error_log(print_r($state, true));
	error_log(print_r($accountId, true));

	$taxPreview = new TaxAwareClient();
	$taxInputs = array('ITEM_Data' => array(array( 'ITEM_NO'=> "10", 'CUSTOMER_NO' => "0000197333", 'QUANTITY' => $quantity, 'UNIT_PRICE'=> $unitPrice, 'CURRENCY'=> "USD", 'GROSS_AMOUNT'=> $grossAmount, 'TAX_DATE'=> $date, 'STREET'=> $streetName, 'CITY'=> $city, 'COUNTY'=> "", 'STATE'=> $state, 'POSTAL_CODE'=> $postalCode, 'COUNTRY'=> $country, 'MAT_TAX_CODE'=> "01" )));
	$result = json_decode($taxPreview->getItemData(json_encode($taxInputs)));
		
	if ($debug == 1) {
			error_log("Printing Tax Data");
			error_log(print_r($result, true));
	}

	$messages = $result;
 }
 
function getSFDCAccountDetails() {
	global $debug;
	global $messages;
	if ($debug == 1) {
		error_log("[Retrieve SFDC Account Details]");
	}
    $accountNumber = $_REQUEST['SFDCAccountId'];


	$sfdcRes = sApi::createSfdcAccount($accountNumber);
		
    $messages = $sfdcRes;
}

function getOktaContactDetails() {
	global $debug;
	global $messages;
	if ($debug == 1) {
		error_log("[Retrieve SFDC Account Details]");
	}
    $accountNumber = $_REQUEST['SFDCAccountId'];
$qumulateGroupUUID = $_REQUEST['qumulateGroupUUID'];

$_SESSION['qumulateGroupUUID'] = $qumulateGroupUUID;
	$sfdcRes = sApi::fetchOktaContact($accountNumber);
		
    $messages = $sfdcRes;
}

function getBillToFromOkta()
{

	global $messages;

	include 'config.php';

	$oktaID = $_REQUEST['SFDCAccountId'];

	$qumulateGroupUUID = $_REQUEST['qumulateGroupUUID'];

	$SFDCsessionID = $_SESSION['sfdcsessionid'];

	$url = $sfdcrest.$oktaID;		

	$token = "Bearer ".$SFDCsessionID;

	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL,$url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_TIMEOUT, 4);
	curl_setopt($ch, CURLOPT_HTTPHEADER, array(
				'Authorization:'.$token,
				'Host: $sfdchost'
				));

	$data = curl_exec($ch); 

	$decoded = json_decode($data);
	
	if(curl_errno($ch))
	    print curl_error($ch);
	else
	    curl_close($ch);

	$messages = $decoded ;
		
}


function getBilltoDetails()
{
	global $debug;
	global $messages;
	if ($debug == 1) {
		error_log("[Retrieve Bill to Details]");
	}

    $ext_cust_id = $_REQUEST['Ext_Cust_Id__c'];

	$sfdcRes = sApi::fetchBillTo($ext_cust_id);
		
    $messages = $sfdcRes;
}

function getSFDCContactDetails() {
	global $debug;
	global $messages;
	if ($debug == 1) {
		error_log("[Retrieve SFDC Account Details]");
	}
    $accountNumber = $_REQUEST['SFDCAccountId'];


	$sfdcRes = sApi::createSfdcContact($accountNumber);
		
    $messages = $sfdcRes;
}


function previewSubscription(){
	global $debug;
	global $messages;
	if ($debug == 1) {
		error_log("[Preview subscription]");
	}
	$cart = $_SESSION['cart'];

	if (count($cart->cart_items) == 0) {
		error_log('Cart Is Empty: '.print_r($cart,true));
		addErrors(null,'CART_EMPTY');
	} else {
		$subPreview = new SubscriptionManager(null, $cart, null, null, null, false, false);
		$result = $subPreview->PreviewSubscription();

		if ($debug == 1) {
			error_log("Printing Test previewSubscription");
			error_log(print_r($result, true));
		}

		$messages = $result;
	}	
}

/** 
 * Function to pay the invoice amount
 */
function payInvoice(){
	global $messages, $debug;
	if ($debug == 1) {
		error_log("{pay Invoice}");
	}
	$invoiceId = $_REQUEST['invoiceId'];
	$accountId = $_REQUEST['accountId'];
	$data = array("accountKey" => $accountId,
				  "invoiceId" => $invoiceId);

	$result = PaymentManager::payInvoice($data);

	if ($debug == 1) {
		error_log($result);
	}

	$messages = $result;

} 

/**
 * Function to get Invoices for payment processing
 */
function getInvoiceForPayment(){
	global $messages;
	$email = $_SESSION['email'];

	$response = AccountManager::getInvoiceForPayment($email);

	$messages = $response;
}

/**
 * Function to check if the user is logged in 
 */
function isUserLoggedIn(){
	global $messages;

	$email = $_SESSION['email'];

	if($email == "")
	{
		$_SESSION['email'] = $_REQUEST['email'];
	}
	error_log("IsuserloggedIn");

	if(isset($_SESSION['email'])){

		if(empty($_SESSION['email']))
		{
			error_log("Email session not set");
			addErrors(null,"SESSION_NOT_SET");
		}
		else{
			error_log("Yes");
			$messages = true;
		}
	} else {
		error_log("Email session not set");

		addErrors(null,"SESSION_NOT_SET");

	}


}

/**
 * Function that removes the payment method for
 * that particular account 
 */
function RemovePaymentMethod(){

	global $messages;

	$pmId = $_REQUEST['pmId'];

	$email = $_SESSION['email'];
	
	$updRes = PaymentManager::RemovePaymentMethod($email, $pmId);
	
	$messages = $updRes;

	$resStatus = json_decode($updRes);
	
	if(!$resStatus->success){
		addErrors("UpdatePaymentMethod","Payment method update failed");
	}
}


/** 
 *  Function that updates the payment method as default payment method for
 *	that particular account 
 */
function UpdatePaymentMethod(){
	global $messages;
	global $debug;

	$pmId = $_REQUEST['pmId'];

	$email = $_SESSION['email'];
	
	$updRes = PaymentManager::changeDefaultPaymentMethod($email, $pmId);

	if ($debug == 1) {
		error_log('UpdatePaymentMethod result: ' . print_r($updRes, true));
	}

	$messages = $updRes;

	$resStatus = json_decode($updRes);

	if(!$resStatus->success){
		addErrors("UpdatePaymentMethod","Payment method update failed");
	}
	
}

/**
 *	Function that fetches the Usage details of a particular
 *	Customer acocunt
 *  @param $page page number
 */
function getUsageHistory($page){
	global $messages;
	$email = $_SESSION['email'];

	if($page == "firstPage"){ //Get the fist page

		$response = AccountManager::getUsageHistory($email);

	} else if ($page == "pageNum") { //Get the page based on the page number

		$pageNum = $_REQUEST['pageNum'];
		$email = $email . "?page=" . $pageNum;
		$response = AccountManager::getUsageHistory($email);

	}

	if(!$response){
		addErrors("getUsageHistory","getUsageHistory not successful");
	}

	$messages = $response;
}


/**
 *	Function that fetches the Past payments of a particular
 *	Customer acocunt
 *  @param $page page number
 */
function getPaymentHistory($page){
	global $messages;
	$email = $_SESSION['email'];

	if($page == "firstPage"){ //Get the fist page

		$response = AccountManager::getPaymentHistory($email);

	} else if ($page == "pageNum") { //Get the page based on the page number

		$pageNum = $_REQUEST['pageNum'];
		$email = $email . "?page=" . $pageNum;
		$response = AccountManager::getPaymentHistory($email);

	}

	if(!$response){
		addErrors("getPaymentHistory","getPaymentHistory not successful");
	}

	$messages = $response;
}


/**
 *	Function that fetches the Past invoices of that particular
 *	Customer acocunt
 *  @param $page page number
 */
function getInvoiceHistory($page){
	global $messages;
	$email = $_SESSION['email'];
	//error_log($page);
	if($page == "firstPage"){ //Get the fist page

		$response = AccountManager::getInvoiceHistory($email);

	} else if ($page == "pageNum") { //Get the page based on the page number

		$pageNum = $_REQUEST['pageNum'];
		$email = $email . "?page=" . $pageNum;
		$response = AccountManager::getInvoiceHistory($email);
	}
	
	if(!$response){
		addErrors("getPaymentHistory","getPaymentHistory not successful");
	}

	$messages = $response;
}

/**
 *  Function that make a call to the Account Manager class to update
 *	the contact details 
 *  @param $contactType eg. BillTo or SoldTo
 */
function updateContact($contactType){
	global $messages;
	$email = $_SESSION['email'];

	if($contactType == "BillTo"){

		$contactDetails = $_REQUEST['billToContact'];
		$billTo = json_decode($contactDetails);
		$subs = new SubscriptionManager(null, null, null, $billTo, null, false, false);
		$response = $subs->updateBillToContact($email);

	} else {

		$contactDetails = $_REQUEST['soldToContact'];
		$soldTo = json_decode($contactDetails);
		$subs = new SubscriptionManager(null, null, null, null, $soldTo, false, false);
		$response = $subs->updateSoldToContact($email);

	}
	//Assign the response to the messages are return the value using output() function
	$messages = $response;

}

/**
 *  Function that returns the Country code when country name
 *	is passed 
 */
function getCountryCode(){
	include('./model/Countries.php');
	global $messages;

	$countryName = $_REQUEST['countryName'];
	$isoCode = array_search($countryName, $countries);

	if(!$isoCode){
		addErrors("ISO_Country_code",'ISO country code is null');
	}

	$messages = $isoCode;

}

/**
 *  Function that returns the Country code when country name
 *  is passed 
 */
function getStateCode(){

	include('./model/states.php');
	global $messages;

	$countryName = strtoupper($_REQUEST['countryName']);
	$stateName = $_REQUEST['stateName'];
	if($countryName == "CANADA"){

		$isoCode = array_search($stateName, $CANADIAN_STATES); 

	} else {

		$isoCode = array_search($stateName, $US_STATES);
	}
	
	if(!$isoCode){
		addErrors("ISO_State_code",'ISO State code is null');
	}

	$messages = $isoCode;

}

function passwordReset(){
	global $messages;

	$email = $_REQUEST['email'];

	$dbInst = new MysqlRequest();
	$mysqli = $dbInst->getDBInstance();

	error_log("passwordReset");

	$messages = resetPassword($email, $mysqli);	//Authentication.php
	error_log("Password reset done . " . $messages);

}
/**
 *  GetPaymentMethodSummary() Get payment method details by calling the account manager class
 *  @return response body of Rest Request
 */
function GetPaymentMethodSummary(){
	global $messages;

	$email = $_SESSION['email'];

	$summary = AccountManager::getPaymentMethodDetail($email);

	$messages = $summary;

	$resStatus = json_decode($summary);

	if(!$resStatus->success){
		addErrors("GetPaymentMethodSummary",'Get Payment Method Summary is not successful');
	}
}

/**
 *	GetAccountSummary() method makes the call to AccountManager class and
 *	returns the account details
 *  @return response body of Rest Request
 */
function GetAccountSummary(){
	global $messages;

	//Changed for testing
	 $email = $_SESSION['email'];

	//$email = TestAccountEstuate;
	$summary = AccountManager::getAccountDetail($email);

	error_log("Printing Test Account Summary");
			error_log(print_r($summary, true));

	$messages = $summary;

}

/**
 * getCountries() reads the country data and returns the array back to the AJAX call
 * @return list of countries (JSON)
 */
function getCountries(){	
	include('./model/Countries.php');
	
	global $messages;
		
	$result = json_encode($countries);


	$messages = $result ;
}

/**
 * getUsState() fetches the list of US states 
 * @return JSON data of all US states
 */
function getUsState(){
	include('./model/states.php');

	global $messages;

	//echo json_encode($US_STATES);
	$messages = $US_STATES;

}

/**
 * getUsState() fetches the list of US states 
 * @return JSON data of all US states
 */
function getConfigData(){
	include('./model/config_data.php');

	global $messages;

	//echo json_encode($US_STATES);
	$messages = $configData;

}

/**
 * getCanadianState() fetches the list of Canadian states
 * @return JSON data of all canadian states 
 */

function getCanadianState(){
	include('./model/states.php');
	global $messages;

	//echo json_encode($CANADIAN_STATES);

	$messages = $CANADIAN_STATES;
}

/** 
 *	Checks Zuora to see if email is already present
 *	@return true if email is not present in zuora
 *			false if email is present in zuora	
 */
function checkEmailAvailability(){
	global $messages;
	$email = $_REQUEST['email'];

	$result = AccountManager::checkEmailAvailability($email);
	
	$messages = $result;
}

/** 
 *	Checks Zuora to see if email is already present
 *	@return true if email is not present in zuora
 *			false if email is present in zuora	
 */
function checkUserAvailability(){
	//include("controller/Authentication.php");
	global $messages;
	$email = $_REQUEST['email'];

	$mySQL = new MysqlRequest();

	$dbInstance = $mySQL->getDBInstance();

	$result = userExist($email, $dbInstance);
	
	$messages = $result;
}

/**
 *  subscribe() Makes call to SubscriptionManager class method subscribe() to create account
 *  and subscriptions 
 *	@return response body (JSON)
 */
function subscribe(){
	global $messages;
	//Fetch data from Request variables
	$cart = $_SESSION['cart'];
	$email = filter_input(INPUT_GET, 'email', FILTER_SANITIZE_STRING);
	$password = filter_input(INPUT_GET, 'pass', FILTER_SANITIZE_STRING);
	$billTo = json_decode($_REQUEST['billToContact']);
	$sameBillToSoldTo = $_REQUEST['soldToBillToSame'];
	$crmId = $_REQUEST['crmId'];
	$billingNumber = $_REQUEST['billingNumber'];

	error_log("Email (JI )  " . $email);
	error_log("Password (JI ) " . $password);
	error_log("Bill To" . $billTo);
	error_log("Flag" .$sameBillToSoldTo);
	
	if($sameBillToSoldTo == "true"){
		$soldTo = json_decode($_REQUEST['billToContact']);
	} else {
		$soldTo = json_decode($_REQUEST['soldToContact']); 
	}
	$billMeLater = $_REQUEST['billMeLater'];

	$googleUser = false;
	if(isset($_SESSION['googleUser'])){
		$googleUser = true;
	}
	$subs = new SubscriptionManager($_REQUEST['refId'], $cart, $email, $billTo, $soldTo, $billMeLater,$crmId, $billingNumber );
	

	$subsResult = $subs->subscribe();
	
	//Set the email address to Session variable 'email'
	error_log($subsResult);
	$response = json_decode($subsResult);
	

	error_log("step 1");
    // Test Code.
	//return $response;
	
	if($response->success){
    		$_SESSION['email'] = $response->accountNumber;
    		$_COOKIE['email'] = $response->accountNumber;
    	} else {
    		error_log("JI - subs not created");
    	}
	error_log(print_r($response, true));
	
	$messages = $subsResult;

	/*error_log(print_r($response, true));
	if($response->success){
		$register = new RegistrationManager();

		$mySQL = new MysqlRequest();

		$dbInstance = $mySQL->getDBInstance();

		$_SESSION['email'] = $response->accountNumber;
		$_COOKIE['email'] = $response->accountNumber;
		error_log("Step 2");
		if($googleUser){
			error_log("Registering google user");
			$register->registerGoogleUser($response->accountNumber, $dbInstance);
			
			
		} else {
			error_log('Registering regular user');
			$register->registerUser($response->accountNumber, $email, $password, $dbInstance);
			//$_SESSION['email'] = $email;
			//$_COOKIE['email'] = $email;
		}

	} else {

		error_log("JI - subs not created");
	}
	
	
	$messages = $subsResult;*/
}

/**
 * Retrieves current subscription 
 * @param 
 * @return Current Subscription in Array Format.
 */
function getLatestSubscription(){
	include('./config.php');
 	global $messages;
 	
 	$sub = SubscriptionManager :: getCurrentSubscription($_SESSION['email']);

 	$amResObject = json_decode(json_encode($sub), false);
	// error_log("amResDecode is: " . print_r($amResObject, true), 0);

	if($amResObject->success) {
		$messages = $amResObject->payload;
	}
	else {
		$errors = $amResObject->payload;
		addErrors('getLatestSubscription', $errors);
	}
}

function undo(){
	global $messages;

	$rpId = $_REQUEST['undoId'];
	error_log("undo rpId is: " . $rpId);

	if(isset($_SESSION['cart'])){
		error_log("Cart update before undo" . print_r($_SESSION['cart']->amendments, true), 0);
		
		foreach($_SESSION['cart']->amendments as $i=>$items){
			if($items->ratePlanId == $rpId){
				unset($_SESSION['cart']->amendments[$i]);
				$_SESSION['cart']->amendments = array_values($_SESSION['cart']->amendments);

				error_log($rpId . " was unset in amendments!");
			}	//end if
		}	//end for
		error_log("Cart after undo" . print_r($_SESSION['cart']->amendments, true), 0);
	}	//end if

	// session_regenerate_id();
	global $out;
	$out = false;

}

function amendLoad(){	//This will be called each time someone takes an amend action.
	global $messages;
	require_once('./controller/Date.php');
	require_once('./controller/Amender.php');
	require_once('./controller/SubscriptionManager.php');
	include('./config.php');	

	if(!isset($_SESSION['cart'])){
		emptyCart();
	}

	$date = calcDate($defaultAmendCED);
	$amendVerb = $_REQUEST['amendVerb'];
	$ratePlanId = $_REQUEST['ratePlanId'];
	$ratePlanName = $_REQUEST['ratePlanName']; 
	$productName = $_REQUEST['productName'];
	
	$sub = SubscriptionManager :: getCurrentSubscription($_SESSION['email']);
	$subResObject = json_decode(json_encode($sub), false);
	$subObject = $subResObject->payload;
	$subObjectDecode = json_decode($subObject);
	error_log("subObjectDecode is: " . print_r($subObjectDecode, true), 0);

	$removeDate = Amender::getRemoveProductDate($_SESSION['email'], $ratePlanId, $subObjectDecode);
	if ($removeDate == null) {
		$removeDate = date('Y-m-d');
	}

	if(isset($_SESSION['cart'])){
		if($amendVerb == 'add') {
			$quantityChargeArray = json_decode($_REQUEST['quantity']); 
			error_log("quantityChargeArray is: " . print_r($quantityChargeArray, true), 0);
			$_SESSION['cart']->addNewAmend($ratePlanId, $ratePlanName, $productName, $quantityChargeArray, $date);
			//Forms the String 'add' to fill data Encode
		}
		else if ($amendVerb == 'update') {
			$quantityChargeArray = json_decode($_REQUEST['quantity']); 
			error_log("quantityChargeArray is: " . print_r($quantityChargeArray, true), 0);
			$_SESSION['cart']->addUpdateAmend($ratePlanId, $ratePlanName, $productName, $quantityChargeArray, $date);
		}
		else if($amendVerb == 'remove') {
			error_log("removeDate for addRemoveAmend is: " . $removeDate);
			$_SESSION['cart']->addRemoveAmend($ratePlanId, $ratePlanName, $productName, $removeDate);
		}
	}
	else {
		addErrors(null,'Cart has not been set up.');
		return;	
	}

	error_log("Cart Contents are: " . print_r($_SESSION['cart'], true), 0);
	$messages = $_SESSION['cart'];
}

function amendExecute(){
	require_once('./controller/Amender.php');
	error_log("Amend Execute");
	
	$batchSize = 9;	//default batch size.

	for($i = 0; $i < sizeof($_SESSION['cart']->amendments); $i+=$batchSize){
		//Starting from the first element iterate by 9 each time.
		$beginIndex = $i;	//beginIndex is 0, 9, 18 etc...
		$endIndex = $i + $batchSize;	//endIndex is whatever beginIndex is + 9, 
										//to close the max number in the batch.
		
		if ($endIndex >= sizeof($_SESSION['cart']->amendments)){ 
		//If the endIndex is already greater than or equal to the size of the array, 
		//that means we are at the end.  
			$endIndex = sizeof($_SESSION['cart']->amendments);
			//Setting the endIndex to the size of the array.
		}
		
		if ($beginIndex == $endIndex) {	//Testing for empty arrays, if so exit out.
			break; 
		}
		error_log("beginIndex is: " . $beginIndex);
		
		error_log("endIndex is: " . $endIndex);

		$batch = array_slice($_SESSION['cart']->amendments, $beginIndex, $endIndex-$beginIndex);
		//Setting the batch to a particular array_slice based on the beginIndex and the offset.
		error_log("batch is: " . print_r($batch, true), 0);
		
		$batch_decode = json_decode(json_encode($batch), false);

		$amRes = Amender :: multiAmend($_SESSION['email'], $batch_decode);

		//Send in batches of 9.
		error_log('amRes: '.print_r($amRes,true));

	}

	global $messages;
	$messages = $amRes;

	if ($amRes['success']) {
		$_SESSION['cart']->add = array();
		$_SESSION['cart']->remove = array();
		$_SESSION['cart']->update = array();
		$_SESSION['cart']->amendments = array();
	}
	$_SESSION['cart'];
}

function amendPreview(){
	require_once('./controller/Amender.php');
	global $messages;
	
	error_log('amendPreview session cart: ' . print_r($_SESSION['cart'], true), 0);
	$batchSize = 9;	//default batch size.

	if(count($_SESSION['cart']->amendments) > 0){

		for($i = 0; $i < sizeof($_SESSION['cart']->amendments); $i+=$batchSize){
		//Starting from the first element iterate by 9 each time.
			$beginIndex = $i;	//beginIndex is 0, 9, 18 etc...
			$endIndex = $i + $batchSize;	//endIndex is whatever beginIndex is + 9, 
											//to close the max number in the batch.
			
			if ($endIndex >= sizeof($_SESSION['cart']->amendments)){ 
			//If the endIndex is already greater than or equal to the size of the array, 
			//that means we are at the end.  
				$endIndex = sizeof($_SESSION['cart']->amendments);
				//Setting the endIndex to the size of the array.
			}
			
			if ($beginIndex == $endIndex) {	//Testing for empty arrays, if so exit out.
				break; 
			}
			error_log("beginIndex is: " . $beginIndex);
			
			error_log("endIndex is: " . $endIndex);

			$batch = array_slice($_SESSION['cart']->amendments, $beginIndex, $endIndex-$beginIndex);
			//Setting the batch to a particular array_slice based on the beginIndex and the offset.
			error_log("batch is: " . print_r($batch, true), 0);
			
			$batch_decode = json_decode(json_encode($batch), false);

			$amRes = Amender :: previewMultiAmend($_SESSION['email'], $batch_decode);
		}
	}	//end if
	else{
		$amRes = 0;
	}
	error_log('amendPreview amRes: ' . print_r($amRes, true), 0);
	$messages = $amRes;

}

function getCancelOption(){
	global $messages;
	include('./config.php');

	$messages = $defaultCancelOption;
	error_log("getCancelOption is: " . $messages, 0);
}
/**
 * Previews Rate Plans to be Added.
 * @param 
 * @return total contract value.
 */
function previewAddRatePlan(){
//Retrieve the subtotal of the amendment being added.
	global $messages;
	require_once('./controller/Amender.php');

	$ratePlanId = $_REQUEST['ratePlanId'];
	$ratePlanName = $_REQUEST['ratePlanName'];
	$productName = $_REQUEST['productName'];
	$quantityChargeArray = json_decode($_REQUEST['quantity']); 
	
	$amRes = Amender :: previewAddRatePlan($_SESSION['email'], $ratePlanId, $quantityChargeArray);
	
	$amResObject = json_decode(json_encode($amRes), false);

	if($amResObject->success) {
		$messages = $amResObject->payload;
	}
	else {
		$errors = $amResObject->payload;
		addErrors('previewAddRatePlan', $errors);
	}
}
/**
 * Amends Subscription for additional rate plan.
 * @param 
 * @return Success message.
 */
function addRatePlan(){
	global $messages;
	require_once('./controller/Amender.php');

	$ratePlanId = $_REQUEST['ratePlanId'];
	$ratePlanName = $_REQUEST['ratePlanName'];
	$productName = $_REQUEST['productName'];
	$quantityChargeArray = json_decode($_REQUEST['quantity']); 

	$amRes = Amender :: addRatePlan($_SESSION['email'], $ratePlanId, $quantityChargeArray);
	
	$amResObject = json_decode(json_encode($amRes), false);

	if($amResObject->success) {
		$messages = $amResObject->payload;
	}
	else {
		$errors = $amResObject->payload;
		addErrors('addRatePlan', $errors);
	}
}
/**
 * Previews Rate Plan to be Removed.
 * @param 
 * @return Removal Date.
 */
//Remove Product amendments generate no invoices, so this method will instead return the date on which the removal should take effect (End of term)
function previewRemoveRatePlan(){
	global $messages;
	require_once('./controller/Date.php');
	include('./config.php');

	$date = calcDate($defaultAmendCED);
	$messages = $date;
}
/**
 * Amends Subscription for removal rate plan.
 * @param 
 * @return Success message.
 */
function removeRatePlan(){
	global $messages;
	
	require_once('./controller/Amender.php');
	include('./config.php'); 

	$rpId = $_REQUEST['itemId'];

	error_log("rpId of removeRatePlan of index.php is: " . $rpId);

	$sub = SubscriptionManager :: getCurrentSubscription($_SESSION['email']);
	$subResObject = json_decode(json_encode($sub), false);
	$subObject = $subResObject->payload;
	$subObjectDecode = json_decode($subObject);
	error_log("subObjectDecode in index.php removeRatePlan is: " . print_r($subObjectDecode, true), 0);
	$subId = $subObjectDecode->subscriptions[0]->id;
	
	global $defaultTimeZone;
	date_default_timezone_set($defaultTimeZone);

	$date = Amender::getRemoveProductDate($_SESSION['email'], $rpId, $subObjectDecode);
	if ($date == null) {
		$date = date('Y-m-d');
	}
	
	$amRes = Amender :: removeRatePlan($date, $rpId, $subId);

	$amResObject = json_decode(json_encode($amRes), false);
	error_log("amResDecode in removeRatePlan of index.php is: " . print_r($amResObject, true), 0);

	if($amResObject->success) {
		$messages = $amResObject->payload;
	}
	else {
		$errors = $amResObject->payload;
		addErrors('removeRatePlan', $errors);
	}
}
/**
 * Cancels Subscription
 * @param 
 * @return Success message.
 */
function cancelSubscription(){
	//Subscription status must be set to "Active."
	global $messages;
	require_once('./controller/Amender.php');

	$subId = $_REQUEST['cancelId'];
	error_log("Cancellation Id is: " . $subId);
	
	$amRes = Amender :: cancelSubscription($subId);

	$amResObject = json_decode(json_encode($amRes), false);
	error_log("amResDecode in cancelSubscription of index.php is: " . print_r($amResObject, true), 0);

	if($amResObject->success) {
		$messages = $amResObject->payload;
	}
	else {
		$errors = $amResObject->payload;
		addErrors('cancelSubscription', $errors);
	}
}
/**
 * Amends Subscription for updating rate plan quantities.
 * @param 
 * @return Success message.
 */
function updateRatePlan(){
	global $messages;
	require_once('./controller/Amender.php');
	
	$quantity = $_REQUEST['itemQty'];	//accepts the actual "id" of each object.
	$ratePlanId = $_REQUEST['itemId'];
	$ratePlanChargeId = $_REQUEST['cId'];
	$accountName = $_SESSION['email'];

	$amRes = Amender :: updateRatePlan($accountName, $ratePlanId, $ratePlanChargeId, $quantity);
	
	$amResObject = json_decode(json_encode($amRes), false);
	error_log("amResDecode is: " . print_r($amResObject, true), 0);

	if($amResObject->success) {
		$messages = $amResObject->payload;
	}
	else {
		$errors = $amResObject->payload;
		addErrors('updateRatePlan', $errors);
	}
}


function previewUpdateRatePlan(){
	global $messages;
	require_once('./controller/Amender.php');
	
	$quantity = $_REQUEST['itemQty'];	//accepts the actual "id" of each object.
	$ratePlanId = $_REQUEST['itemId'];
	$ratePlanChargeId = $_REQUEST['cId'];
	$accountName = $_SESSION['email'];

	$amRes = Amender :: previewUpdateRatePlan('A00003686', '2c92c0fa51af60e90151af7a215a03c9', '2c92c0f94be36e93014be70df13211eb', 5);
	
	$amResObject = json_decode(json_encode($amRes), false);
	error_log("amResDecode is: " . print_r($amResObject, true), 0);

	if($amResObject->success) {
		$messages = $amResObject->payload;
	}
	else {
		$errors = $amResObject->payload;
		addErrors('updateRatePlan', $errors);
	}
}

/**
 * Initializes a new Cart session variable to represent empty cart.
 * @param 
 * @return Cart Contents
 */
function emptyCart(){
	global $messages;
	$_SESSION['cart'] = new Cart();
	$messages = $_SESSION['cart'];
	error_log("Hello EmptyCart!");
}
/**
 * Retrieves initial cart contents
 * @param 
 * @return Cart Contents
 */
function getInitialCart(){
	global $messages;
	error_log("getInitialCart");
	//session_unset(); //resets session variables.
	if(!isset($_SESSION['cart'])){
		emptyCart();
	}
	$messages = $_SESSION['cart'];
}
/**
 * Adds selected rateplans to Cart session variable.
 * @param 
 * @return Cart Contents
 */
function addItemToCart(){
	global $messages;
	include('./config.php');

	if(!isset($_SESSION['cart'])){
		emptyCart();
	}

	$ratePlanId = $_REQUEST['ratePlanId'];
	$ratePlanName = $_REQUEST['ratePlanName'];
	$productName = $_REQUEST['productName'];
	$quantityChargeArray = json_decode($_REQUEST['quantity']); 

	error_log('quantityCHargeArray is: ' . print_r($quantityChargeArray, true), 0);
	error_log('ratePlanId is:' . $ratePlanId, 0) ;	

	if(isset($_SESSION['cart'])){
		if(!$allowDuplicateRatePlans){
			if($_SESSION['cart']->checkCartItem($ratePlanId)){
				error_log("checkCartItem returns true! New Cart Item will be added.", 0);
				$_SESSION['cart']->addCartItem($ratePlanId, $quantityChargeArray, $ratePlanName, $productName);
			}
			else {
				error_log("checkCartItem returns false! New cart item not added.", 0);
			}
		}
		else{
			$_SESSION['cart']->addCartItem($ratePlanId, $quantityChargeArray, $ratePlanName, $productName);
			error_log("No need to check, let's just add!", 0);
		}
	} else {
		addErrors(null,'Cart has not been set up.');
		return;	
	}
	
	error_log('SESSION variable is: ' . print_r($_SESSION['cart'], true), 0);
	$messages = $_SESSION['cart'];
}
/**
 * Removes selected rateplans from Cart session variable.
 * @param 
 * @return Cart Contents
 */
function removeItemFromCart(){
	error_log('Starting removeItemFromCart in index');
	global $messages;

	$itemId;
	$removed;

	if(isset($_REQUEST['itemId'])){
		error_log('itemId: '.$_REQUEST['itemId']);
		$itemId = $_REQUEST['itemId'];
	} else {
		error_log('Item Id not specified');
		addErrors(null,'Item Id not specified.');
		return;		
	}


	if(isset($_SESSION['cart'])){
		error_log('Calling removeCartItem with itemId: '+print_r($itemId,true));
		$removed = $_SESSION['cart']->removeCartItem($itemId);
		if(!$removed){
			addErrors(null,'Item no longer exists.');
		}
	} else {
		addErrors(null,'Cart has not been set up.');
		return;		
	}

	$messages = $_SESSION['cart'];
}
/**
 * Refreshes current product catalog.
 * @param 
 * @return Catalog Cache.
 */
function refreshCatalog(){
	require_once('./controller/Catalog.php');
	global $messages;
	
	$refreshResult = Catalog :: refreshCache();
	$messages = $refreshResult;
}
/**
 * Reads product catalog cache
 * @param 
 * @return Catalog Cache.
 */
function readCatalog(){
	require_once('./controller/Catalog.php');
	global $messages;
	
	$catalog_groups;

	if (isset($_REQUEST['page'])) {
		error_log('page: ' . $_REQUEST['page']);
		$catalog_groups = Catalog :: readCache($_REQUEST['page']);	//returns an array.
	} else {
		error_log('page: null');
		$catalog_groups = Catalog :: readCache(null);	//returns an array.
	}
	
	$messages = $catalog_groups;

	// $filtered = Catalog :: productFilter();
}

/** 
 * getNewIframeSrc() Gets the New Iframe source
 */
function getNewIframeSrc(){
	global $messages;

	$iframeSrc = PaymentManager::getNewAccountUrl();

	$messages = $iframeSrc;
}

/**
 * getExistingIframeSrc() helps to create new frame and attach it to the account
 */
function getExistingIframeSrc(){
	global $messages;

	$email = $_SESSION['email'];
	$accountId = $_REQUEST['AccountId'];

	$iframeSrc = PaymentManager::getExistingAccountUrl($accountId);

	$messages = $iframeSrc;

}

function clearAmendments() {
	global $messages;

	if (isset($_SESSION['cart']->amendments)) {
		unset($_SESSION['cart']->amendments);	
	}
	
	$messages = true;
}

function debug($a) {
	global $debug ;
	if($debug) {
		echo "/*";
		var_dump($a);
		echo "*/";
	}
}


function output(){
	global $errors,$messages;
	$msg = array();
	
	if(count($errors)>0) {
		$msg['success'] = false;
		$msg['msg'] = $errors;
		//error_log(print_r($msg, true));
	}
	else {
		//error_log("Success = true");
		$msg['success'] = true;	//element [0] in $msg[].
		if(!is_array($messages)) 
			$messages = array($messages);
		$msg['msg'] = $messages; //element [1] in $msg[]. Automatically packs on the second element in the array by assignment.
		//error_log("Packed success messages");
		//error_log(print_r($msg, true));

		//error_log("Json encoded");
		
	}
	$msgEncode = json_encode($msg);
	error_log('output (encoded): '.print_r($msgEncode,true));
	echo $msgEncode;

}

function addRatePlan1(){
	global $messages;
	require_once('./controller/Amender.php');

	//$ratePlanId = $_REQUEST['ratePlanId'];
	//$ratePlanName = $_REQUEST['ratePlanName'];
	//$productName = $_REQUEST['productName'];
	//$quantityChargeArray = json_decode($_REQUEST['quantity']); 

	$quantity = 2; //$_REQUEST['itemQty'];	//accepts the actual "id" of each object.
	$ratePlanId = '2c92c0fa51aa5f880151ae44c4684b0d'; //$_REQUEST['itemId'];
	$ratePlanChargeId = '2c92c0fa51aa5f880151ae44c4684b0f'; //$_REQUEST['cId'];
	$_SESSION['email'] = 'krishna.katve@varian.com'; //$_SESSION['email'];
	$productName = 'Qumulate';
	$ratePlanName = 'Qumulate Monthly';


	$amRes = Amender :: addRatePlan1('A00003679', $ratePlanId, $quantityChargeArray,$preview);
	
	$amResObject = json_decode(json_encode($amRes), false);

	if($amResObject->success) {
		$messages = $amResObject->payload;
	}
	else {
		$errors = $amResObject->payload;
		addErrors('addRatePlan', $errors);
	}
}

function getAccountNumber(){
	global $messages;
	$quuid = $_REQUEST['quuid'];
	$accID = SubscriptionManager:: getAccountNumber($quuid);

	$messages = $accID;
}


if($out)
{
	output(); 
}
	
?>