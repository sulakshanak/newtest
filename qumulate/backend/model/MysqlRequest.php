<?php

/**
 * 
 *  Short description for file:
 *	The MysqlRequest class contains methods to create a Mysql db connection and  
 *  query the db and return the response object.
 *
 * V1.0
 */

class MysqlRequest
{
	protected $db;

	public function __construct ()
	{
		$this->db 	= 	$this->prepareDBConnection();
	}


	/**
	 * execute method executes the actual query
	 *  @param $query Query to be executed
	 *  @return $output Query result (usage: $output->fetch_object()->{column-name})
	 *
	 */
	public function execute ($query)
	{
		try
		{	
			if($this->db != null){
				$connection = $this->db;
				$output = $connection->query($query);
				return $output;
			} else {
				return "DB_NOT_CONNECTED";
			}
		}
		catch (Exception $e)
		{
			throw $e;
		}
	}

	/**
	 * getDBInstance method returns the dbInstance created
	 *  @return $DbInst 
	 *
	 */
	public function getDBInstance()
	{
		if ($this->db->connect_error) {
			return null;
		} else {
			return $this->db;
		}
	}

	/**
	 * execute method executes the actual query
	 *  @param $query Query to be executed
	 *  @return $output Query result (usage: $output->fetch_object()->{column-name})
	 *
	 */
	private function prepareDBConnection ()
	{
		include ('config.php');
		$hostname 	 = $db_hostname;
		$dbname 	 = $db_name;
		$username 	 = $db_username;
		$password 	 = $db_password;

		$connection = new mysqli($hostname, $username, $password, $dbname);
		error_log($connection->connect_error);
		if ($connection->connect_error) {
			return null;
		} else {
			return $connection;
		}
	}
}

?>