<?php
 
include './config.php';

	
$productSelectOptions = array(
		'filterProducts'=>$filterProducts,  
		'productFilterField'=>$productFilterField,  
		'productFilterValue'=>$productFilterValue,  
		'filterProductRatePlans'=>$filterProductRatePlans,  
		'productRatePlanFilterField'=>$productRatePlanFilterField,  
		'productRatePlanFilterValue'=>$productRatePlanFilterValue,  
		'cachePath'=>$cachePath,  
		'allowDuplicateRatePlans'=>$allowDuplicateRatePlans

	);

$newAccountDefaults = array(
		'defaultPaymentTerm'=>$defaultPaymentTerm, 
		'defaultTimeZone'=>$defaultTimeZone, 
		'defaultAutopay'=>$defaultAutopay, 
		'defaultCurrency'=>$defaultCurrency, 
		'defaultBillCycleDay'=>$defaultBillCycleDay, 
		'defaultInvoiceTemplateId'=>$defaultInvoiceTemplateId, 
		'defaultCommunicationProfileId'=>$defaultCommunicationProfileId, 
		'defaultAccountNotes'=>$defaultAccountNotes
	);

$newSubscriptionDefaults = array(
		'defaultCED'=>$defaultCED, 
		'defaultSAD'=>$defaultSAD, 
		'defaultCAD'=>$defaultCAD, 
		'defaultTSD'=>$defaultTSD, 
		'defaultSubscriptionNotes'=>$defaultSubscriptionNotes, 
		'defaultTermType'=>$defaultTermType, 
		'defaultInitialTerm'=>$defaultInitialTerm, 
		'defaultRenewalTerm'=>$defaultRenewalTerm, 
		'defaultAutoRenew'=>$defaultAutoRenew, 
		'defaultInvoiceCollectNewSub'=>$defaultInvoiceCollectNewSub, 
		'defaultInvoiceTargetDate'=>$defaultInvoiceTargetDate, 
		'billMe'=>$billMe, 
		'defaultPaymentMethodId'=>$defaultPaymentMethodId

	);

$amendSubscriptionDefaults = array(
		'defaultAmendCED'=>$defaultAmendCED, 
		'defaultInvoiceCollectAmendSub'=>$defaultInvoiceCollectAmendSub, 
		'defaultCancellationPolicy'=>$defaultCancellationPolicy
	);

$configData = array(  
	'productSelectOptions'=>$productSelectOptions,  
	'newSubscriptionDefaults'=>$newSubscriptionDefaults, 
	'newAccountDefaults'=>$newAccountDefaults, 
	'amendSubscriptionDefaults'=>$amendSubscriptionDefaults
 );


?>