<?php

class Cart_Item{
	public $itemId;
	public $ratePlanId;
	public $ratePlanName;
	public $productId;
	public $productName;
	public $cartCharges;
	public $date;
	public $amendVerb;	//rater than having to pass amendVerb to each class we just assign the 
						//respective value in the Cart class which then gets passed around as a whole.
}

?>