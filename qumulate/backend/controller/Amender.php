<?php


/**
 * \brief The Amender class manages Amendments for the logged in user's subscription.
 * 
 * V1.05
 */
class Amender {

	public static function multiAmend($accountName, $amendDetail) {
		include('./config.php');
		require_once('Date.php');

		$newUrl = $baseUrl . 'subscriptions/accounts/';
		$acctUrl = $newUrl . $accountName;
		$restResult = new RestRequest($acctUrl, 'GET', null);
		$restResult->execute();
		$sub = $restResult->getResponseBody();

		$subDecode = json_decode($sub);
		$subId = $subDecode->subscriptions[0]->id;

		$data = array();
		$outerAddArray = array();
		$outerRemoveArray = array();
		$outerUpdateArray = array();

		error_log("amendDetail is: " . print_r($amendDetail, true), 0);
		$cartItems = $amendDetail;

		foreach ($cartItems as $i => $amendment) {	//iterates through all the cartitems.
			$addArray = array();
			$removeArray = array();
			$updateArray = array();
			error_log("Amendment is: " . print_r($amendment, true), 0);	
			$amendVerb = $amendment->amendVerb;
			error_log("AmendVerb in amendExecute is: " . $amendVerb);

			if($amendVerb == 'add'){
				$addArray['productRatePlanId'] = $amendment->ratePlanId;
				$addArray['contractEffectiveDate'] = $amendment->date;
				$chargeOverridesArray = array();
				$cartCharges = $amendment->cartCharges;	//this points to an array of CartCharges.

				error_log("cartCharges in add is:" . print_r($cartCharges, true), 0);
				foreach ($cartCharges as $j => $cart_charge) {	//iterate through all the cartCharges
					$eachCharge = array();	//an new array to store individual charge information is created with each iteration.
					$eachCharge['productRatePlanChargeId'] = $cart_charge->chargeId;
					$eachCharge['quantity'] = $cart_charge->quantity;
					array_push($chargeOverridesArray, $eachCharge);
				}
				$addArray['chargeOverrides'] = $chargeOverridesArray;
				
				array_push($outerAddArray, $addArray);
				$data['add'] = $outerAddArray;	//gets updated with the most recent version of outerAddArray after each iteration.
			} 

			else if($amendVerb == 'update'){
				$updateArray['ratePlanId'] = $amendment->ratePlanId;
				$updateArray['contractEffectiveDate'] = $amendment->date;
				$chargeOverridesArray = array();
				$cartCharges = $amendment->cartCharges;	//this points to an array of CartCharges.

				error_log("cartCharges in update is:" . print_r($cartCharges, true), 0);
				foreach ($cartCharges as $j => $cart_charge) {	//iterate through all the cartCharges
					$eachCharge = array();	//a new array to store individual charge information is created with each iteration.
					$eachCharge['ratePlanChargeId'] = $cart_charge->chargeId;
					$eachCharge['quantity'] = $cart_charge->quantity;
					array_push($chargeOverridesArray, $eachCharge);
				}
				$updateArray['chargeUpdateDetails'] = $chargeOverridesArray;
				array_push($outerUpdateArray, $updateArray);
				$data['update'] = $outerUpdateArray;	
			} 

			else if($amendVerb == 'remove'){
				$removeArray['ratePlanId'] = $amendment->ratePlanId;
				$removeArray['contractEffectiveDate'] = $amendment->date;

				array_push($outerRemoveArray, $removeArray);
				$data['remove'] = $outerRemoveArray;	//gets updated with the most recent version of outerAddArray after each iteration.
			} 
		}
		$data['invoiceCollect'] = $defaultInvoiceCollectNewSub;
			
		error_log("data is: " . print_r($data, true), 0);

		$dataEncode = json_encode($data);

		$newUrl = $baseUrl . 'subscriptions/';
		$subUrl = $newUrl . $subId;
		error_log($subUrl);

		$restResult = new RestRequest($subUrl, 'PUT', $dataEncode);
		$restResult->execute();

		$requestBody = $restResult->getRequestBody();
		$resultBody = $restResult->getResponseBody();
		$resultInfo = $restResult->getResponseInfo();		

		error_log("requestBody is: " . print_r($requestBody, true), 0);
		error_log("responseBody is: " . print_r($resultBody, true), 0);

		$resultBodyDecode = json_decode($resultBody);
		error_log("resultBodyDecode in mulitAmend is: " . print_r($resultBodyDecode, true), 0);

		if($resultBodyDecode->success) {
			$resultArray['success'] = true;
			$resultArray['payload'] = $resultBody;
			return $resultArray;	
		}
		else {
			error_log("Amender error message is: " . $resultBodyDecode->reasons[0]->message);
			$resultArray['success'] = false;
			$resultArray['payload'] = $resultBodyDecode->reasons[0]->message;
			return $resultArray;
		}
	}
	public static function previewMultiAmend($accountName, $amendDetail){
		include('./config.php');
		require_once('Date.php');

		$newUrl = $baseUrl . 'subscriptions/accounts/';
		$acctUrl = $newUrl . $accountName;
		$restResult = new RestRequest($acctUrl, 'GET', null);
		$restResult->execute();
		$sub = $restResult->getResponseBody();

		$subDecode = json_decode($sub);
		$subId = $subDecode->subscriptions[0]->id;

		$date = $subDecode->subscriptions[0]->subscriptionStartDate;

		error_log("amendDetail is: " . print_r($amendDetail, true), 0);
		$cartItems = $amendDetail;

		$data = array();
		$outerAddArray = array();
		$outerRemoveArray = array();
		$outerUpdateArray = array();

		foreach ($cartItems as $i => $amendment) {	//iterates through all the cartitems.
			$addArray = array();
			$removeArray = array();
			$updateArray = array();
			error_log("Amendment is: " . print_r($amendment, true), 0);	
			$amendVerb = $amendment->amendVerb;
			error_log("AmendVerb in amendExecute is: " . $amendVerb);

			if($amendVerb == 'add'){
				$addArray['productRatePlanId'] = $amendment->ratePlanId;
				$addArray['contractEffectiveDate'] = $amendment->date;
				$chargeOverridesArray = array();
				$cartCharges = $amendment->cartCharges;	//this points to an array of CartCharges.
		

				error_log("cartCharges is:" . print_r($cartCharges, true), 0);
				foreach ($cartCharges as $j => $cart_charge) {	//iterate through all the cartCharges
					$eachCharge = array();	//an new array to store individual charge information is created with each iteration.
					$eachCharge['productRatePlanChargeId'] = $cart_charge->chargeId;
					$eachCharge['quantity'] = $cart_charge->quantity;
					array_push($chargeOverridesArray, $eachCharge);
				}
				$addArray['chargeOverrides'] = $chargeOverridesArray;
		
				array_push($outerAddArray, $addArray);
				$data['add'] = $outerAddArray;	//gets updated with the most recent version of outerAddArray after each iteration.

				$data['preview'] = true;

				error_log("amendPreview data after array refactoring is: " . print_r($data, true), 0);
			
				$data = '{"currentTerm": 12,"renewalTerm": 12,"notes": "","add": [{"ratePlanId": "'.$updateArray['ratePlanId'].'","chargeUpdateDetails": [{"ratePlanChargeId": "'.$eachCharge['ratePlanChargeId'].'","quantity": '.$eachCharge['quantity'].'}],"contractEffectiveDate": "'.$date.'" }]}';
				

			} 

			else if($amendVerb == 'update'){
				//$updateArray['Type'] = "UpdateProduct";
				$updateArray['ratePlanId'] = $amendment->ratePlanId;
				$updateArray['contractEffectiveDate'] = $amendment->date;
				$chargeOverridesArray = array();
				$cartCharges = $amendment->cartCharges;	//this points to an array of CartCharges.

				error_log("cartCharges is:" . print_r($cartCharges, true), 0);
				foreach ($cartCharges as $j => $cart_charge) {	//iterate through all the cartCharges
					$eachCharge = array();	//an new array to store individual charge information is created with each iteration.
					$eachCharge['ratePlanChargeId'] = $cart_charge->chargeId;
					$eachCharge['quantity'] = $cart_charge->quantity;
					array_push($chargeOverridesArray, $eachCharge);
				}
				$updateArray['chargeUpdateDetails'] = $chargeOverridesArray;
				array_push($outerUpdateArray, $updateArray);
				$data['update'] = $outerUpdateArray;	//gets updated with the 

				$data['preview'] = true;

				error_log("amendPreview data after array refactoring is: " . print_r($data, true), 0);
			
				$data = '{"currentTerm": 12,"renewalTerm": 12,"notes": "","update": [{"ratePlanId": "'.$updateArray['ratePlanId'].'","chargeUpdateDetails": [{"ratePlanChargeId": "'.$eachCharge['ratePlanChargeId'].'","quantity": '.$eachCharge['quantity'].'}],"contractEffectiveDate": "'.$date.'" }]}';
				

			} 

			else if($amendVerb == 'remove'){
				$removeArray['ratePlanId'] = $amendment->ratePlanId;
				$removeArray['contractEffectiveDate'] = $amendment->date;

				array_push($outerRemoveArray, $removeArray);
				$data['remove'] = $outerRemoveArray;	//gets updated with the most recent version of outerAddArray after each iteration.

				$data['preview'] = true;

				error_log("amendPreview data after array refactoring is: " . print_r($data, true), 0);
			
				$data = '{"currentTerm": 12,"renewalTerm": 12,"notes": "","remove": [{"ratePlanId": "'.$updateArray['ratePlanId'].'","chargeUpdateDetails": [{"ratePlanChargeId": "'.$eachCharge['ratePlanChargeId'].'","quantity": '.$eachCharge['quantity'].'}],"contractEffectiveDate": "'.$date.'" }]}';

			} 
		}

		$dataEncode = json_encode($data);

				error_log("dataEncode is: " . $dataEncode);

				$newUrl = $baseUrl . 'subscriptions/';
				$subUrl = $newUrl . $subId;
				error_log($subUrl);

				$restResult = new RestRequest($subUrl, 'PUT', $data);
				$restResult->execute();

		$requestBody = $restResult->getRequestBody();
		$resultBody = $restResult->getResponseBody();
		$resultInfo = $restResult->getResponseInfo();		

		error_log("requestBody is: " . print_r($requestBody, true), 0);
		error_log("responseBody is: " . print_r($resultBody, true), 0);

		$resultBodyDecode = json_decode($resultBody);
		error_log("resultBodyDecode is: " . print_r($resultBodyDecode, true), 0);

		if($resultBodyDecode->success) {
			$resultArray['success'] = true;
			$resultArray['payload'] = $resultBody;
			return $resultArray;	
		}
		else {
			error_log("Amender error message is: " . $resultBodyDecode->reasons[0]->message);
			$resultArray['success'] = false;
			$resultArray['payload'] = $resultBodyDecode->reasons[0]->message;
			return $resultArray;
		}
	}

	public static function previewUpdateRatePlan($accountName, $productRatePlanId, $productRatePlanChargeId, $quantity) {
		include('./config.php');
		require_once('Date.php');

		$date = calcDate($defaultAmendCED);
		$resultArray = array();

		$data = array(
			'accountKey' => $accountName,
			'termType' => $defaultTermType,
			'initialTerm' => $defaultInitialTerm,
			'contractEffectiveDate' => $date,
			'subscribeToRatePlans' => $productRatePlanArray = array(array(
				'productRatePlanId' => $productRatePlanId,
				'chargeOverrides' => $productRatePlanChargeArray = array(array(
					'productRatePlanChargeId' => $productRatePlanChargeId,
					'quantity' => $quantity))
				)
			)
		);

		error_log("previewUpdateRatePlan data array is: " . print_r($data, true), 0);
		$dataEncode = json_encode($data);
		error_log("previewUpdateRatePlan JSON data array is: " . print_r($dataEncode, true), 0);

		$newUrl = $baseUrl . 'subscriptions/preview';

		$restResult = new RestRequest($newUrl, 'POST', $dataEncode);
		$restResult->execute();
		$requestBody = $restResult->getRequestBody();
		$resultBody = $restResult->getResponseBody();
		$resultInfo = $restResult->getResponseInfo();
		

		error_log(print_r($requestBody, true), 0);
		error_log("previewUpdateRatePlan resultBody is: " . print_r($resultBody, true), 0);
		
		$resultBodyDecode = json_decode($resultBody);
		error_log("resultBodyDecode is: " . print_r($resultBodyDecode, true), 0);

		if($resultBodyDecode->success) {
			$resultArray['success'] = true;
			$resultArray['payload'] = $resultBody;
			return $resultArray;	
		}
		else {
			error_log("Amender error message is: " . $resultBodyDecode->reasons[0]->message);
			$resultArray['success'] = false;
			$resultArray['payload'] = $resultBodyDecode->reasons[0]->message;
			return $resultArray;
		}
	}

	public static function updateRatePlan($accountName, $ratePlanId, $ratePlanChargeId, $quantity) {
		include('./config.php');
		require_once('Date.php');

		$date = calcDate($defaultAmendCED);
		$resultArray = array();

		//Retrieve Subscription Id
		$newUrl = $baseUrl . 'subscriptions/accounts/';
		$acctUrl = $newUrl . $accountName;
		$restResult = new RestRequest($acctUrl, 'GET', null);
		$restResult->execute();
		$sub = $restResult->getResponseBody();

		$subDecode = json_decode($sub);
		$subId = $subDecode->subscriptions[0]->id;

		$data = array(
			'update' => $productRatePlanArray = array(array(
				'ratePlanId' => $ratePlanId,
				'contractEffectiveDate' => $date,
				'chargeUpdateDetails' => $productRatePlanChargeArray = array(array(
					'ratePlanChargeId' => $ratePlanChargeId,
					'quantity' => $quantity))
				)
			)
		);

		$dataEncode = json_encode($data);

		$newUrl = $baseUrl . 'subscriptions/';
		$subUrl = $newUrl . $subId;
		error_log($subUrl);

		$restResult = new RestRequest($subUrl, 'PUT', $dataEncode);
		$restResult->execute();

		$requestBody = $restResult->getRequestBody();
		$resultBody = $restResult->getResponseBody();
		$resultInfo = $restResult->getResponseInfo();		

		error_log("requestBody is: " . print_r($requestBody, true), 0);
		error_log("responseBody is: " . print_r($resultBody, true), 0);

		$resultBodyDecode = json_decode($resultBody);
		error_log("resultBodyDecode is: " . print_r($resultBodyDecode, true), 0);

		if($resultBodyDecode->success) {
			$resultArray['success'] = true;
			$resultArray['payload'] = $resultBody;
			return $resultArray;	
		}
		else {
			error_log("Amender error message is: " . $resultBodyDecode->reasons[0]->message);
			$resultArray['success'] = false;
			$resultArray['payload'] = $resultBodyDecode->reasons[0]->message;
			return $resultArray;
		}
	}

	public static function previewAddRatePlan($accountName, $rpId, $qtyArray) {
		include('./config.php');
		require_once('Date.php');
		
		$date = calcDate($defaultAmendCED);
		$resultArray = array();
		$chargeOverrides = array();
		
		foreach($qtyArray as $each_charge){	//gets the objects out of the array.
			error_log('id is: ' . $each_charge->cId, 0);
			error_log('qty is: ' . $each_charge->qty, 0);
			$chargeId = $each_charge->cId;	
			$quantity = $each_charge->qty; 

			array_push($chargeOverrides, array('productRatePlanChargeId' => $chargeId, 'quantity' => $quantity));
		}

		error_log("chargeOverrides data array is: " . print_r($chargeOverrides, true), 0);

		$data = array(
				'accountKey' => $accountName,
				'initialTerm' => $defaultInitialTerm,
				'termType' => $defaultTermType,
				'contractEffectiveDate' => $date,
				
				'subscribeToRatePlans' => array(array('productRatePlanId' => $rpId, 'chargeOverrides' => $chargeOverrides))
			);	//Need for loop for qtyArray?
		error_log("previewAddRatePlan data array is: " . print_r($data, true), 0);
		$dataEncode = json_encode($data);

		$newUrl = $baseUrl . 'subscriptions/preview';
		$restResult = new RestRequest($newUrl, 'POST', $dataEncode);
		$restResult->execute();
		$requestBody = $restResult->getRequestBody();
		$resultBody = $restResult->getResponseBody();
		$resultInfo = $restResult->getResponseInfo();
		error_log("request result is: " . print_r($restResult, true), 0);
		error_log(print_r($requestBody, true), 0);
		error_log("responseBody is: " . print_r($resultBody, true), 0);
		
		$resultBodyDecode = json_decode($resultBody);
		error_log("resultBodyDecode is: " . print_r($resultBodyDecode, true), 0);

		if($resultBodyDecode->success) {
			$tcv = $resultBodyDecode->totalContractedValue;
			error_log("tcv is: " . $tcv);
			$resultArray['success'] = true;
			$resultArray['payload'] = $tcv;
			return $resultArray;	
		}
		else {
			error_log("Amender error message is: " . $resultBodyDecode->reasons[0]->message);
			$resultArray['success'] = false;
			$resultArray['payload'] = $resultBodyDecode->reasons[0]->message;
			return $resultArray;
		}
	}

	/**
	 * \brief Adds a new ratePlan to the current user's subscription. 
	 * 
	 * New products added to the user's subscription are effective immediately. 
	 * A quantity can also be supplied, that will apply to all recurring and one-time charges on the rate plan that do not use flat fee pricing.
	 * @param $accountName Name of the target account
	 * @param $prpId Product Rate Plan of the amendment to be added.
	 * @param $qty Amount of UOM for the RatePlan being added. A null value can be passed for product rate plans that use flat fee pricing
	 * @param $preview Flag to determine whether this function will be used to create an amendment, or preview an invoice
	 * @return Amend Result
	 */
	public static function addRatePlan($currentSub, $ratePlanId, $quantityChargeArray) {
		include('./config.php');
		require_once('Date.php');

		global $defaultInvoiceCollectNewSub;
		global $baseUrl;
		
		$resultArray = array();
		$date = calcDate($defaultAmendCED);
		$chargeOverrides = array();

		$newUrl = $baseUrl . 'subscriptions/accounts/';
		$acctUrl = $newUrl . $currentSub;
		$restResult = new RestRequest($acctUrl, 'GET', null);
		$restResult->execute();
		$sub = $restResult->getResponseBody();
		$subDecode = json_decode($sub);
		$subId = $subDecode->subscriptions[0]->id;

		foreach($quantityChargeArray as $each_charge) {
			error_log('id: '.$each_charge->cId.', qty: '.$each_charge->qty);

			if (isset($each_charge->qty) && isset($each_charge->cId)) {
				array_push($chargeOverrides, array('productRatePlanChargeId' => $each_charge->cId, 'quantity' => $each_charge->qty));
			}
		}

		error_log("chargeOverrides data array is: " . print_r($chargeOverrides, true));

		$data = array(
			'add' => array(
				array(
					'productRatePlanId' => $ratePlanId, 
					'contractEffectiveDate' => $date, 
					'chargeOverrides' => $chargeOverrides
				)
			),'invoiceCollect' => $defaultInvoiceCollectNewSub
		);

		$dataEncode = json_encode($data);
		error_log('dataEncode: '.$dataEncode);

		$updateSubUrl = $baseUrl.'subscriptions/'.$subId;
		error_log('updateSubUrl: '.$updateSubUrl);

		$restResult = new RestRequest($updateSubUrl, 'PUT', $dataEncode);
		$restResult->execute();

		$requestBody = $restResult->getRequestBody();
		$resultBody = $restResult->getResponseBody();
		$resultInfo = $restResult->getResponseInfo();		

		error_log("responseBody is: " . print_r($resultBody, true));
		
		$resultBodyDecode = json_decode($resultBody);
		error_log("resultBodyDecode is: " . print_r($resultBodyDecode, true), 0);

		if($resultBodyDecode->success) {
			$resultArray['success'] = true;
			$resultArray['payload'] = $resultBody;
			return $resultArray;	
		}
		else {
			error_log("Amender error message is: " . $resultBodyDecode->reasons[0]->message);
			$resultArray['success'] = false;
			$resultArray['payload'] = $resultBodyDecode->reasons[0]->message;
			return $resultArray;
		}
	}


	public static function getRemoveProductDate($accountName, $ratePlanId, $subscriptions) {
		require_once('./config.php');
		require_once('./controller/AccountManager.php');

		if (isset($accountName) && isset($ratePlanId) && isset($subscriptions)) {
			$invRes = AccountManager::getInvoiceHistory($accountName);
			error_log('invRes: ' . print_r($invRes, true));

			$invResJson = json_decode($invRes);

			if ($invResJson->success) {
				$latestInv = $invResJson->invoices[0];

				$removalDate = date('Y-m-d');
				error_log('Initial removalDate: ' . $removalDate);

				$relatedChargeIds = array();
				error_log('subscriptions: ' . print_r($subscriptions, true));
				foreach($subscriptions->subscriptions as $sub) {
					foreach($sub->ratePlans as $rp) {
						if ($rp->id == $ratePlanId) {
							error_log('Found matching RatePlan');
							foreach($rp->ratePlanCharges as $rpc) {
								array_push($relatedChargeIds, $rpc->id);
							}
						}
					}
				}
				error_log('relatedChargeIds: ' . print_r($relatedChargeIds, true));


				if (isset($latestInv)) {
					foreach($latestInv->invoiceItems as $invItem) {
						$serviceEndDate = $invItem->serviceEndDate;
						error_log('serviceEndDate: ' . $serviceEndDate . ', id: ' . $invItem->chargeId);
						if (in_array($invItem->chargeId, $relatedChargeIds) && $serviceEndDate > $removalDate) {
							error_log('replacing $removalDate with ' . $serviceEndDate);
							$removalDate = $serviceEndDate;
						} else {
							error_log('charge was either not in chosen ratePlan or serviceEndDate was before removalDate');
						}
					}
				}
				return $removalDate;
			} else {
				error_log('Invoice retrieval in getRemoveProductDate unsuccessful.');
				return null;
			}
		} else {
			error_log('Passed in accountName or ratePlanId to getRemoveProductDate was null');
			return null;
		}
	}

	public static function removeRatePlan($date, $ratePlanId, $subId) {
		include('./config.php');
		require_once('SubscriptionManager.php');

		//Subscription Account Url
		global $baseUrl;
		$resultArray = array();

		$newUrl = $baseUrl . 'subscriptions/' . $subId;

		$tempArray = array(array('ratePlanId' => $ratePlanId, 'contractEffectiveDate' => $date));

		$dataEncode = json_encode(array('remove' => $tempArray, 'invoiceCollect' => $defaultInvoiceCollectAmendSub));

		$requestPutWithParams = new RestRequest($newUrl, 'PUT', $dataEncode);
		$requestPutWithParams->execute();

		$request = $requestPutWithParams->getRequestBody();
		$result = $requestPutWithParams->getResponseBody();
		error_log(print_r($request, true));
		error_log(print_r($result, true));
		$result = json_decode($result, true);

		$resultBodyDecode = json_decode(json_encode($result), false);
		error_log("resultBodyDecode is: " . print_r($resultBodyDecode, true), 0);

		if($resultBodyDecode->success) {
			$resultArray['success'] = true;
			$resultArray['payload'] = $result;
			return $resultArray;	
		}
		else {
			error_log("Amender error message is: " . $resultBodyDecode->reasons[0]->message);
			$resultArray['success'] = false;
			$resultArray['payload'] = $resultBodyDecode->reasons[0]->message;
			return $resultArray;
		}
	}

	public static function cancelSubscription($subId) {
		include('./config.php');

		$resultArray = array();

		$data = array(
			'cancellationPolicy' => $defaultCancellationPolicy,
			// 'invoiceCollect' => $defaultInvoiceCollectAmendSub,
			'invoiceCollect' => false
		);

		error_log("cancelSubscription data array is: " . print_r($data, true), 0);
		$dataEncode = json_encode($data); 

		$newUrl = $baseUrl . 'subscriptions/' . $subId . '/cancel';

		$restResult = new RestRequest($newUrl, 'PUT', $dataEncode);
		$restResult->execute();
		$requestBody = $restResult->getRequestBody();
		$resultBody = $restResult->getResponseBody();
		$resultInfo = $restResult->getResponseInfo();

		error_log("cancelSubscription resultBody is: " . print_r($resultBody, true), 0);

		$resultBodyDecode = json_decode($resultBody);
		error_log("resultBodyDecode is: " . print_r($resultBodyDecode, true), 0);

		if($resultBodyDecode->success) {
			$resultArray['success'] = true;
			$resultArray['payload'] = $resultBody;
			return $resultArray;	
		}
		else {
			error_log("Amender error message is: " . $resultBodyDecode->reasons[0]->message);
			$resultArray['success'] = false;
			$resultArray['payload'] = $resultBodyDecode->reasons[0]->message;
			return $resultArray;
		}
	}



	public static function addRatePlan1($accountName, $prpId, $qty, $preview) {
		$zapi;
		try {
			$zapi = new zApi();
		} catch (Exception $e) {
			throw new Exception("INVALID_ZLOGIN");
		}

		$sub = SubscriptionManager :: getCurrentSubscription1($accountName);
		$qty =2;
		date_default_timezone_set('America/Los_Angeles');
		$date = date('Y-m-d\TH:i:s');
		$amendment = array (
			'EffectiveDate' => $date,
			'Name' => 'Add Rate Plan' . time(),
			'Description' => 'New Rate Plan',
			'Status' => 'Completed',
			'SubscriptionId' => $sub->subId,
			'Type' => 'NewProduct',
			'ContractEffectiveDate' => $date,
			'ServiceActivationDate' => $date,
			'ContractAcceptanceDate' => $date,
			'EffectiveDate' => $date,
			'RatePlanData' => array (
				'RatePlan' => array (
					'ProductRatePlanId' => $prpId,
				)
			)
		);

		//If a quantity has been passed, specify charge data to cover all quantifiable charges on the rate plan being added
		if ($qty != null) {
			$ratePlanChargeData = array ();
			//$charges = Catalog :: getRatePlan($prpId)->charges;

					array_push($ratePlanChargeData, array (
						"RatePlanCharge" => array (
							"ProductRatePlanChargeId" => '2c92c0f94be36e93014be70df13211eb',
							"Quantity" => 3
						)
					));
				
			$amendment['RatePlanData']['RatePlanChargeData'] = $ratePlanChargeData;
		}

		$amendOptions = array (
			"GenerateInvoice" => true,
			"ProcessPayments" => true,
			
		);
		$previewOptions = array (
			"EnablePreviewMode" => $preview
		);

		$amendResult = $zapi->zAmend($amendment, $amendOptions, $previewOptions);
		return $amendResult;
	}

		

}
?>