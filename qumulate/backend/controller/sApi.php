<?php

/**
* \brief The sApi class is used to interface with Salesforce API
* 
* V1.05
*/
class sApi{
	/**
	 * Creates an account in Salesforce with the given name
	 * @param $accountName Account Name of new Salesforce Account
	 * @return a response result containing a 'success' result and an 'id' of the created account if successful.
	 */
	static function createSfdcAccount($accountNumber){
		include("./config.php");
	
		$mySforceConnection = new SforceEnterpriseClient();
		$mySforceConnection->createConnection($SfdcWsdl);
		$mySforceConnection->login($SfdcUsername, $SfdcPassword.$SfdcSecurityToken);
		
	
		$query = "SELECT ac.Id, ac.Name, ac.SFDC_Account_Ref_Number__c, ac.Bill_to_SFDC_Account_ID__c, ac.BillingStreet, ac.BillingCity, ac.BillingState, ac.BillingPostalCode, ac.BillingCountry FROM Account ac";
		
		$byNameClause = " WHERE ac.AccountNumber = '".trim($accountNumber)."'"; 
		$response = $mySforceConnection->query($query . $byNameClause);
		// if found return Query result records..
		if(count($response->records) > 0)
			return $response->records;
		else {
			/* else create and return..
			$records = array();
			$records[0] = new stdclass();
			$records[0]->Name = $accountName;
			$records[0]->Country__c = "US";
			$response = $mySforceConnection->create($records, 'Account');
			if($response[0]->success === 1)
				return $mySforceConnection->query($query . " WHERE ac.Id=".(int)$response[0]->id)->records;
			
			throw new Exception("New account creation for $accountName failed!!");  */
		}
		
	}

	static function fetchOktaContact($accountNumber){
		include("./config.php");
	
		$mySforceConnection = new SforceEnterpriseClient();
		$mySforceConnection->createConnection($SfdcWsdl);
		$mySforceConnection->login($SfdcUsername, $SfdcPassword.$SfdcSecurityToken);
		
		// First look-up an Account!
				
		$query = "SELECT Id, AccountId,OktaId__c, Contact.Account.Id,Contact.Account.Name,ownerid, Contact.Account.Ext_Cust_Id__c,Contact.Account.SFDC_Account_Ref_Number__c,Email,MailingCity,MailingCountry,MailingState,MailingStreet,MailingPostalCode,FirstName,LastName,Phone from Contact";

		$byNameClause = " WHERE OktaId__c = '".trim($accountNumber)."' LIMIT 1";

		$response = $mySforceConnection->query($query . $byNameClause);

		// if found return Query result records..

		if(count($response->records) > 0)
			return $response->records;
		else {
			
		}
		
	}

	static function fetchBillTo($ext_cust_id){
		include("./config.php");
	
		$mySforceConnection = new SforceEnterpriseClient();
		$mySforceConnection->createConnection($SfdcWsdl);
		$mySforceConnection->login($SfdcUsername, $SfdcPassword.$SfdcSecurityToken);
		
		//print_r($mySforceConnection->describeSObject('ERP_Partner_Association__c'));

		// First look-up an Account!
	
		
		$query = "SELECT Id, Sales_Org__c, Partner_Street__c, Partner_Street_line_2__c,Partner_City__c, Partner_State__c, Partner_Country__c, Partner_Zipcode_postal_code__c FROM ERP_Partner_Association__c";


		$byNameClause = " where ERP_Customer_Number__c='6052733' LIMIT 1";

		$response = $mySforceConnection->query($query . $byNameClause);
		// if found return Query result records..
		if(count($response->records) > 0)
			return $response->records;
		else {
			/* else create and return..
			$records = array();
			$records[0] = new stdclass();
			$records[0]->Name = $accountName;
			$records[0]->Country__c = "US";
			$response = $mySforceConnection->create($records, 'Account');
			if($response[0]->success === 1)
				return $mySforceConnection->query($query . " WHERE ac.Id=".(int)$response[0]->id)->records;
			
			throw new Exception("New account creation for $accountName failed!!");  */
		}
		
	}

}

?>
