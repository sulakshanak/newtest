<?php

/**
 * The Cart class manages a user's cart. One of these is stored for each user in a session variable to keep track of all of their selected items before they've purchased them.
 * 
 */

class Cart{
	/*! A list of cart item models that each store a rate plan to be displayed to the user*/
	public $cart_items; 
	/*! A tally of cart items used to generate a unique cart id for each item added*/
	public $latestItemId; 
	public $add;
	public $remove;
	public $update;
	public $amendments;
	/**
	 * Initializes an empty cart instance.
	 */
	public function __construct() {
		$this->clearCart();
	}

	/**
	 * Clears all items from this cart instance.
	 */
	public function clearCart() {
		$this->cart_items = array();
		$this->add = array();
		$this->update = array();
		$this->remove = array();
		$this->amendments = array();
		$this->latestItemId = 1;
	}
	//Everytime this function is called, another array gets populated...so this function should return an array.
	public function addNewAmend($rateplanId, $ratePlanName, $productName, $quantityChargeArray, $date) {
		error_log("Hello addNewAmend");
		//Date is tied to the rateplan.
		$newCartItem = new Cart_Item();
		$newCartItem->ratePlanId = $rateplanId;	
		$newCartItem->ratePlanName = $ratePlanName;
		$newCartItem->productName = $productName;
		$newCartItem->date = $date;
		$newCartItem->amendVerb = 'add';
		$newCartItem->itemId = $this->latestItemId++;
		error_log('itemId: ' . $newCartItem->itemId, 0);

		$qtyArray = $quantityChargeArray;

		$rpId = $newCartItem->ratePlanId;
		error_log('qtyArray: ' . print_r($qtyArray, true), 0);
		error_log('ratePlanId: ' . $rateplanId);
		error_log('date: ' . $date);

		$newCartItem->cartCharges = array();	//$quantityChargeArray
		
		if (isset($qtyArray)) {
			foreach($qtyArray as $each_charge){
				$newCartCharge = new Cart_Charge();
				error_log('id is: ' . $each_charge->cId, 0);
				error_log('qty is: ' . $each_charge->qty, 0);
				$newCartCharge->chargeId = $each_charge->cId;	//Need to check if quantityChargeArray has "cId" tag.
				$newCartCharge->quantity = $each_charge->qty; 	//Need to check if quantityChargeArray has "qty" tag.
				$newCartCharge->uom = $each_charge->uom;
				$newCartCharge->chargeName = $each_charge->cName;
				array_push($newCartItem->cartCharges, $newCartCharge);
			}
		}

		error_log('cart.php (newCartItem) contents are: ' . print_r($newCartItem, true), 0);
		
		// array_push($this->add, $newCartItem);
		array_push($this->amendments, $newCartItem);
	}

	public function addUpdateAmend($rateplanId, $ratePlanName, $productName, $quantityChargeArray, $date) {
		error_log("Hello addUpdateAmend");

		$newCartItem = new Cart_Item();
		$newCartItem->ratePlanId = $rateplanId;	
		$newCartItem->ratePlanName = $ratePlanName;
		$newCartItem->productName = $productName;
		$newCartItem->date = $date;
		$newCartItem->amendVerb = 'update';
		$newCartItem->itemId = $this->latestItemId++;

		$qtyArray = $quantityChargeArray;

		$rpId = $newCartItem->ratePlanId;
		error_log('qtyArray: ' . print_r($qtyArray, true), 0);
		error_log('ratePlanId: ' . $rateplanId);
		error_log('date: ' . $date);

		$newCartItem->cartCharges = array();	//$quantityChargeArray
		
		if (isset($qtyArray)) {
			foreach($qtyArray as $each_charge){
				$newCartCharge = new Cart_Charge();
				error_log('id is: ' . $each_charge->cId, 0);
				error_log('qty is: ' . $each_charge->qty, 0);
				$newCartCharge->chargeId = $each_charge->cId;	//Need to check if quantityChargeArray has "cId" tag.
				$newCartCharge->quantity = $each_charge->qty; 	//Need to check if quantityChargeArray has "qty" tag.
				array_push($newCartItem->cartCharges, $newCartCharge);
			}
		}

		error_log('cart.php (newCartItem) contents are: ' . print_r($newCartItem, true), 0);
		
		// array_push($this->update, $newCartItem);
		array_push($this->amendments, $newCartItem);
	}

	public function addRemoveAmend($ratePlanId, $ratePlanName, $productName, $date) {
		error_log("Hello addRemoveAmend");

		$newCartItem = new Cart_Item();
		$newCartItem->ratePlanId = $ratePlanId;
		$newCartItem->ratePlanName = $ratePlanName;
		$newCartItem->productName = $productName;
		$newCartItem->date = $date;
		// $newCartItem->date = $date;
		$newCartItem->amendVerb = 'remove';
		$newCartItem->itemId = $this->latestItemId++;

		error_log('cart.php (newCartItem) contents are: ' . print_r($newCartItem, true), 0);

		// array_push($this->remove, $newCartItem);
		array_push($this->amendments, $newCartItem);
	}

	/**
	 * Checks cart for existence of a given ratePlanId
	 * @param $ratePlanId Id of a ratePlan to search for in the Cart
	 * @return Boolean True is ratePlanId is NOT in Cart. False if ratePlanId is FOUND in Cart
	 */
	public function checkCartItem($ratePlanId) {
		if (!empty($ratePlanId)) {
			foreach($this->cart_items as $itemIterator){
				if($itemIterator->ratePlanId === $ratePlanId){
					error_log("Found a matching RatePlan in the cart.");
					return false;
				}
			}
			return true;
		} else {
			error_log('ratePlanId passed to checkCartItem was empty');
			return false;
		}
	}

	/**
	 * Adds a new item to this cart instance.
	 * @param $rateplanId The ProductRatePlanId of the item being added
	 * @param $quantityChargeArray Array of charges and their corresponding quantities
	 * @param $ratePlanName Name of ProductRatePlan
	 * @param $productName Name of Product
	 */
	public function addCartItem($rateplanId, $quantityChargeArray, $rateplanName, $productName){
		$newCartItem = new Cart_Item();
		if((isset($rateplanId)))
		$newCartItem->ratePlanId = $rateplanId;
		if((isset($rateplanName)))
		$newCartItem->ratePlanName = $rateplanName;
		if((isset($productName)))
		$newCartItem->productName = $productName;	
		$newCartItem->itemId = $this->latestItemId++;
		error_log('itemId: ' . $newCartItem->itemId, 0);

		$rpId = $newCartItem->ratePlanId;
		error_log('quantityChargeArray: ' . print_r($quantityChargeArray, true), 0);
		
		$newCartItem->cartCharges = array();
		
		if (isset($quantityChargeArray)) {
			foreach($quantityChargeArray as $each_charge){
				$newCartCharge = new Cart_Charge();
				error_log('id is: ' . $each_charge->cId, 0);
				error_log('qty is: ' . $each_charge->qty, 0);
				$newCartCharge->chargeId = $each_charge->cId;	
				$newCartCharge->quantity = $each_charge->qty; 
				$newCartCharge->chargeName = $each_charge->cName;
				$newCartCharge->uom = $each_charge->uom; 
				array_push($newCartItem->cartCharges, $newCartCharge);
			}
		}

		error_log('cart.php (newCartItem) contents are: ' . print_r($newCartItem, true), 0);
		
		array_push($this->cart_items, $newCartItem);
	}

	/**
	 * Removes an item from the user's cart.
	 * @param $itemId The unique cart item ID of the item being removed from the cart
	 */
	public function removeCartItem($itemId) {
		error_log('Entering removecartItem.');

		if (!empty($itemId)) {
			error_log('removeCartItem itemId is:' . $itemId, 0);
			for($i=0; $i<(count($this->cart_items)); $i++) {
				if($this->cart_items[$i]->itemId==$itemId) {
					unset($this->cart_items[$i]);
					$this->cart_items = array_values($this->cart_items);
					error_log('Found the matching item Id');
					return true;
				}
			}
			error_log('Could not find a matching itemId');
			return false;
		} else {
			error_log('itemId passed to removeCartItem was empty.');
			return false;
		}
	}
}

?>