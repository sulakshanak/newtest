<?php
/**
 * Short description for file:
 *	All date manipulation functions that are generic should go into 
 *	this file for code reusability and standard practices.
 *
*/

	/**
	 *	calcDate($day) function to calculate date based on the input string
	 *	@param $day string (eg) Today, First_of_current_month
	 *	@return $date string format (YYYY-MM-DD) (eg) 2014-01-30
	*/
	function calcDate($day){
		include './config.php';
		date_default_timezone_set($defaultTimeZone);

		$date = date("Y-m-d", time());

		//Returns today's date
		if($day == "Today"){
			return $date;
		} 

		//Calculates first of next month
		if ($day =="First_of_next_month"){
			$firstDayNextMonth = date('Y-m-01', strtotime('next month'));
			return $firstDayNextMonth;
		}

		//Calculates first of current month
		if ($day == "First_of_current_month"){
			$date =  date("Y-m-01", time());
			return $date;
		} 

		return null;
	}

?>