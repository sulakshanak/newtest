<?php

class Catalog {

	public static function refreshCache($pageSize = 10) {
		include_once('./config.php');
		include_once('./model/RestRequest.php');
		global $baseUrl, $cachePath;


		//Ensure passed in pageSize is between 1 and 40. If not, set to 10 (default)
		$pageSize = (isset($pageSize) && $pageSize > 0 && $pageSize <= 40) ? $pageSize : 10;
		error_log('Starting refreshCache. $pageSize: ' . $pageSize);

		$catalogUrl = $baseUrl . 'catalog/products';
		error_log('catalogUrl: ' . $catalogUrl);

		$getParams = array (
			'pageSize'=>$pageSize
		);

		$catalogArray = array();
		$pageNum = 1;
		do {
			error_log('Starting Loop');
			$getParams['page'] = $pageNum;
			// $currUrl = $catalogUrl . 'page='.$pageNum;
			error_log('Current params: ' . print_r($getParams, true));
			$request = new RestRequest($catalogUrl, 'GET', $getParams);
			$request->execute();

			$resultDecoded = json_decode($request->getResponseBody());

			if ($resultDecoded->success) {
				foreach($resultDecoded->products as $product) {
					error_log('pushing Product to array: ' . print_r($product, true));
					array_push($catalogArray, $product);
				}
			} else {
				$msg = '';
				if (isset($resultDecoded->reasons)) {
					foreach($resultDecoded->reasons as $reason) {
						$msg .= $reason->message;
					}
				}
				error_log('Exception happened: '.$msg);
				throw new Exception('Error with catalog call: '.$msg);
			}

			$pageNum++;
		} while (isset($resultDecoded->nextPage));

		$fh = fopen($cachePath, 'w') or die('Cannot open catalog cache file.');
		fwrite($fh, print_r(json_encode($catalogArray), true));

		fclose($fh);
		return json_encode($catalogArray);
	}

	/**
	 * Reads the Product Catalog Data from the locally saved JSON cache. If no cache exists, this will refresh the catalog from Zuora first.
	 * @return A model containing all necessary information needed to display the products and rate plans in the product catalog
	 */
	public static function readCache($page){
		require('./config.php');

		if(!file_exists($cachePath)){
			error_log("Read catalog -> refresh catalog");
			return self::refreshCache();
		}
		$myFile = $cachePath;
		$fh = fopen($myFile, 'r');
		$catalogJson = fread($fh, filesize($myFile));
		fclose($fh);
		$catalog_groups = json_decode($catalogJson);

		if (!isset($page) || $page == 1) {
			//If filtering of products is enabled, remove the products without accepted categories
			if($filterProducts) {

				//Cycle through products
				foreach($catalog_groups as $i=>$product){

					//Null checking of fields
					if ($productFilterField != null && $product->$productFilterField != null) {

						//Check if product category is within our accepted values array. Performs case-insensitive check
						if (!in_array(strtoupper($product->$productFilterField),array_map('strtoupper',$productFilterValue))) {
							// error_log('Filtering out product '.print_r($catalog_groups[$i],true));
							unset($catalog_groups[$i]);
						} else {

							//Product is allowed. Now check RatePlans
							if ($filterProductRatePlans) {

								//Ensure product has rateplans
								if ($product->productRatePlans != null) {

									//Cycle through rateplans and check their filtering field
									foreach($product->productRatePlans as $j=>$rateplan) {

										//Ensure field and values are configured in config file
										if ($productRatePlanFilterField != null && $productRatePlanFilterValue != null) {

											//If value on RatePlan is null or is not in array of accepted values, filter it out
											if ($rateplan->$productRatePlanFilterField == null || !in_array(strtoupper($rateplan->$productRatePlanFilterField),array_map('strtoupper',$productRatePlanFilterValue))) {
												error_log('Filtering out RatePlan: '.print_r($rateplan,true));
												unset($product->productRatePlans[$j]);
											}
										} else {
											error_log('RatePlan filter field and-or value not configured properly in config file.');
										}
									}
								} else {
									error_log('Error retrieving rate plans on Product');
								}
							}
						}
					} else {
						error_log('Product or category is null. Will not filter products.');
					}
				}
			}
		} else if ($page==2) {
			//If filtering of products is enabled, remove the products without accepted categories
			if($filterAddOnProducts) {

				//Cycle through products
				foreach($catalog_groups as $i=>$product){

					//Null checking of fields
					if ($addOnProductFilterField != null && $product->$addOnProductFilterField != null) {

						//Check if product category is within our accepted values array. Performs case-insensitive check
						if (!in_array(strtoupper($product->$addOnProductFilterField),array_map('strtoupper',$addOnProductFilterValue))) {
							// error_log('Filtering out product '.print_r($catalog_groups[$i],true));
							unset($catalog_groups[$i]);
						} else {

							//Product is allowed. Now check RatePlans
							if ($filterAddOnProductRatePlans) {

								//Ensure product has rateplans
								if ($product->productRatePlans != null) {

									//Cycle through rateplans and check their filtering field
									foreach($product->productRatePlans as $j=>$rateplan) {

										//Ensure field and values are configured in config file
										if ($addOnProductRatePlanFilterField != null && $addOnProductRatePlanFilterValue != null) {

											//If value on RatePlan is null or is not in array of accepted values, filter it out
											if ($rateplan->$addOnProductRatePlanFilterField == null || !in_array(strtoupper($rateplan->$addOnProductRatePlanFilterField),array_map('strtoupper',$addOnProductRatePlanFilterValue))) {
												error_log('Filtering out RatePlan: '.print_r($rateplan,true));
												unset($product->productRatePlans[$j]);
											}
										} else {
											error_log('RatePlan filter field and-or value not configured properly in config file.');
										}
									}
								} else {
									error_log('Error retrieving rate plans on Product');
								}
							}
						}
					} else {
						error_log('Product or category is null. Will not filter products.');
					}
				}
			}
		} else {
			//If filtering of products is enabled, remove the products without accepted categories
			if($filterProducts || $filterAddOnProducts) {

				//Cycle through products
				foreach($catalog_groups as $i=>$product){

					//Null checking of fields
					if ($productFilterField != null && $product->$productFilterField != null && $addOnProductFilterField != null && $product->$addOnProductFilterField != null) {

						//Check if product category is within our accepted values array. Performs case-insensitive check
						if (!in_array(strtoupper($product->$productFilterField),array_map('strtoupper',$productFilterValue)) && !in_array(strtoupper($product->$addOnProductFilterField),array_map('strtoupper',$addOnProductFilterValue))) {
							// error_log('Filtering out product '.print_r($catalog_groups[$i],true));
							unset($catalog_groups[$i]);
						} else {

							//Product is allowed. Now check RatePlans
							if ($filterProductRatePlans || $filterAddOnProductRatePlans) {

								//Ensure product has rateplans
								if ($product->productRatePlans != null) {

									//Cycle through rateplans and check their filtering field
									foreach($product->productRatePlans as $j=>$rateplan) {

										//Ensure field and values are configured in config file
										if ($productRatePlanFilterField != null && $productRatePlanFilterValue != null && $addOnProductRatePlanFilterField != null && $addOnProductRatePlanFilterValue != null) {

											//If value on RatePlan is null or is not in array of accepted values, filter it out
											if (($rateplan->$productRatePlanFilterField == null || !in_array(strtoupper($rateplan->$productRatePlanFilterField),array_map('strtoupper',$productRatePlanFilterValue))) && ($rateplan->$addOnProductRatePlanFilterField == null || !in_array(strtoupper($rateplan->$addOnProductRatePlanFilterField),array_map('strtoupper',$addOnProductRatePlanFilterValue)))) {
												error_log('Filtering out RatePlan: '.print_r($rateplan,true));
												unset($product->productRatePlans[$j]);
											}
										} else {
											error_log('RatePlan filter field and-or value not configured properly in config file.');
										}
									}
								} else {
									error_log('Error retrieving rate plans on Product');
								}
							}
						}
					} else {
						error_log('Product or category is null. Will not filter products.');
					}
				}
			}
		}



		//Stores today's date in format returned by rateplans effective dates
		$today = date("Y-m-d");

		//Filter out rateplans based on EffectiveStartDate and EffectiveEndDate
		foreach($catalog_groups as $i=>$product) {
			error_log('Testing. i: '.$i.', Catalog Groups:'.print_r($catalog_groups,true));
			//Use this variable to hide Product if all its RatePlans are removed
			$hasActiveRatePlans = false;

			//Ensure Product has RatePlans
			if ($product->productRatePlans != null) {

				//Cycle through rateplans to check effective dates
				foreach($product->productRatePlans as $j=>$rateplan) {
					if ($rateplan->effectiveEndDate != null && $rateplan->effectiveStartDate != null) {
						
						//Ensure StartDate <= today <= EndDate
						if (($rateplan->effectiveEndDate >= $today && $rateplan->effectiveStartDate <= $today)) {
							$hasActiveRatePlans = true;
						} else {
							error_log('Removing RatePlan due to effective dates: '.print_r($rateplan,true));
							unset($product->productRatePlans[$j]);

						}
					}
				}
			}

			//Filter out Product if it has no active RatePlans
			if (!$hasActiveRatePlans) {
				error_log('Product has no active RatePlans. Removing Product '.print_r($product,true));
				error_log('i: '.$i);
				error_log('Catalog groups before: '.print_r($catalog_groups,true));
				unset($catalog_groups[$i]);
				error_log('Current catalog groups: '.print_r($catalog_groups,true));
			}

		}
		
		//Setting additional allowDuplicateRatePlans variable.
		$catalog_groups['allowDuplicateRatePlans'] = $allowDuplicateRatePlans;
		error_log("Catalog JSON Decode: " . print_r($catalog_groups, true), 0);

		return $catalog_groups;
	}


}

?>