<?php 

/**
 * 
 *  Short description for file:
 *	The SubscriptionManager class contains methods to create and  
 *  view details of the logged in user's Subscription.
 *
 */


include '../config.php';

class SubscriptionManager{
	
	private $cart;
	private $refId;
	private $email;
	private $billTo;
	private $soldTo;
	private $billMeLater;
	private $googleUser;
	
	/**
	 *	Constructor for class SubscriptionManager initializes the current instance
	 *	variable
	 *	@param $ref_id hpmCreditCardPaymentMethodId
	 *	@param $cart_items cart object
	 *	@param $email email address of customer
	 *	@param $billTo Bill to address
	 *	@param $soldTo Sold to address
	 */
	public function __construct($ref_id, $cart_items, $email, $billTo, $soldTo, $billMeLater, $crmId, $billingNumber ) {

        $this->refId = $ref_id;
        $this->cart = $cart_items;
        $this->email = $email;
        $this->billTo = $billTo;
        $this->soldTo = $soldTo;
        $this->billMeLater = $billMeLater;
	$this->crmId = $crmId;
	$this->billingNumber = $billingNumber ;


    }

    /**
	 *	subscribe method is the main method that makes the rest request
	 *	to create subscription with the help of other helper methods to build 
	 *	the subscription data
	 *	@return $result Rest response (JSON)
	 */
    public function subscribe(){
    	include './config.php';

		global $refId, $billMeLater;

		$hpmPaymentMethodId;
		$url = $baseUrl . "accounts";
		$billMe = $this->billMeLater;

		$data = $this->buildSubscriptionData();

		//Encode the array to JSON string
		$dataEncoded = json_encode($data);

		//Rest call to create account and subscription
		$requestPostWithParams = new RestRequest($url , 'POST', $dataEncoded);
		$requestPostWithParams ->execute();
		$result = $requestPostWithParams->getResponseBody();

		$reponse = $result;


		$result = json_decode($result);

		return $reponse;
	}

	/**
	 *	Builds Subscription data before creating subscription and account.
	 */
	private function buildSubscriptionData(){
		include './config.php';
		include './controller/Date.php';

		global $refId, $billMeLater;

		if($this->billMeLater == "true"){

			$invoiceCollect = false;
			$autopay = false;
			$hpmPaymentMethodId = $defaultPaymentMethodId;

		} else {
			//$invoiceCollect = $defaultInvoiceCollectNewSub;
			$invoiceCollect = false;
			$autopay = true; //$defaultAutopay;
			$hpmPaymentMethodId = $this->refId;
		}
		date_default_timezone_set('America/Los_Angeles');
		$date = date('Y-m-d\TH:i:s',time());
		$today = getdate();
		$mday = $today['mday'];
		//Build the data for the create account call. Leave account number field blank
 
		$data = array(
				"name" => $this->email, 
				"currency" => $defaultCurrency,
				"notes" => $defaultAccountNotes,
				"billCycleDay" => $mday,
				"autoPay" => $autopay,
				"invoiceTemplateId" => $defaultInvoiceTemplateId,
				"communicationProfileId" => $defaultCommunicationProfileId,
				"paymentTerm" => $defaultPaymentTerm,
				"Ext_Cust_Id__c" => $this->billingNumber, //"333456",
				"billToContact" => $this->getBillToContact(),
				"soldToContact" => $this->getSoldToContact(),
				"hpmCreditCardPaymentMethodId" => $hpmPaymentMethodId,
				"subscription" => $this->getSubscriptionDetails(),
				"invoiceCollect" => $invoiceCollect,
				"crmId" => $this->crmId,
				"invoiceTargetDate" => calcDate($defaultInvoiceTargetDate)); 
		
		  error_log("Subscription Test Data");
		  error_log($data);

	
		return $data;
	}

	/**
	 *	getBillToContact method builds the bill to contact address from the
	 *	billTo variable and the BillToIndex
	 *	@return $billToContact (array)bill to contact address
	 */
	public function getBillToContact(){

		$billToContact = array();
		$billToIndex = $this->getBillToIndex();
		$size = count($this->billTo); 

		for($i = 0; $i < $size; $i++) {
			//Build the array based on billTo Contact index
		  	$billToContact[$billToIndex[$i]] = $this->billTo[$i];
		}
		
		return $billToContact;
	}

	/**
	 *	getPreviewBillToContact method builds the bill to contact address from the
	 *	billTo variable and the BillToIndex
	 *	@return $billToContact (array)bill to contact address
	 */
	public function getPreviewBillToContact(){

		$billToContact = array();
		$billToIndex = $this->getPreviewBillToIndex();
		$size = count($this->billTo); 

		for($i = 0; $i < $size; $i++) {
			//Build the array based on billTo Contact index
		  	$billToContact[$billToIndex[$i]] = $this->billTo[$i];
		}
		return $billToContact;
	}

	/**
	 *	getPreviewBillToIndex() holds the key indexes for Bill To Address
	 *  @return Index for billTo contact details (array)
	 */
	function getPreviewBillToIndex(){
		$billToIndex = array("city" , "country" , "state", "zipCode");

		return $billToIndex;
	}

	/**
	 *	getSoldToContact method builds the sold to contact address from the
	 *	soldTo variable and the SoldToIndex
	 *	@return $soldToContact (array)sold to contact address
	 */
	public function getSoldToContact(){
		
		$soldToContact = array();
		$soldToIndex = $this->getSoldToIndex();
		$size = count($this->soldTo);

		for($i = 0; $i < $size; $i++) {
			//Build the array based on soldTo Contact index
		  	$soldToContact[$soldToIndex[$i]] = $this->soldTo[$i];
		}
		return $soldToContact;
	}

	/**
	 *	getBillToIndex() holds the key indexes for Bill To Address
	 *  @return Index for billTo contact details (array)
	 */
	function getBillToIndex(){
		$billToIndex = array("address1" , "address2", "city", 
							 "country", "workPhone", "zipCode",
							 "state", "firstName", "lastName","workEmail");

		return $billToIndex;
	}

	/**
	 *	getSoldToIndex holds the key indexes for Sold To Address
	 *  @return Index for soldTo contact details (array)
	 */
	function getSoldToIndex(){
		$soldToIndex = array("address1" , "address2", "city", 
							 "country", "workPhone", "zipCode",
							 "state", "firstName", "lastName","workEmail");

		return $soldToIndex;
	}

	/**
	 * getSubscriptionDetails() fetches the products to be subscribed for
	 * @return subscription details (array)
	 */
	function getSubscriptionDetails(){
		include './config.php';
		$qumulateuuid = $_SESSION['qumulateGroupUUID'];
		$subscriptionDet = array("termType" => $defaultTermType,
		    "notes" => $defaultSubscriptionNotes,
		    "subscribeToRatePlans" => $this->getProductRatePlans(),
		    "contractEffectiveDate" => calcDate($defaultCED),
			"serviceActivationDate"	=> calcDate($defaultSAD),
			"customerAcceptanceDate" => calcDate($defaultCAD),
			"qumulateGroupUUID__c" => $qumulateuuid,
			"termStartDate"	=> calcDate($defaultTSD));
			
	
		if ($defaultTermType == 'TERMED') {

			$subscriptionDet['initialTerm'] = $defaultInitialTerm;
			$subscriptionDet['renewalTerm'] = $defaultRenewalTerm;
			$subscriptionDet['autoRenew'] = $defaultAutoRenew;
		}

		return $subscriptionDet;
	}

	/**
	 * getProductRatePlans() fetches the product rate plans to be subscribed for
	 * @return rate plan details (array)
	 */
	function getProductRatePlans(){
		$prodRatePlans = array();
		$cartItems = $this->cart->cart_items;

		foreach($cartItems as $ci){
		 	array_push($prodRatePlans, $this->getProductRatePlanId($ci->ratePlanId, $ci->cartCharges));		 	
		 }
		
		return $prodRatePlans;
	}

	/** 
	 * getChargeOverrides($cartCharges) Fetch the charge over rides
	 * @param $cartCharges
	 * @return $chargeOverrides if the charge over rides are present else null
	 */
	function getChargeOverrides($cartCharges) {
		$chargeOverrides = array();

		if($cartCharges != null && count($cartCharges) > 0){	 	
			foreach($cartCharges as $cc){
			 	array_push($chargeOverrides, $this->getRatePlanChargeInfo($cc->chargeId, $cc->quantity));
			 }

			 return $chargeOverrides;
		} else {
			return null;
		}
	}

	/**
	 * getRatePlanChargeInfo($chargeId, $quantity) fetches Rate plan Charge Info
	 * @param $chargeId Rate plan charge id
	 * @param $quantity Quantity of the rate plan charge
	 * @return $ratePlanCharges Rate plan charge (array)
	 */
	function getRatePlanChargeInfo($chargeId, $quantity){
		$ratePlanCharges = array("productRatePlanChargeId" => $chargeId,
								 "quantity" => $quantity);
		return $ratePlanCharges;
	}

	/**
	 * getProductRatePlanId($ratePlanId, $cartCharges) fetches the rate plan Id's
	 * @param $ratePlanId
	 * @param $cartCharges
	 * @return $prodRatePlan (array)
	 */
	function getProductRatePlanId($ratePlanId, $cartCharges){
	
		$prodRatePlan = array("productRatePlanId" => $ratePlanId,
							  "chargeOverrides" => $this->getChargeOverrides($cartCharges));
		return $prodRatePlan;
	}

	
	/**
	 * getCurrentSubscription($accountName) returns the current subscription
	 * @param $accountName email address of current account holder
	 * @return $result
	 */
	public static function getCurrentSubscription($accountName){
		include ('./config.php');
		$resultArray = array();
		
		if ($accountName != null && $accountName != '') {

			$newUrl = $baseUrl . 'subscriptions/accounts/';
			$acctUrl = $newUrl . $accountName;
			$subscriptionResult = new RestRequest($acctUrl, 'GET', null);
			$subscriptionResult->execute();
			$result = $subscriptionResult->getResponseBody();
			
			$resultBodyDecode = json_decode($result);
			
			error_log("resultBodyDecode is: " . print_r($resultBodyDecode, true), 0);

			if($resultBodyDecode->success) {
				$resultArray['success'] = true;
				$resultArray['payload'] = $result;
				error_log("resultArray is: " . print_r($resultArray, true), 0);
				return $resultArray;	
			}
			else {
				error_log("Amender error message is: " . $resultBodyDecode->reasons[0]->message);
				$resultArray['success'] = false;
				$resultArray['payload'] = $resultBodyDecode->reasons[0]->message;
				return $resultArray;
			}
				return $result;
		} else {

			return null;
		}
	}
		
	/**
	 *  updateBillToContact($accountName) Function to update the Bill To Contact details of an account 
	 *  @param $accountName
	 *  @return response body (string)
	 */
 	public function updateBillToContact($accountName){
		include './config.php';

		if ($accountName != null && $accountName != '') {

			$billToContact = $this->getBillToContact();

			$data = json_encode(array("billToContact" => $billToContact));

			$newUrl = $baseUrl . 'accounts/' . $accountName;

			$restResult = new RestRequest($newUrl, 'PUT', $data);
			
			$restResult->execute();
			
			$result = $restResult->getResponseBody();

			return $result;
		} else {

			return null;
		}	
	}

	/**
	 *  updateSoldToContact($accountName) Function to update the Bill To Contact details of an account 
	 *  @param $accountName
	 *  @return response body (string)
	 */
	public function updateSoldToContact($accountName){
		include './config.php';

		if ($accountName != null && $accountName != '') {
			$soldToContact = $this->getSoldToContact();

			$data = json_encode(array("soldToContact" => $soldToContact));

			$newUrl = $baseUrl . 'accounts/' . $accountName;

			$restResult = new RestRequest($newUrl, 'PUT', $data);
			
			$restResult->execute();
			
			$result = $restResult->getResponseBody();

			return $result;
		} else {

			return null;
		}	
	}

	/**
	 *  PreviewSubscription() Function to preview the subscription before creating it 
	 *  @return response body (string)
	 */
	public function PreviewSubscription(){
		include './config.php';
		include './controller/Date.php';

		$subscribeTo = $this->getProductRatePlans();

	    $accountInfo = $this->getPreviewContactInfo();
       
		 $data = array("previewAccountInfo" => $accountInfo,
					  "termType" => $defaultTermType,
					  "contractEffectiveDate" => calcDate($defaultCED),
					  "serviceActivationDate" => calcDate($defaultSAD),
					  "customerAcceptanceDate" => calcDate($defaultCAD),
					  "termStartDate" => calcDate($defaultTSD),
					  "invoiceTargetDate" => calcDate($defaultInvoiceTargetDate),
					  "subscribeToRatePlans" => $subscribeTo); 
		/* $accountInfo = "A00000998";
		$data = array("accountKey" => $accountInfo,
					  "termType" => $defaultTermType,
					  "contractEffectiveDate" => calcDate($defaultCED),
					  "serviceActivationDate" => calcDate($defaultSAD),
					  "customerAcceptanceDate" => calcDate($defaultCAD),
					  "termStartDate" => calcDate($defaultTSD),
					  "invoiceTargetDate" => calcDate($defaultInvoiceTargetDate),
					  "subscribeToRatePlans" => $subscribeTo); */
					  

		if($defaultTermType == "TERMED"){
			$data['initialTerm'] = $defaultInitialTerm;
		}

		$newUrl = $baseUrl . 'subscriptions/preview';

		$restResult = new RestRequest($newUrl, 'POST', json_encode($data));
		
		$restResult->execute();
		
		$result = $restResult->getResponseBody();

		return $result;
	}

	/**
	 *	getPreviewContactInfo() Function fetchs the contact information to make the
	 *   preview subscription call.
	 * 	@return $accountInfo 
	 */
	function getPreviewContactInfo(){
		include './config.php';

		date_default_timezone_set('America/Los_Angeles');
		$date = date('Y-m-d\TH:i:s',time());
		$today = getdate();
		$defaultBillCycleDay = $today['mday'];
		
		if($this->billTo == null){

			return array("currency" => $defaultCurrency,
						 "billCycleDay" => $defaultBillCycleDay);
		} else {

			return array("currency" => $defaultCurrency,
						 "billCycleDay" => $defaultBillCycleDay,
						 "billToContact" => $this->getPreviewBillToContact());
		}
	}

	public static function getCurrentSubscription1($accountNumber){

		$zapi = new zApi();

		//Get Account Id of current User
		$accountId = null;
		$accResult = $zapi->zQuery("SELECT Id FROM Account WHERE AccountNumber='A00003679'");
		foreach($accResult->result->records as $acc){
			$accountId = $acc->Id;
		}
		
		if($accountId==null){
			throw new Exception('ACCOUNT_DOES_NOT_EXIST');
		}

		//Get Active Subscription
		$activeSub = new SubscriptionManager();

		$subResult = $zapi->zQuery("SELECT Id,Name,Status,Version,PreviousSubscriptionId,ContractEffectiveDate,TermStartDate FROM Subscription WHERE AccountId='".$accountId."' AND Status='Active'");
		foreach($subResult->result->records as $sub){
			$activeSub->subId = $sub->Id;
			$activeSub->Version = $sub->Version;
			$activeSub->subscriptionNumber = $sub->Name;
		}
		
		if($subResult->result->size==0){
			throw new Exception('SUBSCRIPTION_DOES_NOT_EXIST');
		}

		date_default_timezone_set('America/Los_Angeles');
		$activeSub->userEmail = $accountNumber;
		$activeSub->endOfTermDate = date('Y-m-d\TH:i:s',time());
		$activeSub->startDate = $sub->TermStartDate;

		$curDate = date("Y-m-d") .'T00:00:00.000-08:00';
		//Get Existing Rate Plans
		$activeSub->active_plans = array();
		$activeSub->removed_plans = array();
		$rpResult = $zapi->zQuery("SELECT Id,Name,ProductRatePlanId FROM RatePlan WHERE SubscriptionId='".$activeSub->subId."'");	
		foreach($rpResult->result->records as $rp){
			$newPlan = new Amender_Plan();
			$newPlan->Id = $rp->Id;
			$newPlan->Name = $rp->Name;
			//Get Product Name
			$prpResult = $zapi->zQuery("SELECT Description,ProductId FROM ProductRatePlan WHERE Id='".$rp->ProductRatePlanId."'");
			$newPlan->Description = (isset($prpResult->result->records[0]->Description) ? $prpResult->result->records[0]->Description : '');
			$pResult = $zapi->zQuery("SELECT Name FROM Product WHERE Id='".$prpResult->result->records[0]->ProductId."'");
			$newPlan->ProductName = $pResult->result->records[0]->Name;

			//Get all charges
			$newPlan->amender_charges = array();
			$rpcResult = $zapi->zQuery("SELECT Id,Name,ProductRatePlanChargeId,ChargeModel,ChargeType,UOM,Quantity,ChargedThroughDate FROM RatePlanCharge WHERE RatePlanId='".$rp->Id."'");
			foreach($rpcResult->result->records as $rpc){
				$newCharge = new Amender_Charge();
				$newCharge->Id = $rpc->Id;
				$newCharge->Name = $rpc->Name;
				$newCharge->ChargeModel = $rpc->ChargeModel;
				$newCharge->ProductRatePlanChargeId = $rpc->ProductRatePlanChargeId;
				if($rpc->ChargeModel!='Flat Fee Pricing'){
					$newPlan->uom = $rpc->UOM;
					$newPlan->quantity = $rpc->Quantity;
					$newCharge->Uom = $rpc->UOM;
					$newCharge->Quantity = $rpc->Quantity;
				}
				//For all charges, find maximum ChargedThroughDate					
				if(isset($rpc->ChargedThroughDate)){
					if($rpc->ChargedThroughDate > $activeSub->endOfTermDate){
						$activeSub->endOfTermDate = $rpc->ChargedThroughDate;
					}
				}
				array_push($newPlan->amender_charges, $newCharge);
			}
			array_push($activeSub->active_plans, $newPlan);
		}
		//Get Removed Rate Plans
		$rpResult = $zapi->zQuery("SELECT Id,Name,AmendmentType,AmendmentId,ProductRatePlanId FROM RatePlan WHERE SubscriptionId='".$activeSub->subId."' AND AmendmentType='RemoveProduct'");	
		foreach($rpResult->result->records as $rp){
			$newPlan = new Amender_Plan();
			$newPlan->Id = $rp->Id;
			$newPlan->Name = $rp->Name;

			//Get Product Name
			$prpResult = $zapi->zQuery("SELECT Description,ProductId FROM ProductRatePlan WHERE Id='".$rp->ProductRatePlanId."'");
			$newPlan->Description = (isset($prpResult->result->records[0]->Description) ? $prpResult->result->records[0]->Description : '');
			$pResult = $zapi->zQuery("SELECT Name FROM Product WHERE Id='".$prpResult->result->records[0]->ProductId."'");
			$newPlan->ProductName = $pResult->result->records[0]->Name;
			
			$newPlan->AmendmentId = $rp->AmendmentId;
			$newPlan->AmendmentType = $rp->AmendmentType;
			$newPlan->effectiveDate = 'end of current billing period.';
			
			//Query Amendment for this rate plan to get Effective Removal Date
			$amdResult = $zapi->zQuery("SELECT Id,ContractEffectiveDate FROM Amendment WHERE Id='".$newPlan->AmendmentId."'");		
			foreach($amdResult->result->records as $amd){
				$newPlan->effectiveDate = $amd->ContractEffectiveDate;
			}

			//Get all charges
			$newPlan->amender_charges = array();
			$rpcResult = $zapi->zQuery("SELECT Id,Name,ProductRatePlanChargeId,ChargeModel,ChargeType,UOM,Quantity,ChargedThroughDate FROM RatePlanCharge WHERE RatePlanId='".$rp->Id."'");
			foreach($rpcResult->result->records as $rpc){
				$newCharge = new Amender_Charge();
				$newCharge->Id = $rpc->Id;
				$newCharge->Name = $rpc->Name;
				$newCharge->ChargeModel = $rpc->ChargeModel;
				$newCharge->ProductRatePlanChargeId = $rpc->ProductRatePlanChargeId;
				if(isset($rpc->UOM)){
					$newPlan->uom = $rpc->UOM;
					$newPlan->quantity = $rpc->Quantity;
					$newCharge->Uom = $rpc->UOM;
					$newCharge->Quantity = $rpc->Quantity;
				}
				//For all charges, find maximum ChargedThroughDate					
				if(isset($rpc->ChargedThroughDate)){
					if($rpc->ChargedThroughDate > $activeSub->endOfTermDate){
						$activeSub->endOfTermDate = $rpc->ChargedThroughDate;
					}
				}
				array_push($newPlan->amender_charges, $newCharge);
			}
			array_push($activeSub->removed_plans, $newPlan);
		}
		return $activeSub;
	}
	
	//Function to get acc number and redirect user based on acc number
	
	public static function getAccountNumber($quuid){
		
		include 'zApi.php';
		$zapi = new zApi();

			
		//Get Account Id of current User
		$accountId = null;
		$accID = $zapi->zQuery("SELECT AccountId FROM Subscription WHERE qumulateGroupUUID__c='".$quuid."' and qumulateGroupUUID__c!='' and status='Active' ");

		$accResult = $zapi->zQuery("SELECT AccountNumber FROM Account WHERE Id='".$accID->result->records[0]->AccountId ."'");

		

		foreach($accResult->result->records as $acc){
			$accountId = $acc->AccountNumber;
		}

	return $accountId;
	}

}

?>