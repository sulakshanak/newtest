<?php

/**
 * 
 *  Short description for file:
 *	 The AccountManager class manages Account information for the 
 *  logged in user.
 *
 *
 * V1.0
 */

class AccountManager{
	/**
	 * getAccountDetail($accountName) : Gets account information for the 
	 * given account name
	 * @param $accountName Name of the target account
	 * @return reponse body of the get account REST call
	 */
	public static function getAccountDetail($accountName){
		include './config.php';
		
		if ($accountName != null && $accountName != '') {

			$newUrl = $baseUrl . 'accounts/';
			$newUrl = $newUrl . $accountName . '/summary';

			//REST api call
			$restResult = new RestRequest($newUrl, 'GET', null);
			$restResult->execute();
			
			//Fetch the response body
			$result = $restResult->getResponseBody();

			return $result;
		} else {
			return null;
		}
	}
	
	/**
	 * getPaymentMethodDetail($accountName) function queries Zuora using REST api to 
	 * fetch all payment method details 
	 * @param $accountName Name of the target account
	 * @return reponse body of the get account REST call
	 */
	public static function getPaymentMethodDetail($accountName){
		include './config.php';
		
		if ($accountName != null && $accountName != '') {
			//Prepare the correct URL to be called
			$newUrl = $baseUrl . '/payment-methods/credit-cards/accounts/';
			$newUrl = $newUrl . $accountName;

			//REST api call
			$restResult = new RestRequest($newUrl, 'GET', null);
			$restResult->execute();

			//Fetch the response body
			$result = $restResult->getResponseBody();

			return $result;
		} else {

			return null;
		}
	}

	/**
	 * getInvoiceHistory($accountName) function queries the Zuora using REST api to 
	 * fetch all Invoice history details 
	 * @param $accountName Name of the target account
	 * @return reponse body of the get account REST call
	 */
	public static function getInvoiceHistory($accountName){
		include './config.php';
		
		if ($accountName != null && $accountName != '') {
			//Prepare the correct URL to be called
			$newUrl = $baseUrl . 'transactions/invoices/accounts/';
			$newUrl = $newUrl . $accountName.'?pageSize=10';

			//REST api call
			$restResult = new RestRequest($newUrl, 'GET', null);
			$restResult->execute();
			
			//Fetch the response body
			$result = $restResult->getResponseBody();

			return $result;
		} else {

			return null;
		}

	}

	/**
	 * getInvoiceHistory($accountName) function queries the Zuora using REST api to 
	 * fetch all Invoice history details 
	 * @param $accountName Name of the target account
	 * @return reponse body of the get account REST call
	 */
	public static function getInvoiceForPayment($accountName){
		include './config.php';
		
		if ($accountName != null && $accountName != '') {

			//Prepare the correct URL to be called
			$newUrl = $baseUrl . 'accounts/';
			$newUrl = $newUrl . $accountName . '/summary';

			//REST api call
			$restResult = new RestRequest($newUrl, 'GET', null);
			$restResult->execute();
			
			//Fetch the response body
			$accountResult = $restResult->getResponseBody();

			$account = json_decode($accountResult);

			$accountBalance = $account->basicInfo->balance;
			if($accountBalance > 0){
				//fetch invoices
				$continue = true;
				$payInvoices = array();

				while($continue){

					$result = AccountManager::getInvoiceHistory($accountName);
					$responseResult = json_decode($result);
					$invoices = $responseResult->invoices;
					$oneTime = false;
					$balance = 0;

					for($i = 0; $i < count($invoices); ++$i) {

					    if($invoices[$i]->balance > 0){
					    	array_push($payInvoices, $invoices[$i]);
					    	$balance = $balance + $invoices[$i]->balance;
					    }

					    if($balance >= $accountBalance){
					    	$continue = false;
					    }
					}

					if(!array_key_exists('nextPage', $responseResult)){
						$continue = false;
					}
				}
				
			} else {
				//No invoices are available for payment processing
				$payInvoices = null;
			}
			return $payInvoices;
		} else {
			return null;
		}

	}


	/**
	 * getPaymentHistory($accountName) function queries the Zuora using REST api to 
	 * fetch all payment history details
	 * @param $accountName Name of the target account
	 * @return reponse body of the get account REST call
	 */
	public static function getPaymentHistory($accountName){
		include './config.php';

		if ($accountName != null && $accountName != '') {

			//Prepare the correct URL to be called
			$newUrl = $baseUrl . 'transactions/payments/accounts/';
			$newUrl = $newUrl . $accountName.'?pageSize=10';

			//REST api call
			$restResult = new RestRequest($newUrl, 'GET', null);
			$restResult->execute();
			
			//Fetch the response body
			$result = $restResult->getResponseBody();

			return $result;
		} else {
			return null;
		}

	}

	/**
	 * getUsageHistory($accountName) function queries the Zuora using REST api to 
	 * fetch all payment history details 
	 * @param $accountName Name of the target account
	 * @return reponse body of the get account REST call
	 */
	public static function getUsageHistory($accountName){
		include './config.php';

		if ($accountName != null && $accountName != '') {

			//Prepare the correct URL to be called
			$newUrl = $baseUrl . 'usage/accounts/';
			$newUrl = $newUrl . $accountName;

			//REST api call
			$restResult = new RestRequest($newUrl, 'GET', null);
			$restResult->execute();
			
			//Fetch the response body
			$result = $restResult->getResponseBody();

			return $result;

		} else {
			return null;
		}
	}

	/**
	 * checkEmailAvailability($email) function queries the Zuora using REST api to 
	 * check if the email is already available
	 * @param $email email address of the account to be created or log in
	 * @return true if email is not available
	 *		   false if email is available
	 */
	public static function checkEmailAvailability($email){
		include './config.php';

		if ($email != null && $email != '') {

			//Prepare the correct URL to be called
			$newUrl = $baseUrl . 'accounts/';
			$newUrl = $newUrl . $email;

			//REST api call
			$restResult = new RestRequest($newUrl, 'GET', null);
			$restResult->execute();
			
			//Fetch the response body
			$result = $restResult->getResponseBody();
			error_log($result);
			//Decode string response in to JSON object
			$result = json_decode($result, true);
			$success = $result['success'];

			//Return value based on the success value of the response body
			if ($success){
				return false;
			} else {
				return true;
			} 
		} else {
			return null;
		}
	}
}

?>