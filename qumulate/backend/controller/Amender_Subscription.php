<?

private $client;
 private $header;

 /**
  * Creating a new zApi instance will log in using a SOAP login call, and will generate a session header which can be used for subsequent API calls. If the credentials set up in config.php are invalid, the instantiation of the class will throw an exception.
  */
 public function __construct(){
  include("./config.php");
  
 }

 
 public function printXml(){
  $soap_request = $this->client->__getLastRequest(); 
  $soap_response = $this->client->__getLastResponse(); 
  echo("<pre>");
  echo "SOAP request:<br>" . htmlentities($soap_request) . "<br><br>"; 
  echo "SOAP response:<br>" . htmlentities($soap_response) . "<br><br>";
  echo("</pre>");
  echo "<br>";
 }

?>