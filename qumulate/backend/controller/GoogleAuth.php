<?php

/**
 * 
 *  Short description for file:
 *	The GoogleAuth file contains methods to handle various operations involved
 *  in signing up and login google+ users 
 *
 * V1.0
 */
require realpath(dirname(__FILE__).'/..').'/GoogleApi/Google_Client.php';
require realpath(dirname(__FILE__).'/..').'/GoogleApi/contrib/Google_Oauth2Service.php';

function googleAuth($code){
	//global $messages;
	include('config.php');
	$gClient = new Google_Client();
	$gClient->setClientId($google_client_id);
	$gClient->setClientSecret($google_client_secret);
	$gClient->setRedirectUri($google_redirect_url);
	$gClient->setDeveloperKey($google_developer_key);

	$google_oauthV2 = new Google_Oauth2Service($gClient);
	error_log("Google code");
	error_log($code);
	$gClient->revokeToken();

	//If code is empty, redirect user to google authentication page for code.
	//Code is required to aquire Access Token from google
	//Once we have access token, assign token to session variable
	//and we can redirect user back to page and login.
	if ($code != null && !isset($_SESSION['token'])) 
	{ 
		$gClient->authenticate($code);
		$_SESSION['token'] = $gClient->getAccessToken();
	}

	if (isset($_SESSION['token'])) 
	{ 
		$gClient->setAccessToken($_SESSION['token']);
	}


	if ($gClient->getAccessToken()) 
	{
		//For logged in user, get details from google using access token
		$user 				= $google_oauthV2->userinfo->get();
		$user_id 				= $user['id'];
		$user_name 			= filter_var($user['name'], FILTER_SANITIZE_SPECIAL_CHARS);
		$email 				= filter_var($user['email'], FILTER_SANITIZE_EMAIL);
		$profile_url 			= filter_var($user['link'], FILTER_VALIDATE_URL);
		$profile_image_url 	= filter_var($user['picture'], FILTER_VALIDATE_URL);
		$_SESSION['token'] 	= $gClient->getAccessToken();
		$_SESSION['googleUser'] = true;
		$_SESSION['googleUserInfo'] =  array("google_id" => $user_id,
											   "google_name" => $user_name,
											   "google_email" => $email,
											   "google_link" => $profile_url,
											   "google_picture_link" => $profile_image_url);

		

		//Check if the user already has an account with Zuora
		$dbInst = new MysqlRequest();

		$zuoraAccountQuery = $dbInst->execute("SELECT zuora_account_number FROM google_users WHERE google_id =  '$user_id'");
		$result =  array("user", $user);

		if(intval($zuoraAccountQuery->num_rows) > 0){

			$ZaccNum = $zuoraAccountQuery->fetch_row();
			$_SESSION['email'] = $ZaccNum[0];
			$result =  array("account", "Redirect");
		} else {
			error_log("JI - Z Account not available");
		}
	}
	else 
	{
		//For Guest user, get google login url
		$authUrl = $gClient->createAuthUrl();
		$result = array("url", $authUrl);
	}


	return $result;
}

?>