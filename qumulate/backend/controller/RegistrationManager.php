<?php


/**
 * 
 *  Short description for file:
 *	The SubscriptionManager class contains methods to create and  
 *  view details of the logged in user's Subscription.
 *
 * V1.0
 */



//require realpath(dirname(__FILE__).'/..').'/model/db_connect.php';

class RegistrationManager{
	public function __construct() {

    }



	/**
	 *	registerGoogleUser method is the method that handles all new google
	 * 	user registration. It inserts the google user details on to the database
	 *
	 *	@param $result [JSON] Rest response after creating account on Zuora 
	 *
	 */
	public function registerGoogleUser($accountNumber, $dbInstance){

		//Extract data
		$zuoraAccountNumber = $accountNumber;
		$google_user_info = $_SESSION['googleUserInfo'];
		$user_id = $google_user_info['google_id'];
		$user_name = $google_user_info['google_name'];
		$email = $google_user_info['google_email'];
		$profile_url = $google_user_info['google_link'];
		$profile_image_url = $google_user_info['google_picture_link'];

		//Create DB instance and insert data
		if ($insert_stmt = $dbInstance->prepare("INSERT INTO google_users (google_id,  zuora_account_number, google_name, google_email, google_link, google_picture_link) 
			VALUES (?, ?, ?, ?, ?, ?)")) {
		        $insert_stmt->bind_param('ssssss', $user_id, $zuoraAccountNumber, $user_name,$email,$profile_url,$profile_image_url);
		        // Execute the prepared query.
		        if (! $insert_stmt->execute()) {
		            header('Location: ../error.php?err=Registration failure: INSERT');
		        }
		    }

		return true;
		
	}

}
?>