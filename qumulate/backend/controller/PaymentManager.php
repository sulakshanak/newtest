<?php
/**
 * PaymentManager class manages existing PaymentMethods, and create Source URLs to 
 * generate Iframes to enter new credit cards.
 *
 *
 * V1.0
 */
class PaymentManager{

	/**
	 * Generates a URL for a new subscriber to enter credit card and contact information.
	 * @return New PaymentMethod URL
	 */
	public static function getNewAccountUrl(){

		$URL = PaymentManager::generateUrl() . "&retainValues=true";
		return $URL;
	}

	/**
	 * Used to generate the Base URL for both Existing and New accounts, using the configured Z-Payments Page information
	 * @return Base HPM URL
	 */
	public static function generateUrl(){		
		include("./config.php");

		//generate random token
		$token_length = 32;
		$token_bound = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
		$token = "";
		while(strlen($token) < $token_length) {
			$token .= $token_bound{mt_rand(0, strlen($token_bound)-1)};
		}

		//get current time in utc milliseconds
		list($usec, $sec) = explode(" ", microtime());
		$timestamp = (float)$sec - 2;
		$queryString = 'id=' . $pageId . '&tenantId=' . $tenantId . '&timestamp=' . $timestamp . '000&token=' . $token;

		//concatenate API security key with query string
		$queryStringToHash = $queryString . $apiSecurityKey;
		//get UTF8 bytes
		$queryStringToHash = utf8_encode($queryStringToHash);
		//create MD5 hash
		$hashedQueryString = md5($queryStringToHash);
		//encode to Base64 URL safe
		$hashedQueryStringBase64ed = strtr(base64_encode($hashedQueryString), '+/', '-_');
		//formulate the url
		$iframeUrl = $appUrl . "/apps/PublicHostedPaymentMethodPage.do?" .
			"method=requestPage&" .
			$queryString . "&" .
			"signature=" . $hashedQueryStringBase64ed; //. "&field_Address1=Visa";

		return $iframeUrl;
	}

	/**
	 *	Generates a URL for an existing User to enter credit card and contact information.
	 *
	 *	@return New PaymentMethod URL
	 *
	 */
	public static function getExistingAccountUrl($accountName){

		$URL = PaymentManager::generateUrl() . "&retainValues=true" . '&field_accountId=' . $accountName;
		return $URL;
	}
	
	/**
	 * changeDefaultPaymentMethod($accountName, $pmId) sets the default payment method of the given user to a 
	 * different paymentmethod on their account
	 *
	 * @param $accountName Name of the target account
	 * @param $pmId ID of new active payment method
	 *
	 * @return response body (String)
	 *
	 */
	public static function changeDefaultPaymentMethod($accountName, $pmId){

		include './config.php';
		$data = json_encode(array("defaultPaymentMethod" => true));

		$newUrl = $baseUrl . 'payment-methods/credit-cards/' . $pmId;
		$restResult = new RestRequest($newUrl, 'PUT', $data);
		
		$restResult->execute();
		
		$result = $restResult->getResponseBody();

		return $result;
	}

	/**
	 * removePaymentMethod($accountName, $pmId) sets the default payment method of the given user to 
	 * a different paymentmethod on their account
	 *
	 * @param $accountName Name of the target account
	 * @param $pmId ID of new active payment method
	 *
	 * @return response body (String)
	 *
	 */
	public static function removePaymentMethod($accountName, $pmId){
		include './config.php';

		$newUrl = $baseUrl . 'payment-methods/' . $pmId;

		$restResult = new RestRequest($newUrl, 'DELETE', null);
		
		$restResult->execute();
		
		$result = $restResult->getResponseBody();

		return $result;
	}

	/**
	 * payInvoice($accountId, $invoiceId) pays the invoice amount using default payment method of the 
	 * given user 
	 *
	 * @param $accountId Id of the target account
	 * @param $invoiceId Invoice ID of invoice to be payed
	 *
	 * @return response body (String)
	 *
	 */
	public static function payInvoice($requestData){
		include './config.php';

		$newUrl = $baseUrl . 'operations/invoice-collect';

		$restResult = new RestRequest($newUrl, 'POST', json_encode($requestData));
		
		$restResult->execute();
		
		$result = $restResult->getResponseBody();

		return $result;
	}

}

?>