<?php


class  CorsManager {
	
	private $url;
	private $method;
	private $data;
	private $signature;
	private $token;
	private $success;

	public function __construct($url=null, $method=null, $data=null) {

		$this->url = $url;
		$this->method = $method;
		$this->data = $data;
		$this->signature = null;
		$this->token = null;
	}

	public function validateCors() {
		include './model/RestRequest.php';

		if ($this->data == null) {
			return 'Data cannot be null';
		} else if ($this->url == null) {
			return 'URL cannot be null';
		} else if ($this->method == null) {
			return 'Method cannot be null';
		} else {
			$restRequest = new RestRequest($this->url, $this->method, $this->data);
			$restRequest -> execute();
			$result = $restRequest -> getResponseBody();
			$resultDecoded = json_decode($result,true);
			$this->success = $resultDecoded['success'];
			if ($this->success) {
				$this->signature = $resultDecoded['signature'];
				$this->token = $resultDecoded['token'];
			}
			return $result;
		}
	}


	public function getUrl() {
		return $this->url;
	}

	public function setUrl($newUrl) {
		$this->url = $newUrl;
	}

	public function getSignature() {
		return $this->signature;
	}

	public function getToken() {
		return $this->token;
	}

	public function getData() {
		return $this->data;
	}

	public function setData($newData) {
		$this->data = $newData;
	}
}




?>