<?php

//require('../../../simplesamlphp/lib/_autoload.php');
//session_start();

?>  
<!DOCTYPE html>
<html>
  <head>
    <title><?= $title ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap -->
<link href="./css/header.css" rel="stylesheet"/>
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="css/base.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="js/jquery.js" ></script>
<script type="text/javascript">
	$(document).ready(function(){
		
		getRedirectUrl(document.getElementById('qumulateGroupUUID').value);
		

	});
	
	function getRedirectUrl(quuid){

		
		$.getJSON("backend/index.php?type=getRedirectUrl",{quuid:quuid},
                    function(data){
                        if(data.success){
                            var config = data.msg; 
							if(config!=""){
								
								document.getElementById('email').value = config;

								$("#oktaform").attr("action", document.getElementById("accpg").value);
								

							} else{
								$("#oktaform").attr("action", document.getElementById("selectpdtpg").value);	
							}
                            $("#oktaform").submit();
                        }
                    }
                );
		
	}
</script>
  </head>
  <body style="padding-top: 60px">

	<div class="navbar navbar-fixed-top navbar-inverse">
	  <div class="navbar-inner">
	    <div class="container">
	      <div class="col-md-8"> 
	        <!-- ===================== --> 
	        <!-- Logo --> 
	        <!-- ===================== --><a class="brand" style="letter-spacing:0.1em;" href="/;jsessionid=ZxOEWa_zn9XWxiBBTNOCsBbR.prod-app01"> VARIAN | Qumulate<span style="font-size:16px;position:relative;top:-3px">&trade;</span></a>
	        <ul class="nav">
	          <li id="aboutTab"><a href="#about">About</a></li>
	        </ul>
	      </div>
	      <div class="col-md-4">

	      </div>
	    </div>
	  </div>
	</div>


    <div class="container">
		

					
		<?php 

			include 'backend/config.php';
			$url=$_SERVER['REQUEST_URI']; $arr1 = urldecode($url); //echo $arr1; exit;

			$qumulateGroupUUID = "";
			$params = explode("?", $url);

			$param = urldecode($params[1]); 

			$newparam = explode("&", $param);

			foreach($newparam as $value)
			{
				$val = explode("=", $value);
				
				if($val[0] == "qumulateGroupUUID")
				{
					$qumulateGroupUUID = $val[1];

				}
				if($val[0] == "oktaid")
				{
					$oktaid = $val[1];
				}
				if($val[0] == "returnUrl")
				{
					$returnurl = $val[1];
				}


			} 
			
		?>
		
			<!--------Redirect User based on UUID Starts-------->
			
			<input type="hidden" id="accpg" name="accpg" value="<?php echo $domainURL ?>account.php" ><br>
			<input type="hidden" id="selectpdtpg" name="selectpdtpg" value="<?php echo $domainURL ?>select_products.php"><br>
			
			<!--------Redirect User based on UUID Ends-------->
			
		<form action="" id="oktaform" method="post">
			<input type="hidden" id="oktaid" name="oktaid" value="<?php echo $_REQUEST[oktaid]; ?>" ><br>
			<input type="hidden" id="qumulateGroupUUID" name="qumulateGroupUUID" value="<?php echo $qumulateGroupUUID; ?>"><br>
			<input type="hidden" id="returnurl" name="returnurl" value="<?php echo $returnurl; ?>"><br>
			<input type="hidden" id="email" name="email" value=""><br>
		</form>



    </div>

  </body>
</html>