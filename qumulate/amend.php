<?php

error_reporting(E_ERROR | E_PARSE);

	$email=$_POST['email']; 
	$_SESSION['email']=$email;
	$domainurl=explode("/",$_POST['returnurl']);



?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Change Subscription</title>

	<!-- Style Sheets -->
	<link rel="stylesheet" href="css/selectproducts.css" />
	<link rel="stylesheet" href="css/bootstrap.css" />
	<link href="css/bootstrap-2.3.2.css" rel="stylesheet" type="text/css" />

	<style>
	   	body { padding-top: 70px; }
		.bigFont { font-size: 15px; }
		.inputSize { width: 50px; }
  	</style>
	
	<!-- Javascript Files -->
	<script src="js/jquery.js" ></script>
	<script src="js/function.js" ></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/jquery.blockUI.js"></script>
	<script src="js/bootbox.js"></script>
	<script type="application/javascript" >


		$(document).ready(function(){
	$( "#header" ).load( "loggedHeader1.html");
			$("#footer").load("footer.html");
			$("#infor").hide();
			$(".loading").hide();
			$('.accept-change').hide();
			$('.upcoming-changes').hide();

			$.ajaxSetup({
			    cache: false
			});
			
			$.getJSON("backend/index.php?type=IsUserLoggedIn",
			function(data){
				if(!data.success) { 
					if(data.msg[0].msg=='SESSION_NOT_SET'){
						console.log("Session not set");
                        			//window.location.replace('https://test.qumulate.varian.com/index.xhtml');
		
						var logout = "<?php include 'backend/config.php'; echo $logout;  ?>";
						window.location.replace(logout);

					}
					} else{
					
					
					//Retrieve Product Catalog from backend cache
					$.getJSON("backend/index.php?type=ReadCatalog", {page:3},
						function(data) {
							if(!data.success){
								alert("Error reading catalog cache.");
							}
							else{
								listProducts(data.msg);
							} 
						}
					);
					getCurrentSubscription();
					getAccountSummary();
					showProductsAdded();
					//showPreview();
				}
			}
		    );
		});

		function clearAmendments() {
			
			$.getJSON("backend/index.php?type=ClearAmendments", 
				function(data) {
					if(!data.success){
						console.log("Error clearing cart.");
					}
					else{
						alert('Cart Cleared');
					} 
	        	}
			);
		}

		/**
		 * Validates that the input is a number.
		 * @param  number 	evt 
		 * @return Boolean 	true if a number, false if not a number.
		 */
		function isNumberKey(evt){
			var charCode = (evt.which) ? evt.which : event.keyCode
			if (charCode > 31 && (charCode < 48 || charCode > 57))
				return false;
			return true;
		}

		/**
		 * Retrieves the most recent subscription.
		 * @return invokes loadPlan with JSON encoded message if successful, errors if not.
		 */
		var getCurrentSubscription = function(){
			$("#loading").show('fast');
			// alert('undo amend calling getCurrentSubscription');
			// $.getJSON("backend/index.php?type=EmptyCart");	//Cleans add/remove/update objects after "SAVE".
			// Get Subscription Information
			console.log("getCurrentSubscription called!");
			$.getJSON("backend/index.php?type=GetLatestSubscription",
				function(data){
					if(!data.success) {	
						alert("Error is: " + data.msg[0].msg);
					} else { 
						loadPlan(data.msg); 
					}
					
	        	}
			);

			$(".chosen_plans").html("Loading Subscription");
			$(".amendment_list").html("Loading Subscription");
		}

		/**
		 * Loads and displays the current subscription.
		 * @param  msg JSON encoded String depicting current subscription.
		 * @return renders current subscription.
		 */
		var loadPlan = function(msg) {
			$("#loading").fadeOut('fast');
			var obj = JSON.parse(msg);
			var cancelOption;
			var html = "";
			var removeHtml = "";
			var saveHtml = "";
			var cancelHtml = "";
			
			
			for(var i in obj.subscriptions[0].ratePlans) {
				var ratePlan = obj.subscriptions[0].ratePlans[i];

				//Only look at RatePlans that haven't been removed
				if(ratePlan.lastChangeType != "Remove"){

					html+="<div class='panel-body' style='border: none'; color:#333;>";

					for(var j in obj.subscriptions[0].ratePlans[i].ratePlanCharges){
						var citem = obj.subscriptions[0].ratePlans[i].ratePlanCharges[j];

						if(citem.type != 'Usage' && ((citem.model == 'PerUnit') || (citem.model == 'Tiered' ) || (citem.model == 'Volume')	)	) {
							
								var pricingSummary = citem.pricingSummary;
								var uom = pricingSummary.split('/')[1];

								html+="<span class='rateplan_name' style='font-size: 15px;'>"+citem.name+" </span>";
								html+=" <br><input type='text' style='width:50px;' id='update_field_"+citem.productRatePlanChargeId+"' value='" +citem.quantity+ "' /> Each  ";	//HTML ID is capturing this specific field.

								html+="<input type='hidden' id='original_value_" + citem.productRatePlanChargeId + "' value='" + citem.quantity + "' />";

								html+="  <button type='submit' class='btn btn-primary floatRight btn_update' id='update_item_"+citem.productRatePlanChargeId+"' rpId='"+ ratePlan.id +"' rpName='"+ratePlan.ratePlanName+"' pName='"+ratePlan.productName+"' citemName='"+citem.name+"' citemId='"+citem.productRatePlanChargeId+"' cId='"+citem.id+"' subId='"+obj.subscriptions[0].id+"' uom='"+uom+"' amendVerb='update' data-rateplanid='"+ratePlan.id+"' style='margin-top:4px;'>Update</button>";

						}//end if
					}//end for

					html+="</div>";	
            	html+="</div>";

	            }//end if 
			}//end for

			
				//Add chosen plans html
			 $(".chosen_plans").html(html);


			$(".btn_undo").click(function(event){
				undo(event);
				var id = event.currentTarget.id;
				console.log('btn_undo id is: ' + id);
				var removeId = id.split("undo_")[1];
				console.log('removeId is: ' + removeId);
				$('#' + id).parents('.panel').children('.panel-heading').css('background-color', '#fcf8e3').css('color', '#8a6d3b').css('border-color', '#faebcc');
				// var value = $('#' + id).parents('.panel').find(':input').val()
				// console.log("Input Box Quantity is: " + value);
				$('#remove_item_' + removeId).show();
			});

			$(".btn_undo_charge").click(function(event){
				//undo(event);

				var id = event.currentTarget.id.replace("undo_charge_btn_","");
				console.log('product rateplan charge id is: ' + id);
				$('#update_field_' + id).val($('#original_value_' + id).val());	//JQuery crossobject reference and assignment.
				
				// show update button and hide undo button
				$('#update_item_' + id).show();
				$('#undo_charge_btn_' + id).hide();

				var rpId = $(this).attr("rpId");
				undoUpdate(rpId);
			});

			$(".btn_save").click(function(event){
				saveAmend(event);
			});

			$(".btn_remove").click(function(event){
				multiAmend(event);
				var id = $(this).attr("data-rateplanid");
				var colorId = event.currentTarget.id;
				console.log("id is: " + id);
				$("#remove_item_" + id).hide();
				$('#update_item_' + id).parents('.panel').find(':button').hide();
				$('#remove_item_' + id).parents('.panel').children('.panel-heading').css('background-color', '#f2dede').css('color', '#a94442').css('border-color', '#ebccd1');
				$('#undo_' + id).show();
			});

			$(".btn_update").click(function(event){
				var id = event.currentTarget.id.replace("update_item_","");
				$('#' + id).parents('.panel').children('.panel-heading').css('background-color', '#d9edf7').css('color', '#31708f').css('border-color', '#bce8f1');

				// show undo button and hide update button
				$('#undo_charge_btn_' + id).show();
				//$('#update_item_' + id).hide();

				multiAmend(event);
				//alert("Subscription Updated");
				// var rpId = $(this).attr("rpId");
				// $('#undo_' + rpId).show();

				subscriptionUpdateMessage();
			});
			
		}
		

 /*
     * Show bill to contact update success message
     */
    function subscriptionUpdateMessage(){

        $.blockUI({
                message: $('div.subscription_update_message'),
                fadeIn: 700,
                fadeOut: 700,
                timeout: 2000,
                showOverlay: false,
                centerY: false,
                css: {
                    width: '300px',
                    height: "35px",
                    bottom: '20px',
                    top: '-100',
                    left: '10px',
                    right: '',
                    border: 'non',
                    radius: '5px',
                    padding: '5px',
                    backgroundColor: '#000',
                    '-webkit-border-radius': '10px',
                    '-moz-border-radius': '10px',
                    opacity: .6,
                    color: '#fff',
                }
            });


    }




		var undoUpdate = function(rpId){
			$.getJSON("backend/index.php?type=Undo", {undoId:rpId},
				function(data){	
					if(!data.success) {
						alert("Error is: " + data.msg[0].msg);
					}
					else{
						// getCurrentSubscription();
						showPreview();
					}
				}
			);
		}

		var cancel = function (event){
			
			
			var str = document.getElementById(event.id);
			$str = str.replace("Microsoft", "W3Schools");
		
			$.getJSON("backend/index.php?type=Undo", {id:rpId},
				function(data){	
					if(!data.success) {
						alert("Error is: " + data.msg[0].msg);
					}
					else{
						// getCurrentSubscription();
						showPreview();
					}
				}
			);

		}

		var undo = function(event){
			var buttonId = event.currentTarget.id;
			var rpId = buttonId.split("undo_")[1];
			console.log("undo Id is: " + rpId);

			$.getJSON("backend/index.php?type=Undo", {undoId:rpId},
				function(data){	
					if(!data.success) {
						alert("Error is: " + data.msg[0].msg);
					}
					else{
						// getCurrentSubscription();
						showPreview();
						console.log('buttonId is: ' + buttonId);
						$('#' + buttonId).hide();
					}
				}
			);
			// getCurrentSubscription();
			// showProductsAdded();
			showPreview();	

		};

		function getAccountSummary(){

	        $.getJSON("backend/index.php?type=GetAccountSummary",
	            function(data){
	                if(data.success){
	                    var acc = JSON.parse(data.msg[0]);
	                    var accountName = acc.basicInfo.name;

	                    window.account = accountName;

	                    //Show Account Info
	                    $('.account_name').html(accountName);

			var name = acc.billToContact.firstName+" "+acc.billToContact.lastName;

			//$("#user_email").html(name);

	                    var htmlGreet = "";
	                    htmlGreet = "<a style=\"float:right; color:#FE8402;\"> Welcome " + name + "!";
	                    $('.accountName').html(htmlGreet);
	                    $('.accountName').show();

	                    updateHeader(name);
	                }
	            }
	        );
	    }
		
		function updateHeader(accountName){

	        var welcome = ""; //" Welcome ";

	        var welcomeMessage = welcome.concat(accountName);
	        var arrowDown = "  <b class=\"caret\"></b>";
	        document.getElementById('accountHeaderName').innerHTML = welcomeMessage.concat(arrowDown);
	    }

		var showProductsAdded = function(){
			var addHtml = "";
			$.getJSON("backend/index.php?type=GetInitialCart",
				function(data){
					if(!data.success){	
						alert("Error in getInitialCart!"); 
					}
					else{
						var msgData = data.msg[0];
						console.log("showProductsAdded data is: " + data.msg[0]);
						console.log("Amendment Objects are: " + data.msg[0].amendments);
						var amendData = data.msg[0].amendments;

						var count = 0;
						for(var i in amendData){
							count ++;
							var amendObject = amendData[i];
							if(amendObject.amendVerb == 'add'){
								console.log("addData is: " + amendData[i]);
								addHtml+="<div class='panel panel-success shadow'>";
	            					addHtml+="<div class='panel-heading'>";
	            						addHtml+="<h3 class='panel-title'>"+amendObject.productName+" : "+amendObject.ratePlanName+"</h3>";

	            							addHtml+="<button type='submit' class='btn-info btn_undoAdd btn-sm floatRight floatUp' id='undo_"+amendObject.ratePlanId+"' rpId='"+amendObject.ratePlanId+"' data-rateplanid='"+amendObject.ratePlanId+"'>Undo</button>";

	            					addHtml+="</div>";
	            						addHtml+="<div class='panel-body'>";

	            						for(var j in amendObject.cartCharges){
	            							var addCharge = amendObject.cartCharges[j];

	            							console.log(addCharge);
	            							addHtml+="<span class='floatLeft'>"+addCharge.chargeName+' ('+addCharge.uom+') qty'+": </span>";
											addHtml+="</br>";
											addHtml+="<input type='text' class='form-control inputSize' disabled='true' value='" +addCharge.quantity+ "' />";
	            						}	//end for
	            						addHtml+="</div>";
	            				addHtml+="</div>";
	            			}	//end if
	            			//TODO: add support for changing css based on remove/update amendments
						}	//end for

						// if (count > 0) {
							$(".btn_save").removeAttr("disabled");
						// } else {
							// $(".btn_save").attr("disabled", "disabled");
						// }

						$(".added_plans").html(addHtml);
						$(".btn_undoAdd").click(function(event){	
							console.log("btn_undoAdd clicked!");
							var id = event.currentTarget.id;
							console.log("btn_undoAdd id: " + id);
							undo(event);
							$('#' + id).parents('.panel').remove();
						});   	
					}	//end else
	        	}	//end function
			);
		};

		var showPreview = function(){

			console.log("showPreview was called!");
			// $.blockUI();
			var previewHtml = "";
			$.getJSON("backend/index.php?type=AmendPreview", 
				function(data){
					if(!data.success){
						alert("Error is: " + data.msg[0].msg);
					}
					else{
						// $.unblockUI();
						console.log("AmendPreview callback is: " + JSON.stringify(data));
						// alert(JSON.stringify(data.msg));
						if(data.msg == 0){
						// if(typeof data.msg === undefined){
							console.log("data.msg.payload is undefined!");
							$(".preview_amount").hide();	//prevents [Amount] "undefined" when undo amendments cause a "0" value. 
						}
						else{
							var preview = jQuery.parseJSON(data.msg.payload);
							if (preview.amountWithoutTax != null && preview.taxAmount != null && preview.amount != null) {
								previewHtml += "<div class='panel panel-default shadow'>";
								previewHtml += "<div class='panel-body'>";
								previewHtml += "<table class='table'>";
								previewHtml += "	<tr>";
								previewHtml += "		<td>Subtotal:</td>";
								previewHtml += "		<td>$"+preview.amountWithoutTax.toFixed(2)+"</td>";
								previewHtml += "	</tr>";
								previewHtml += "	<tr>";
								previewHtml += "		<td>Tax:</td>";
								previewHtml += "		<td>$"+preview.taxAmount.toFixed(2)+"</td>";
								previewHtml += "	</tr>";
								previewHtml += "	<tr>";
								previewHtml += "		<td>Total:</td>";
								previewHtml += "		<td>$"+preview.amount.toFixed(2)+"</td>";
								previewHtml += "	</tr>";
								previewHtml += "</table>";
								previewHtml += "</div>";
								previewHtml += "</div>";
								
								$(".preview_amount").html(previewHtml);
								$(".preview_amount").show();
							} else {
								$(".preview_amount").hide();
							}
	var returnurl  = sessionStorage.getItem('returnurl'); 

	var uuid  = sessionStorage.getItem('qumulateGroupUUID'); 
	returnurl = returnurl + "?qumulateGroupUUID="+uuid; 
	location.href = returnurl;
		
						}


					}	
				}	//end function
			);
		}

		/**
		 * Cancels entire subscription completely.
		 * @param  event: {JavaScript class} subscription id to be cancelled.
		 * @return Redirects to account.php page if successful, errors if not successful.
		 */
		var cancelSubscription = function(event){
			console.log('Entering cancelSubscription');
			var buttonId = event;
			var cancelId = buttonId.split('cancel_subscription_')[1];
			var con = window.confirm("Are you sure you want to cancel this subscription?");	//javascript cancellation window.

			if(con == true){
				$.getJSON("backend/index.php?type=CancelSubscription", {cancelId:cancelId},
					function(data){
						if(!data.success){
							alert("Error is: " + data.msg[0].msg);
						}
						else{
							window.open("./account.php", "_self");	//opens a new window once data comes back from REST Service.
						}
					}
				);		
			}
		}

		var saveAmend = function(event){
			$.getJSON("backend/index.php?type=AmendPreview", 
				function(data){
					if (data == null || data.msg == null || data.msg.payload == null) {
						return null;
					}

					var preview = jQuery.parseJSON(data.msg.payload);
					var confirmText = "Are you sure you want to save this subscription?<br/><br/>";
					confirmText += "<table class='table'>";
					confirmText += "	<tr>";
					confirmText += "		<td>Subtotal:</td>";
					confirmText += "		<td>$"+preview.amountWithoutTax.toFixed(2)+"</td>";
					confirmText += "	</tr>";
					confirmText += "	<tr>";
					confirmText += "		<td>Tax:</td>";
					confirmText += "		<td>$"+preview.taxAmount.toFixed(2)+"</td>";
					confirmText += "	</tr>";
					confirmText += "	<tr>";
					confirmText += "		<td>Total:</td>";
					confirmText += "		<td>$"+preview.amount.toFixed(2)+"</td>";
					confirmText += "	</tr>";
					confirmText += "</table>";

					bootbox.confirm(confirmText, function(con){
						if(con == true){
							$.blockUI({	
								message: $('#loadingtest'),
								css: { 
					                top:  ($(window).height() - 400) /2 + 'px', 
					                left: ($(window).width() - 400) /2 + 'px', 
					                width: '200px' 
				            	}
				        	});

							$.getJSON("backend/index.php?type=AmendExecute",
								function(data){	
									$.unblockUI();
									if(!data.success) {
										$.getJSON("backend/index.php?type=EmptyCart");
										alert("Error is: " + data.msg[0].msg);
									}
									else {
										//TODO: check success of callback msg
										$.getJSON("backend/index.php?type=EmptyCart");
										location.reload();
									}
								}
							);
						}
					});
				}	
			);
		};

		/**
		 * Adds selected rateplan, allows for preview of addition before submitting a subscription amendment for
		 * the rateplan addition.
		 * @param event: {JavaScript class} rateplanid and quantities to be added.
		 * @return Selected rateplan appears on subscription list if successful, errors if not successful.
		 */
		 /**
		 * Updates selected rateplan, allows for preview of update before submitting a subscription amendment  * for the rateplan update.
		 * @param  event: {JavaScript class} rateplanid, rateplanchargeid, uom and (old/new) quantities to be 
		 * added.
		 * @return Selected rateplan updates on subscription list if successful, errors if not successful.
		 */
		var multiAmend = function(event){
			var buttonId = event.target.id;
				
			var amendVerb = event.target.getAttribute('amendVerb');	

			if(amendVerb == 'add'){	//Don't add duplicate ratePlans.
				var itemId = buttonId.split('add_item_')[1];
				console.log('amendVerb is: ' + amendVerb);
				var itemQty = null;
				var field = $('#qty_'+itemId);
				if(field.length){	
					itemQty = field.val();
				}
				var rpName = event.target.getAttribute('rpName');	
				var pName = event.target.getAttribute('pName');
				var chargeQuantityArray = new Array();	//Array to hold Charge Quantities
			
				$('.qty_' + itemId).each(function() {
					var chargeId = $(this).attr('cid');
					var rpQty = $(this).val();
					var chargeName = $(this).attr('cName');
					var unitOfMeasure = $(this).attr('uom');
					chargeQuantityArray.push({cId:chargeId, qty:rpQty, cName:chargeName, uom:unitOfMeasure});
				});
				console.log('chargeQuantityArray is: ' + chargeQuantityArray);
				//Perform the actual Add Product Amendment
				$.getJSON("backend/index.php?type=AmendLoad", {ratePlanId:itemId, quantity:JSON.stringify(chargeQuantityArray), ratePlanName:rpName, productName:pName, amendVerb:amendVerb},
					function(data){	
						if(!data.success) {
							alert("Error is: " + data.msg[0].msg);
						}
						else {
							showProductsAdded();
							showPreview();
						}
					}
				);
			} else if(amendVerb == 'update'){
				var itemId = event.target.getAttribute('rpId');
				console.log('amendVerb is: ' + amendVerb);
				var rpName = event.target.getAttribute('rpName');	
				var pName = event.target.getAttribute('pName');
				var cId = event.target.getAttribute('cId');
				var chargeId = event.target.getAttribute('citemId');
				var chargeName = event.target.getAttribute('citemName');
				var itemQty = $('#update_field_' + chargeId).val();
				var chargeQuantityArray = new Array();	//Array to hold Charge Quantities
			
				chargeQuantityArray.push({cId:cId, qty:itemQty, cName:chargeName});
				
				console.log('chargeQuantityArray is: ' + chargeQuantityArray);
				
				//Perform the actual Update Product Amendment
				$.getJSON("backend/index.php?type=AmendLoad", {ratePlanId:itemId, quantity:JSON.stringify(chargeQuantityArray), ratePlanName:rpName, productName:pName, amendVerb:amendVerb},
					function(data){	
						if(!data.success) {
							alert("Error is: " + data.msg[0].msg);
						}
						else {
							showPreview();
						}
					}
				);
			} else if(amendVerb == 'remove'){
				var itemId = buttonId.split('remove_item_')[1];
				var rpId = event.target.getAttribute('rpId');
				var rpName = event.target.getAttribute('rpName');
				var pName = event.target.getAttribute('pName');
				console.log('amendVerb is: ' + amendVerb);
				//Perform the actual Remove Product Amendment
				$.getJSON("backend/index.php?type=AmendLoad", {ratePlanId:rpId, ratePlanName:rpName, productName:pName, amendVerb:amendVerb}, 
					function(data){
						if(!data.success){
							alert("Error is: " + data.msg[0].msg);
						}
						else{
							showPreview();
						}
					}
				);
			} else{
				alert('amendVerb not recognized');
			}
		};

		/**
		 * Renders current product Catalog based on sandbox account, makes a call to render the rateplans.
		 * @param  data: {JSON encoded product catalog}
		 * @return renders the product catalog.
		 */
		var listProducts = function(data){
			$("#loading").fadeOut('fast');
			
			var obj = data;
			console.log(obj);
			console.log("allowDuplicateRatePlans is: " + obj.allowDuplicateRatePlans);
		
			var html = "";
			var products = obj;
			// console.log("products is: " + products);
			html += "<div class='panel-group'>";

			for(var prodKey in products){	
				// console.log(prodKey);
				var prod = products[prodKey];
				
				if(prod.name != undefined){
					html += "<div class=''>"; //this div had a class name 'panel panel-success shadow', removed based on the project bookwork
					html += "	<div class=''>";
					//html += "		<h3 class='panel-title'>"+prod.name+"</h3>";
					html += "	</div>";
					html += "	<div class='panel-body'>"
					html += "    	<div class='product'>";

					html += "			<div class='col-md-8' style='margin-left:10% !important;'>";
					// render product rateplans
					html += "     			<div class='panel-group plan_list'>";	//a list of panels.
					
					for(var rpKey in prod.productRatePlans){
						var rp = prod.productRatePlans[rpKey];
						html += renderRatePlans(prod, rp);
					}
					html += "     			</div>";
					html += "			</div>";
					html += "  		</div>";
					html += "  	</div>";
					html += "</div>";
				}
			}
			html += "</div>";

			$(".list_products").append(html);	

			$(".btn_add").click(function(event){
				if(products.allowDuplicateRatePlans){
					multiAmend(event);
				} else {
					multiAmend(event);	//Calls once and then hides add button if allowDuplicateRatePlans is set to false.
					$(this).hide();
				}
			});
		};

		/**
		 * renders the RatePlans, makes a call to render the rateplan charges.
		 * @param  prod: {JSON encoded object} Array of products.
		 * @param  rp: {JSON encoded object} Array of rateplans.
		 * @return renders the rateplans based in the product catalog.
		 */
		var renderRatePlans = function(prod, rp){
		
			var html = "";
			var numCharges = rp.productRatePlanCharges.length;	//length is an attribute of productRatePlanCharges Array object (Provided by JavaScript).
			// console.log("numCharges is: " + numCharges);

			var oneTimeCharges = 0;
			var recurringCharges = 0;
			var usageCharges = 0;

			html += "<div class='panel panel-default shadow panel-success'>";
			html += "	<div class='panel-heading clearfix' >";

			html += "		<h4 class='panel-title' >";
			html += 				rp.name+": <br/><br/><small>"+rp.description+"</small>";
			html += "		</h4>";
			html +=		"</div>";
			html +=		"<div id='panelBody_"+rp.id+"' class='panel-collapse collapse in'>";
			html += "		<div class='panel-body'>";

			tempHtml = "";				
			tempHtml += "		<div id='oneTimeTable_"+rp.id+"' class='table-responsive'>";
			tempHtml += "			<table class='table'>";
			tempHtml += "				<thead>";
			tempHtml += "					<tr>";
			tempHtml += "						<th>One Time Charges</th>";
			tempHtml += "					</tr>";
			tempHtml += "				</thead>";
			tempHtml += "				<tbody>";

			for(var chargeKey in rp.productRatePlanCharges) {
				var charge = rp.productRatePlanCharges[chargeKey];
				// console.log('charge: '+JSON.stringify(charge));

				if (charge.type == 'OneTime') {
					oneTimeCharges++;
					tempHtml += "			<tr>";
					tempHtml += "				<td>"+charge.name+"</td>";
					tempHtml += "				<td>"+renderPricingSummary(charge)+"</td>";
					tempHtml += "			</tr>";
				}
			}

			tempHtml += "				</tbody>";
			tempHtml += "			</table>";
			tempHtml += "		</div>";

			if (oneTimeCharges > 0) {
				html += tempHtml;
			}

			tempHtml = "";
			tempHtml += "<div class='chosen_plans' style='margin-top: 5px; color:#333;'></div>";
			
			tempHtml += "		<div id='recurringTable_"+rp.id+"' class='table-responsive'>";
			tempHtml += "			<table class='table'>";
			tempHtml += "				<thead>";
			tempHtml += "					<tr>";
			tempHtml += "						<th>Recurring Charges</th>";
			tempHtml += "					</tr>";
			tempHtml += "				</thead>";
			tempHtml += "				<tbody>";

			for(var chargeKey in rp.productRatePlanCharges){
				var charge = rp.productRatePlanCharges[chargeKey];

				if (charge.type == 'Recurring') {
					recurringCharges++;
					tempHtml += "			<tr>";
					tempHtml += "				<td>"+charge.name+"</td>";
					tempHtml += "				<td>"+renderPricingSummary(charge)+"</td>";
					tempHtml += "			</tr>";
				}
			}

			
			
			tempHtml += "				</tbody>";
			tempHtml += "			</table>";
			tempHtml += "		</div>";

			if (recurringCharges > 0) {
				html += tempHtml;
			}

			tempHtml = "";
			tempHtml += "		<div id='usageTable_"+rp.id+"' class='table-responsive'>";
			tempHtml += "			<table class='table'>";
			tempHtml += "				<thead>";
			tempHtml += "					<tr>";
			tempHtml += "						<th>Usage Charges</th>";
			tempHtml += "					</tr>";
			tempHtml += "				</thead>";
			tempHtml += "				<tbody>";

			for(var chargeKey in rp.productRatePlanCharges){
				var charge = rp.productRatePlanCharges[chargeKey];
				// console.log('charge: '+JSON.stringify(charge));

				if (charge.type == 'Usage') {
					usageCharges++;
					tempHtml += "			<tr>";
					tempHtml += "				<td>"+charge.name+"</td>";
					tempHtml += "				<td>"+renderPricingSummary(charge)+"</td>";
					tempHtml += "			</tr>";
				}
			}

			tempHtml += "				</tbody>";
			tempHtml += "			</table>";
			tempHtml += "		</div>";

			if (usageCharges > 0) {
				html += tempHtml;
			}


			//TODO: fix the name of this
			//TODO: add comments, i.e. renders Quantity boxes for each charge
			for(var chargeKey in rp.productRatePlanCharges){
				var charge = rp.productRatePlanCharges[chargeKey];
				//html += renderProductRatePlanCharge(rp,charge);
			}
			html += "		</div>";
			html += "	</div>";	
			html += "</div>";
			html += "<br/>";	

			return html;
		};


		function renderPricingSummary(charge) {
			var html = "";
			var pricingSummary = charge.pricingSummary[0];

			//Assume USD Currency
			pricingSummary = pricingSummary.replace(/USD/g, "$");

			switch(charge.model) {
				case "PerUnit":
					html += pricingSummary;
					break;
				case "FlatFee":
					html += pricingSummary;
					break;
				case "Overage":
					html += pricingSummary;
					break;
				case "Tiered":
					pricingSummary = pricingSummary.replace(/;/g, "<br/>");
					html += pricingSummary;
					break;
				case "TieredWithOverage":
					pricingSummary = pricingSummary.replace(/;/g, "<br/>");
					pricingSummary = pricingSummary.replace(", thereafter", "<br/>");
					html += pricingSummary;
					break;
				case "Volume":
					var priceintiers = charge.pricing[0].tiers;
					for(var i=0;i<priceintiers.length; i++){
						html += "<p style='text-align:left !important'>From "+priceintiers[i].startingUnit+" to "+priceintiers[i].endingUnit+" - "+"$"+priceintiers[i].price+"/"+charge.uom+"</p>";
					}
					//pricingSummary = pricingSummary.replace(/;/g, "<br/>");
					//html += pricingSummary;
					break;
				case "DiscountPercentage":
					html += pricingSummary;
					break;
				case "DiscountFixedAmount":
					html += pricingSummary;
					break;
			}

			return html;
		}
		/**
		 * renders the RatePlanCharges
		 * @param  charge: {JSON encoded object} Array of charges.
		 * @param  rp: {JSON encoded object} Array of rateplans.
		 * @return renders the rateplancharges based in the product catalog.
		 */
		var renderProductRatePlanCharge = function(rp, charge){
			var html = "";

			if(charge.type != 'Usage' && (charge.model == ('PerUnit' || 'Tiered' || 'Volume'))) {		
				var pricingSummary = charge.pricingSummary[0];	//always only one element in pricingSummary array.
				var uom = pricingSummary.split('/')[1];	//Splits String array based on '/' character and returns the [1] element in the array.
				html += "		<br> "+ charge.name +"("+ pricingSummary +"): <br><input type='text'  class='w80 qty_"+rp.id+"' cid='"+charge.id+"' cName='"+charge.name+"' uom='"+uom+"' value='1' onkeypress='return isNumberKey(event)' /> ("+uom+") ";
			}

			return html;
		};
	</script>
</head>
<body>
	<div id="header"></div>

	<div id="bigContainer" style="background: images/noise.png; width = 100%">
	
		<div class="row">
			<div class="loading">
				<img id="loadingtest" src="loading.gif" />
			</div>
			<div class="col-md-1"></div>
			<div class="col-md-6 zuora_white" style="margin-left: 10%">
		    	<div id="main_box" class="container">
	      		<div class="list_products">
			        	<div class="" style="margin-right: 10%">
		          			<h3 class="text-center">Manage Subscription</h3>
			        	</div>

		      	</div>

		    	</div>
			</div>
			<div class="col-md-3">
		    	<div id="right-sub">
		        	<!----<div style="padding-bottom:5%">
						<h3 class="text-center">Current Subscription</h3>------>
						<!---<span id="user_email"></span>---->
		        	<!----</div>--->

		        	<!-------<ul class="chosen_plans" >
		        	</ul>----->


				<div class="subscription_update_message" style="display:none">
                            <p><img src="images/check48.png" alt="Smiley face" height="15" width="15">  Subcription Updated successfully!</p>
                </div>

		        	

		      	<div class="right-sub inner upcoming-changes">
		      	</div>

		      	<div class="right-sub inner accept-change">
			        	<h3>Accept Change</h3>
			        	<span class='amendment-detail'>Please wait.</span><br/><br/>
			        	<button type="submit" class="btn btn-primary floatLeft accept-amendment">Accept</button>
			        	<button type="submit" class="btn btn-primary floatRight cancel-amendment">Cancel</button>
			    	</div>
			  	</div>
			</div>
			
			<div class="col-md-2"></div>
		</div>
	</div>

	<div id="footer"></div>
</body>
</html>
