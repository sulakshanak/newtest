<?php

error_reporting(E_ERROR | E_PARSE);

?>
<!DOCTYPE html>
<html>
    <meta charset="utf-8" />
    <!-- Set the viewport width to device width for mobile -->
    <meta name="viewport" content="width=device-width" />
    <title>Select Products</title>    

    <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="css/bootstrap-2.3.2.css" rel="stylesheet" type="text/css" />
    <link href="css/primefaces-api.css" rel="stylesheet" type="text/css" />
    <link href="css/nprogress.css" rel="stylesheet" type="text/css" />
    <link href="css/base.css" rel="stylesheet" type="text/css" />
    <!-- Javascript Files -->
    <script type="text/javascript" src="js/jquery.js" ></script>
    <script type="text/javascript" src="js/function.js" ></script>
    <script type="text/javascript" src="js/bootstrap.min.js" ></script>
    <script src="js/bootbox.js"></script>
    <body>
        <!-- start Header -->
        <div id="header">
<!---------to store returnurl----------->
		<?php $domainurl=explode("/",$_POST['returnurl']);  ?>

		<input type="hidden" id="returnurl" name="returnurl" value="<?php echo $_POST['returnurl']; ?>"><br>
		<input type="hidden" id="domainurl" name="domainurl" value="https://<?php echo $domainurl[2]; ?>" ><br>
		<input type="hidden" id="oktaid" name="oktaid" value="<?php echo $_POST['oktaid']; ?>"><br>
		<input type="hidden" id="qumulateGroupUUID" name="qumulateGroupUUID" value="<?php echo $_POST['qumulateGroupUUID']; ?>"><br>
	</div>
        <!-- end header -->
        <div id="bigContainer">
            <!-- Begin Body -->
            <div class="container">
                <div class="row">
                    <div class="col-md-12" >
                        <div id="right-sub" class="Fixed">
                          <!-- new page header -->
                          <div class="text-center" style="overflow:hidden;">
                              <h2 class="nwSbHdng" style="margin-top: 9px;text-align:center !important;font-size:20px;">Order Form</h2>
                          </div>
                          <!-- /new page header -->
                            <div class="page-header text-center" style="overflow:hidden;">
                                <h3 class="nwSbHdng pull-left" style="margin-top: 9px">Chosen Products</h3>
                                <button type="submit" class="btn btn-primary btn-md pull-right" onclick="emptyCart()">Clear Cart</button>
                            </div>
                            <div class="chosen_plans"></div>
                            <div class="preview_amount"></div>
                        </div>
                    </div>
                    <div class="col-md-12 zuora_white roof_height shadow">
                        <div id="main_box">
                            <div class="list_products">
                                <div class="page-header text-center">
                                    <h3 class="nwSbHdng">Select Your Base Product</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
			<!-- go_to_product_selector(); -->
                        <!-- <input type="button" class="pull-left previousButton imageFade nxtprv" value="previous" onclick="window.location='index.html';" style="border-radius: 4px;border: 1px solid transparent;">-->
	        	<input type="image" src="./images/arrow_greenleft.png" style="width:35px;height:35px;" onclick="window.location='index.html';" />
			<!--<input type="button" class="pull-right nextButton imageFade nxtprv" value="next" disabled id="nextButton"  onclick="window.location='subscribe.html';" style="border-radius: 4px;border: 1px solid transparent;"> -->
	                <input type="image" class="pull-right nextButton imageFade"  src="./images/arrow_green.png" style="width:35px;height:35px;" onclick="window.location='subscribe.html';" value="next" disabled id="nextButton" />
		   </div>
                </div>
            </div>
            <script type="text/javascript" >
                $(document).ready(function(){
                    $("#infor").hide();
                    $("#loading").show();
                    getInitialCart();
	
		// add to session storage	

			var query = window.location.search.substring(1);
			var vars = query.split("&");
			var oktaid;
			var qumulateuuid;

			for(var i=0;i<vars.length;i++)
			{
			var pair = vars[i].split("=");
			pair[1] = decodeURIComponent(pair[1]);
				if(pair[0] == "oktaid")
				{
					oktaid = pair[1];
					
				}


			}
			var SFDCAccountId = sessionStorage.getItem('sfdcAccountId');
			var qumid = sessionStorage.getItem('qumulateuuid');

			qumulateuuid = "<?php echo $_POST['qumulateGroupUUID']; ?>";
			oktaid = "<?php echo $_POST['oktaid']; ?>";

			var returnurl  = sessionStorage.getItem('returnurl');	//sessionStorage.setItem('returnurl', document.getElementById('returnurl').value);
			var domainurl  = sessionStorage.getItem('domainurl');	//sessionStorage.setItem('domainurl', document.getElementById('domainurl').value);


			if(SFDCAccountId == null || SFDCAccountId == "" )
			{
				if(oktaid == "" || oktaid == undefined )
				{

					oktaid = "<?php echo $_POST['oktaid']; ?>";

					sessionStorage.setItem('sfdcAccountId', oktaid);
                        		if(sessionStorage.getItem('sfdcAccountId')){
						
                        		}
					else{
						var logout = "<?php include 'backend/config.php'; echo $logout;  ?>"
						window.location.replace(logout);
					}

				}
				else{
					oktaid = "<?php echo $_POST['oktaid']; ?>";

					sessionStorage.setItem('sfdcAccountId', oktaid);

				}
			}

			if(SFDCAccountId != "" && SFDCAccountId != oktaid )
			{

				if(oktaid == ""){

				}else{
					oktaid = "<?php echo $_POST['oktaid']; ?>";

					sessionStorage.setItem('sfdcAccountId', oktaid);
				}
			}

			if(qumid == null || qumid == "" )
			{
				if(qumulateuuid == "" || qumulateuuid == undefined )
				{
					qumulateuuid = "<?php echo $_POST['qumulateGroupUUID']; ?>";
					sessionStorage.setItem('qumulateuuid', qumulateuuid);
					sessionStorage.setItem('qumulateGroupUUID', qumulateuuid);

                        			if(sessionStorage.getItem('qumulateuuid')){
                        		}
				}
				else{
					qumulateuuid = "<?php echo $_POST['qumulateGroupUUID']; ?>";
					sessionStorage.setItem('qumulateuuid', qumulateuuid);
					sessionStorage.setItem('qumulateGroupUUID', qumulateuuid);
				}
			}

			if(qumid != "" && qumid != qumulateuuid )
			{

				if(qumulateuuid == ""){

				}else{
					qumulateuuid = "<?php echo $_POST['qumulateGroupUUID']; ?>";
					sessionStorage.setItem('qumulateuuid', qumulateuuid);
					sessionStorage.setItem('qumulateGroupUUID', qumulateuuid);
				}
			}
			
			if(returnurl == null || returnurl == "")
			{

				if(returnurl == "" || returnurl == undefined)
				{
					sessionStorage.setItem('returnurl', document.getElementById('returnurl').value);
                        			if(sessionStorage.getItem('returnurl')){
                        		}
				}				
			}

			if(domainurl == null || domainurl == "")
			{

				if(domainurl == "" || domainurl == undefined)
				{
					sessionStorage.setItem('domainurl', document.getElementById('domainurl').value);
                        			if(sessionStorage.getItem('domainurl')){
                        		}
				}				
			}

			
                    $.getJSON("backend/index.php?type=ReadCatalog", {page:1},
			function(data){
                            if(!data.success){
                                alert("Error reading catalog cache.");
                            }else{
                                listProducts(data.msg);
                            } 
                        }
                    );
                    $("#remove_all_button").click(function(){
			emptyCart();
                    });
                    $('#signInButton').click(function() {
                        window.location.replace("./login.html");
                    });
                    //Load header
                    $("#header").load("header.html", function() {
                        $("#subscribeTab").addClass('active');
                    });

			var SFDCAccountId = sessionStorage.getItem('sfdcAccountId');
			var qumulateGroupUUID  = sessionStorage.getItem('qumulateuuid');
			var sfdcAccountDetails1 = getOktaContactDetails(SFDCAccountId,qumulateGroupUUID );

                });
	
                /**
                 * Validates that the input is a number.
                 * @param  number 	evt 
                 * @return Boolean 	true if a number, false if not a number.
                 */
                function isNumberKey(evt){
                    var charCode = (evt.which) ? evt.which : event.keyCode
                    if (charCode > 31 && (charCode < 48 || charCode > 57))
			return event.preventDefault();
                }

		function getOktaContactDetails(sfdcAccountId,qumulateGroupUUID){
			
			var oktaContactDetails = new Array();
			var BilltoDetails = new Array();
			sfdcAccountId = sfdcAccountId ; 
			
			$.getJSON("backend/index.php?type=getOktaContactDetails", {SFDCAccountId: sfdcAccountId,qumulateGroupUUID: qumulateGroupUUID},
                function(data){
                    if(data.success){
			
			oktaContactDetails[0] = data.msg[0].MailingCity;
                        oktaContactDetails[1] = data.msg[0].MailingStreet;
                        oktaContactDetails[2] = data.msg[0].MailingCountry;
                        oktaContactDetails[3] = data.msg[0].MailingState;
                        oktaContactDetails[4] = data.msg[0].MailingPostalCode;
			oktaContactDetails[5] = data.msg[0].Phone;
			oktaContactDetails[6] = data.msg[0].Account.Ext_Cust_Id__c;
			oktaContactDetails[7] = data.msg[0].Account.Name;

                    }
                });
				soldToContactDetails = oktaContactDetails;
				return oktaContactDetails;

		}

                /**
                 * Removes all selected products from shopping cart.
                 * @param  
                 * @return Refreshes the Cart listing.
                 */
                var emptyCart = function(){
                    $.getJSON("backend/index.php?type=EmptyCart",
			function(data){
                            if(!data.success){
                                alert("Error in emptyCart!");
                            }else{
                                refreshCart(data);
                            }
                        }
                    );
                };
                /**
                 * Retrieves initial cart
                 * @param  
                 * @return Called from when the page initially loads.
                 */
                var getInitialCart = function(){
                    $.getJSON("backend/index.php?type=GetInitialCart",
			function(data){
                            if(!data.success){	
                                alert("Error in getInitialCart!"); 
                            }else{

                                refreshCart(data);
                            }
                        }
                    );
                };
                /**
                 * Renders shopping cart listing.
                 * @param  cart Array 	data 
                 * @return Displays shopping cart contents.
                 */
                var refreshCart = function(data){
                    var html = "";
                    //console.log("Data msg: " + data.msg[0]);
                    var count = 0;
                    for(var i in data.msg[0].cart_items){
                        var citem = data.msg[0].cart_items[i];
                        //console.log(citem);
                        html += "<div class=' table-responsive nwTblStl'>";
                        html += "<table class='table'>";
                        html += "<thead>";
                        html += "<tr>";
                        html += "<th>Name</th>";
                        html += "<th>Action</th>";
                        html += "</tr>";
                        html += "</thead>";

                        html += "<tbody>";
                        html += "<tr>";
                        html += "<td class='nm'>";

                        html+="<div class='panel-heading'>";
                        html+="<h3 class='panel-title'>"+citem.productName+" : "+citem.ratePlanName+"</h3>";
                        html+="</div>";
                        html += "</td> ";
                        html += "<td class='crt'>";
                        //controls display of ratePlanNames Currently null because cart items productName, ratePlanName is null. Need to find way to pass this information to backend to assign the correct names.
                        html+="<div class='panel-body'>";
                        for(var j in data.msg[0].cart_items[i].cartCharges){
                            var charge_item = data.msg[0].cart_items[i].cartCharges[j];
                            html+="    <span class='rateplan_name floatLeft'>"+charge_item.chargeName+' ('+charge_item.uom+') qty'+": </span><input type='text' class='form-control inputSize smlinpt' disabled='true' value='" +charge_item.quantity+ "' />";
                        }
                        html+="	<button type='submit' class='btn-danger btn_remove btn-sm floatRight rmvBtn' id='remove_item_"+citem.itemId+"' rpId="+citem.itemId+">Remove</button>";

                        html+="</div>";
			html+="</td>";
			html += "</tr>";
			html += "</table>";
                        html+="</div>";
                        count++;
                    }
                    $(".chosen_plans").html(html);
                    //console.log('count: '+count);
                    if (count == 0) {
			$('#nextButton').prop('disabled','true');
			$('#nextButton').addClass('imageFade');
			// $('#right-sub').hide();
                    } else {
                       // alert('in else loop');
			$('#nextButton').prop('disabled','');
			$('#nextButton').removeClass('imageFade');
                        $('.btn_add').prop('disabled','true');
			

			// $('#right-sub').show();
                    }
                    $(".btn_remove").click(function(event){
			//console.log('btn_remove clicked. Calling removeFromCart');
			removeFromCart(event);
                    });
                    showPreview();
                };
                /**
                 * Preview Subscription call to retrieve subtotal
                 */
                var showPreview = function(){
                    var previewHtml = "";
                    $.getJSON("backend/index.php?type=previewSubscription", 
			function(data){
                            if (data != null && data.msg != null && data.success) {
                                var subPreview = JSON.parse(data.msg[0]);
                                if (subPreview.amountWithoutTax != undefined) {
                                    previewHtml += "<div class='panel panel-default shadow'>";
                                    previewHtml += "	<div class='panel-body'>";
                                    previewHtml +="			<b>Subtotal:</b> $"+subPreview.amountWithoutTax.toFixed(2)+"";
                                    previewHtml += "	</div>";
                                    previewHtml += "</div>";

                                    //Update preview section with new amount
                                    $(".preview_amount").html(previewHtml);
                                    $(".preview_amount").show();
                                } else {	
                                    //Previewed amount did not return. Hide preview section
                                    $(".preview_amount").hide();
                                }
                                $('.btn_add').prop('disabled','true');
                               // $('.btn_add').css('cssText', 'display: none !important'); 
				$('.list_products .panel-group .nwTblStl .table tr').find('th:last, td:last').css('cssText', 'display: none !important'); 

                            } else {
                                //Previewed amount did not return. Hide preview section
                               $('.btn_add').prop('disabled','');
				$('.list_products .panel-group .nwTblStl .table tr').find('th:last, td:last').css('cssText', 'display: table-cell !important,text-align:left !important'); 

                                $(".preview_amount").hide();
                            }
			}
                    );
                };

                /**
                 * Validates that the input is a number.
                 * @param  rateplanid 	event 
                 * @return refreshes cart based on action taken.
                 */
                var removeFromCart = function(event){
                    var buttonId = event.currentTarget.id;
                    var itemId = parseInt(buttonId.split('remove_item_')[1]);
                    var rpId = event.target.getAttribute('rpId');
                    //console.log('buttonId: '+buttonId+', itemId: '+itemId+', rpId: '+rpId);

                    $.getJSON("backend/index.php?type=RemoveItemFromCart", {itemId:itemId},
			function(data){
                            if(!data.success) {
                                alert("Error in removeFromCart: "+JSON.stringify(data));
                            }else {
                                refreshCart(data);
                            }
                        }
                    );
                };
                /**
                 * Validates that the input is a number.
                 * @param  rateplanid 	event 
                 * @return refreshes cart based on action taken.
                 */
                var addToCart = function(event){
                    //Rateplanid is given, now find all the charges under this id.
                    //Suggest to make another HTML class that uses the rateplanid which finds the individual quantity fields.
                    var rpId = event.currentTarget.getAttribute('id');	
                    //console.log('rpId: ' + rpId);
                    var rpName = event.currentTarget.getAttribute('rpName');	
                    //console.log('rpName: ' + rpName);
                    var pName = event.currentTarget.getAttribute('pName');
                    //console.log('pName: ' + pName);
                    var chargeQuantityArray = new Array();	//pushing charge Quantities into this array. To be used later.

                    var bootMessage = "Please specify desired quantities for the charges below:<br/><table class='table'>";
                    $('.qtyhidden_' + rpId).each(function() {	
                        //console.log('Entering Charge Loop');
                        var chargeId = $(this).attr('cid');
			//console.log('\tchargeId: '+chargeId);
                        var rpQty = $(this).val();
			if (rpQty < 1) {
                            rpQty = 1;
			}
                        var chargeName = $(this).attr('cName');
                        var unitOfMeasure = $(this).attr('uom');
	chargeQuantityArray.push({cId:chargeId, qty:rpQty, cName:chargeName, uom:unitOfMeasure});

			bootMessage += "<br/><tr><td><b>"+chargeName+":</b></td><td><input type='number' min='1' class='w80 qty_"+rpId+"' cid='"+chargeId+"' cName='"+chargeName+"' uom='"+unitOfMeasure+"' value='1' /> /("+unitOfMeasure+")</td></tr>";
		});
		bootMessage += "</table>";

		if (chargeQuantityArray.length > 0) {
			bootbox.dialog({
	          message: bootMessage,
	          title: "Confirm Quantities",
	          buttons: {
	            confirm: {
	              label: "Add to cart",
	              className: "btn-primary",
	              callback: function() {
	              	//console.log("Confirm pressed");
	              	//console.log('.qty_' + rpId);

	              	chargeQuantityArray = new Array();
	              	$('.qty_' + rpId).each(function() {	
						//console.log('Entering Charge Loop');

						var chargeId = $(this).attr('cid');
						//console.log('\tchargeId: '+chargeId);

						var rpQty = $(this).val();
						if (rpQty < 1) {
							rpQty = 1;
						}
						//console.log('\trpQty: '+rpQty);


						var chargeName = $(this).attr('cName');
						//console.log('\tchargeName: '+chargeName);

						var unitOfMeasure = $(this).attr('uom');
						//console.log('\tunitOfMeasure: '+unitOfMeasure);
						chargeQuantityArray.push({cId:chargeId, qty:rpQty, cName:chargeName, uom:unitOfMeasure});
					});

					//console.log(JSON.stringify({ratePlanId:rpId, quantity:chargeQuantityArray, ratePlanName:rpName, productName:pName}));
					$.getJSON("backend/index.php?type=AddItemToCart", {ratePlanId:rpId, quantity:JSON.stringify(chargeQuantityArray), ratePlanName:rpName, productName:pName},	//rpQty is now a list

						function(data){
							if(!data.success) {
								alert("Error in addToCart!");
							}
							else {
								refreshCart(data);
                                                                $('.btn_add').attr('disabled','disabled');
							}
			        	}
					);
	              }
	            },
	            cancel: {
	              label: "Cancel",
	              className: "btn-danger",
	              callback: function() {
	                //console.log("Cancel pressed");
	              }
	            }
	          }
	        });
		} else {

			//console.log('.qty_' + rpId);
			//console.log(JSON.stringify({ratePlanId:rpId, quantity:chargeQuantityArray, ratePlanName:rpName, productName:pName}));
			$.getJSON("backend/index.php?type=AddItemToCart", {ratePlanId:rpId, quantity:JSON.stringify(chargeQuantityArray), ratePlanName:rpName, productName:pName},	//rpQty is now a list

				function(data){
					if(!data.success) {
						alert("Error in addToCart!");
					}
					else {
						refreshCart(data);
					}
	        	}
			);
		}
	};
	/**
	 * Renders current product Catalog based on sandbox account, makes a call to render the rateplans.
	 * @param  data: {JSON encoded product catalog}
	 * @return renders the product catalog.
	 */
	var listProducts = function(data){
		$("#loading").fadeOut('fast');
		var obj = data;
		//console.log(obj);
		//console.log("allowDuplicateRatePlans is: " + obj.allowDuplicateRatePlans);
	
		var html = "";
			var products = obj;
			//console.log("products is: " + products);
			html += "<div class='panel-group'>";

			for(var prodKey in products){	
				//console.log(prodKey);
				var prod = products[prodKey];
				if(prod.name != undefined){
					html += "<div class=' table-responsive nwTblStl' style='margin-bottom: 30px'>";
					html += "<table class='table'>";
					html += "<thead>";
					html += "<tr>";
					html += "<th>Name</th>";
					//html += "<th>Image</th>";
					html += "<th>Description</th>";
					html += "<th>Action</th>";
					html += "</tr>";
					html += "</thead>";
					
					html += "<tbody>";
					html += "<tr>";
					html += "<td class='nm'> <div class='panel-heading zuora_green white_font'><h3 class='panel-title'>"+prod.name+"</h3></div> </td>";
					//html += "<td class='vol'> <div class='c1 product_description'><image src='images/"+prod.PictureLocation__c+"' /></div> </td>";
					html += "<td class='dscpton' style='width:55%;padding: 15px;'>";
					
					html += "<div class='panel-group plan_list' style='margin:0;'>";	//a list of panels.
					for(var rpKey in prod.productRatePlans){
						var rp = prod.productRatePlans[rpKey];
						html += renderRatePlans(prod, rp);
					}
					html += "</div>";
					
					
					html += "</td>";
					html += "<td class='crt'><button type='submit' class='mybtn btn btn-sm btn-primary btn_add' id='"+rp.id+"' rpName='"+rp.name+"' pName='"+prod.name+"'>Add</button></td>";
					html += "</tr>";
					
					html += "</tbody>";
					
					html += "</table>";
					html += "</div>";
				}
			}
			html += "</div>";

		$(".list_products").append(html);	//jQuery appends html to a Javascript class, this class gets called in the body.
		$(".btn_add").click(function(event){
			if(obj.allowDuplicateRatePlans){
				addToCart(event);
			}
			else{
				addToCart(event);	//need to call this function to check for Ids.
				$(this).hide();
			}
		});
	};
	/**
	 * renders the RatePlans, makes a call to render the rateplan charges.
	 * @param  prod: {JSON encoded object} Array of products.
	 * @param  rp: {JSON encoded object} Array of rateplans.
	 * @return renders the rateplans based in the product catalog.
	 */
	var renderRatePlans = function(prod, rp) {
		var html = "";
		var numCharges = rp.productRatePlanCharges.length;	//length is an attribute of productRatePlanCharges Array object (Provided by JavaScript).
		//console.log("numCharges is: " + numCharges);

		var oneTimeCharges = 0;
		var recurringCharges = 0;
		var usageCharges = 0;

		html += "<div class='panel panel-default shadow '  style='border: none'>";
		html += "	<div class='panel-heading clearfix' >";

		html += "		<h4 class='panel-title product_panel' data-toggle='collapse' data-target='#panelBody_"+rp.id+"'>";
		html += 				rp.name+": <br/><small>"+rp.description+"</small>";
		html += "		</h4>";
		//html +=				"<div class='pull-right'></div>";
		html +=		"</div>";
		html +=		"<div id='panelBody_"+rp.id+"' class='panel-collapse collapse in'>";
		html += "		<div class='panel-body'>";
		
		tempHtml = "";				
		tempHtml += "		<div id='oneTimeTable_"+rp.id+"' class='table-responsive'>";
		tempHtml += "			<table class='table' style='border: 1px solid #ccc'>";
		tempHtml += "				<thead>";
		tempHtml += "					<tr>";
		tempHtml += "						<th>One Time Charges</th>";
		tempHtml += "					</tr>";
		tempHtml += "				</thead>";
		tempHtml += "				<tbody>";

		for(var chargeKey in rp.productRatePlanCharges) {
			var charge = rp.productRatePlanCharges[chargeKey];
			//console.log('charge: '+JSON.stringify(charge));

			if (charge.type == 'OneTime') {
				oneTimeCharges++;
				tempHtml += "			<tr>";
				tempHtml += "				<td>"+charge.name+"</td>";
				tempHtml += "				<td>"+renderPricingSummary(charge)+"</td>";
				tempHtml += "			</tr>";
			}
		}

		tempHtml += "				</tbody>";
		tempHtml += "			</table>";
		tempHtml += "		</div>";

		if (oneTimeCharges > 0) {
			html += tempHtml;
		}

		tempHtml = "";
		tempHtml += "		<div id='recurringTable_"+rp.id+"' class='table-responsive'>";
		tempHtml += "			<table class='table' style='border: 1px solid #ccc'>";
		tempHtml += "				<thead>";
		tempHtml += "					<tr>";
		tempHtml += "						<th>Recurring Charges</th>";
		tempHtml += "					</tr>";
		tempHtml += "				</thead>";
		tempHtml += "				<tbody>";

		for(var chargeKey in rp.productRatePlanCharges){
			var charge = rp.productRatePlanCharges[chargeKey];
			//console.log('charge: '+JSON.stringify(charge));

			if (charge.type == 'Recurring') {
				recurringCharges++;
				tempHtml += "			<tr>";
				tempHtml += "				<td>"+charge.name+"</td>";
				tempHtml += "				<td style='text-align: left !important;'>"+renderPricingSummary(charge)+"</td>";
				tempHtml += "			</tr>";
			}
		}

		tempHtml += "				</tbody>";
		tempHtml += "			</table>";
		tempHtml += "		</div>";

		if (recurringCharges > 0) {
			html += tempHtml;
		}

		tempHtml = "";
		tempHtml += "		<div id='usageTable_"+rp.id+"' class='table-responsive'>";
		tempHtml += "			<table class='table'>";
		tempHtml += "				<thead>";
		tempHtml += "					<tr>";
		tempHtml += "						<th>Usage Charges</th>";
		tempHtml += "					</tr>";
		tempHtml += "				</thead>";
		tempHtml += "				<tbody>";

		for(var chargeKey in rp.productRatePlanCharges){
			var charge = rp.productRatePlanCharges[chargeKey];
			//console.log('charge: '+JSON.stringify(charge));

			if (charge.type == 'Usage') {
				usageCharges++;
				tempHtml += "			<tr>";
				tempHtml += "				<td>"+charge.name+"</td>";
				tempHtml += "				<td style='text-align: left !important;'>"+renderPricingSummary(charge)+"</td>";
				tempHtml += "			</tr>";
			}
		}

		tempHtml += "				</tbody>";
		tempHtml += "			</table>";
		tempHtml += "		</div>";

		if (usageCharges > 0) {
			html += tempHtml;
		}


		//TODO: fix the name of this
		//TODO: add comments, i.e. renders Quantity boxes for each charge
		for(var chargeKey in rp.productRatePlanCharges){
			var charge = rp.productRatePlanCharges[chargeKey];
			html += renderProductRatePlanCharge(rp,charge);
		}

		html += "		</div>";
		html += "	</div>";	
		html += "</div>";
		html += "<br/>";	
		
		return html;
	};

	function renderPricingSummary(charge) {
		var html = "";
		var pricingSummary = charge.pricingSummary[0];

		//Assume USD Currency
		pricingSummary = pricingSummary.replace(/USD/g, "$");

		switch(charge.model) {
			case "PerUnit":
				html += pricingSummary;
				break;
			case "FlatFee":
				html += pricingSummary;
				break;
			case "Overage":
				html += pricingSummary;
				break;
			case "Tiered":
				pricingSummary = pricingSummary.replace(/;/g, "<br/>");
				html += pricingSummary;
				break;
			case "TieredWithOverage":
				pricingSummary = pricingSummary.replace(/;/g, "<br/>");
				pricingSummary = pricingSummary.replace(", thereafter", "<br/>");
				html += pricingSummary;
				break;
			case "Volume":
					var priceintiers = charge.pricing[0].tiers;
					for(var i=0;i<priceintiers.length; i++){
						html += "<p style='text-align:left !important'>From "+priceintiers[i].startingUnit+" to "+priceintiers[i].endingUnit+" - "+"$"+priceintiers[i].price+"/"+charge.uom+"</p>";
					}
				pricingSummary = pricingSummary.replace(/;/g, "<br/>");
				//html += pricingSummary;
				break;
			case "DiscountPercentage":
				html += pricingSummary;
				break;
			case "DiscountFixedAmount":
				html += pricingSummary;
				break;
		}

		return html;
	}

	/**
	 * renders the RatePlanCharges
	 * @param  charge: {JSON encoded object} Array of charges.
	 * @param  rp: {JSON encoded object} Array of rateplans.
	 * @return renders the rateplancharges based in the product catalog.
	 */
	var renderProductRatePlanCharge = function(rp, charge){
		var html = "";

		if(charge.type != 'Usage' && ((charge.model == 'PerUnit') || (charge.model == 'Tiered' ) || (charge.model == 'Volume' ) ) ){
			var pricingSummary = charge.pricingSummary[0];	//always only one element in pricingSummary array.
			var uom = pricingSummary.split('/')[1];	//Splits String array based on '/' character and returns the [1] element in the array.
			console.log(rp.id + " " + charge.id + " " + charge.name + " " + uom);
			var actualuom = charge.uom;
			html += "<br><input type='number' style='display:none;' class='w80 qtyhidden_"+rp.id+"' cid='"+charge.id+"' cName='"+charge.name+"' uom='"+actualuom+"' value='1' /> ";
		}
		return html;
	};

	var addError = function(emsg){
		$("#infor").append(emsg);
		$("#infor").show();
	};

</script>
<!-- start footer -->



<div id="footer"></div>
<script>
//Load footer
$("#footer").load("footer.html");
</script>

<!-- end footer -->

</body>
</html>